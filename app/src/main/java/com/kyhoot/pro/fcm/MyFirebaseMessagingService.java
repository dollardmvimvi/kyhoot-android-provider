package com.kyhoot.pro.fcm;

import android.content.Intent;
import android.util.Log;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.kyhoot.pro.bookingflow.ChatOnMessageCallback;
import com.kyhoot.pro.main.MainActivity;
import com.kyhoot.pro.pojo.chat.ChatData;
import com.kyhoot.pro.service.LocationPublishService;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CalendarEventHelper;
import com.kyhoot.pro.utility.DataBaseChat;
import com.kyhoot.pro.utility.NotificationHelper;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murshid on 18/9/16.
 * <h2>MyFirebaseMessagingService</h2>
 * MyFirebaseMessagingService is used for receiveng messsage from fcm
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static ChatOnMessageCallback chatListener;


    public static void setChatListener(ChatOnMessageCallback chatList)
    {
        chatListener = chatList;
    }

    /**
     * Called when message is jsonRemoteMessage.
     *
     * @param remoteMessage Object representing the message jsonRemoteMessage from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "onMessageReceived Called" + remoteMessage);
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. AppConfigData messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. AppConfigData messages are the type
        // traditionally used with GCM. Notification messages are only jsonRemoteMessage here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        SessionManager sessionManager = SessionManager.getSessionManager(this);
        DataBaseChat db = new DataBaseChat(this);

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            try {
                JSONObject jsonRemoteMessage = new JSONObject(remoteMessage.getData());
                String action = jsonRemoteMessage.getString("action");
                String message = jsonRemoteMessage.getString("msg");
                String title = jsonRemoteMessage.getString("title");

                Gson gson = new Gson();

                if(action !=null )
                {
                    if(action.equals("1") && jsonRemoteMessage.has("pushType") && jsonRemoteMessage.getString("pushType").equals("2") )
                    {
                        ChatData chatData = gson.fromJson(jsonRemoteMessage.getString("data"),ChatData.class);
                        if(!VariableConstant.IS_CHATTING_OPENED)
                        {
                            db.addNewChat(String.valueOf(chatData.getBid()),chatData.getContent(), sessionManager.getProviderId(),
                                    chatData.getFromID(), String.valueOf(chatData.getTimestamp()),chatData.getType(),"1");
                        }

                        if(chatData.getTimestamp() > sessionManager.getLastTimeStampMsg())
                        {
                            sessionManager.setLastTimeStampMsg(chatData.getTimestamp());

                            if(chatListener!=null)
                            {
                                chatListener.onMessageReceived(chatData);
                            }

                            if(!VariableConstant.IS_CHATTING_RESUMED )
                            {
                                sessionManager.setChatCount(String.valueOf(chatData.getBid()), sessionManager.getChatCount(String.valueOf(chatData.getBid()))+1);
                                sessionManager.setChatBookingID(String.valueOf(chatData.getBid()));
                                sessionManager.setChatCustomerName(chatData.getName());
                                sessionManager.setChatCustomerID(chatData.getFromID());

                                NotificationHelper.sendChatNotification(this,chatData.getName(),message, sessionManager.getChatNotificationId());
                                sessionManager.setChatNotificationId(sessionManager.getChatNotificationId()+1);

                                Intent intent = new Intent();
                                intent.setAction(VariableConstant.INTENT_ACTION_NEW_CHAT);
                                sendBroadcast(intent);

                            }
                        }

                        return;
                    }

                    if(!action.equals("1") && !action.equals("12") && !action.equals("5"))
                    {
                        NotificationHelper.sendNotification(this,action,title,message);
                    }

                    /*
                    1 => New BookingStatus , 18 => BookingAccepted or rejected by email
                    5 = > Expired 12 => Cancel,Unassign 22 => ban, 23 => logout , 21 => offline
                    25 = > Profile activation

                    * */
                    if(action.equals("1"))
                    {
                        VariableConstant.IS_BOOKING_UPDATED = true;

                        JSONObject jsonObjectBooking = new JSONObject(jsonRemoteMessage.getString("data"));

                        if(!jsonObjectBooking.getString("bookingId").equals(sessionManager.getLastBookingId()))
                        {
                            sessionManager.setLastBookingId(jsonObjectBooking.getString("bookingId"));

                            boolean isAssignedBooking = false;
                            if(jsonObjectBooking.has("status") && jsonObjectBooking.getString("status").equals("3"))
                            {
                                sessionManager.setIsAssignedBooking(true);
                                isAssignedBooking=true;
                            }

                            if(Utility.isApplicationSentToBackground(this) || !VariableConstant.IS_MYBOOKING_OPENED)
                            {

                                Intent intentOpen = new Intent(this, MainActivity.class);
                                intentOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                sessionManager.setIsNewBooking(true);
                                startActivity(intentOpen);
                            }
                            else if (VariableConstant.IS_ACCEPTEDBOOKING_OPENED && isAssignedBooking) {
                                //justRefresh();
                                Intent intent = new Intent();
                                intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                                sendBroadcast(intent);
                            } else {
                                VariableConstant.IS_BOOKING_UPDATED = true;
                                Intent intentOpen = new Intent(this, MainActivity.class);
                                intentOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                sessionManager.setIsNewBooking(true);
                                startActivity(intentOpen);
                            }
                            Utility.updateBookingAck(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObjectBooking.getString("bookingId"), sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                        }
                        if(jsonObjectBooking.getString("bookingType").equals("1"))
                        {
                            sessionManager.setIsNowBooking(true);
                        }
                        else
                        {
                            sessionManager.setIsNowBooking(false);
                        }

                    }
                    else if(action.equals("18"))
                    {
                        Intent intent = new Intent();
                        intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                        sendBroadcast(intent);
                    }
                    else if(action.equals("12") || action.equals("5"))
                    {

                        JSONObject expireCancelJsonObject = new JSONObject(jsonRemoteMessage.getString("data"));
                        String bookingId = expireCancelJsonObject.getString("bookingId");
                        String header =  expireCancelJsonObject.has("statusMsg") ? expireCancelJsonObject.getString("statusMsg") :
                                title;
                        String msg = expireCancelJsonObject.has("cancellationReason") ? expireCancelJsonObject.getString("cancellationReason") :
                                message;

                        if(!bookingId.equals(sessionManager.getLastBookingIdCancel()))
                        {
                            sessionManager.setLastBookingIdCancel(bookingId);
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                            intent.putExtra("cancelid",bookingId);
                            intent.putExtra("header",header);
                            intent.putExtra("msg",msg);
                            sendBroadcast(intent);

                            VariableConstant.IS_BOOKING_UPDATED = true;
                            NotificationHelper.sendNotification(this,action,header,msg);

                            Log.d(TAG, "Booking cancel from Fcm");
                        }

                        if(expireCancelJsonObject.has("reminderId"))
                        {
                            if(!expireCancelJsonObject.getString("reminderId").equals(""))
                            {
                                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                                calendarEventHelper.deleteEvent(expireCancelJsonObject.getString("reminderId"));
                            }
                        }
                    }
                    else if(action.equals("21"))
                    {
                        if(Utility.isMyServiceRunning(this,LocationPublishService.class))
                        {
                            Intent stopIntent = new Intent(this, LocationPublishService.class);
                            stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
                            startService(stopIntent);
                            AppController.getInstance().getMqttHelper().disconnect();
                        }
                        Intent intent = new Intent();
                        intent.setAction(VariableConstant.INTENT_ACTION_MAKE_OFFLINE);
                        sendBroadcast(intent);
                    }
                    else if(action.equals("23") || action.equals("22"))
                    {
                        Utility.logoutSessionExiperd(sessionManager,this);
                    }
                    else if(action.equals("25"))
                    {
                        Intent intent = new Intent();
                        intent.setAction(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION);
                        sendBroadcast(intent);
                        sessionManager.setIsProfileAcivated(true);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}