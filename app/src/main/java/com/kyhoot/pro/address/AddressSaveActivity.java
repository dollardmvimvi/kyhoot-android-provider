package com.kyhoot.pro.address;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kyhoot.pro.R;
import com.kyhoot.pro.landing.signup.SignupActivity;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.DialogHelper;
import com.kyhoot.pro.utility.LocationUtil;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressSaveActivity</h1>
 * AddressSaveActivity for showing map for pick up the address
 */

public class AddressSaveActivity extends AppCompatActivity implements View.OnClickListener, AddressSavePresenter.AddressSavePresenterImple, OnMapReadyCallback, CompoundButton.OnCheckedChangeListener, LocationUtil.GetLocationListener {

    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private AddressSavePresenter presenter;

    private EditText etAddressHeader;
    private TextView tvAddress;
    private String addressId = "", placeId = "", latitude, longitude, taggedAs = "others";
    private String city, state, pincode;

    private boolean isEdit = false, isSignup = false;

    private GoogleMap googleMap;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 99;
    private View mapView;

    private AlertDialog dialogGps;
    private LocationUtil locationUtil;
    private boolean isCameraMoved = false;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
        if (hasFocus) {
            if (Utility.isGpsEnabled(this)) {
                if (dialogGps != null && dialogGps.isShowing()) {
                    dialogGps.dismiss();
                }

                if (locationUtil != null) {
                    if (locationUtil.isGoogleApiClientConnected()) {
                        locationUtil.startLocationUpdates();
                    } else {
                        locationUtil.connectGoogleApiClient();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_save);

        init();
    }

    /**
     * init the views
     */
    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.saving));
        progressDialog.setCancelable(false);
        presenter = new AddressSavePresenter(this, this);

        locationUtil = new LocationUtil(this, this);
        dialogGps = DialogHelper.adEnableGPS(this);

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        fm.getMapAsync(this);
        mapView = fm.getView();

        isSignup = getIntent().getBooleanExtra("isSignup", false);
/*
        Intent intent = getIntent();
        placeId = intent.getStringExtra("placeId");
        name = intent.getStringExtra("name");
        address = intent.getStringExtra("address");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        isEdit = intent.getBooleanExtra("isEdit", false);
        taggedAs = intent.getStringExtra("taggedAs");*/


        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontLight = Utility.getFontLight(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.location));
        tvTitle.setTypeface(fontBold);

        TextView tvDone = findViewById(R.id.tvDone);
        tvDone.setText(getString(R.string.save));
        tvDone.setTypeface(fontBold);
        tvDone.setOnClickListener(this);

        RadioButton rbHome = findViewById(R.id.rbHome);
        rbHome.setTypeface(fontLight);
        rbHome.setOnCheckedChangeListener(this);
        RadioButton rbOffice = findViewById(R.id.rbOffice);
        rbOffice.setTypeface(fontLight);
        rbOffice.setOnCheckedChangeListener(this);
        RadioButton rbOthers = findViewById(R.id.rbOthers);
        rbOthers.setTypeface(fontLight);
        rbOthers.setOnCheckedChangeListener(this);

        etAddressHeader = findViewById(R.id.etAddressHeader);
        etAddressHeader.setTypeface(fontRegular);

        tvAddress = findViewById(R.id.tvAddress);
        tvAddress.setTypeface(fontRegular);
        tvAddress.setOnClickListener(this);

        presenter.getAddress(sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
    }

    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(false);

        LatLng latLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng()));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).tilt(30).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //apiCallerForAddress.getAddress(googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude, AddressSaveActivity.this);

                latitude = String.valueOf(googleMap.getCameraPosition().target.latitude);
                longitude = String.valueOf(googleMap.getCameraPosition().target.longitude);
                presenter.getAddress(String.valueOf(googleMap.getCameraPosition().target.latitude), String.valueOf(googleMap.getCameraPosition().target.longitude));

            }
        });

        try {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            rlp.setMargins(0, 0, 30, 30);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationUtil.connectGoogleApiClient();
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationUtil.disconnectGoogleApiClient();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvDone:
                if (isSignup) {
                    taggedAs = etAddressHeader.getText().toString();
                    if (tvAddress.getText().toString().equals("")) {
                        onEmptyAddressLine();
                        return;
                    } else if (taggedAs.equals("")) {
                        onEmptyTagged();
                        return;
                    }
                    Utility.hideKeyboad(this);
                    Intent intent = new Intent(this, SignupActivity.class);
                    intent.putExtra("address", tvAddress.getText().toString());
                    intent.putExtra("taggedAs", taggedAs);
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("city", city);
                    intent.putExtra("state", state);
                    intent.putExtra("pincode", pincode);

                    setResult(RESULT_OK, intent);
                    closeActivity();
                    return;
                }
                try {

                    taggedAs = etAddressHeader.getText().toString();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("addLine1", tvAddress.getText().toString());
                    //jsonObject.put("addLine2","");
                    jsonObject.put("placeId", placeId);
                    jsonObject.put("latitude", latitude);
                    jsonObject.put("longitude", longitude);
                    jsonObject.put("taggedAs", taggedAs);
                    jsonObject.put("city", city);
                    jsonObject.put("state", state);
                    jsonObject.put("pincode", pincode);
                    jsonObject.put("userType", VariableConstant.USER_TYPE);

                    presenter.saveEditAddress(isEdit, addressId, AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.tvAddress:
                findPlace();
                break;
        }
    }

    private void findPlace() {
        try {
            LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds
                    (new LatLng(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng())),
                            new LatLng(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng())));

            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Handle the error.

        }
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        VariableConstant.IS_ADDRESS_LIST_CHANGED = true;
        closeActivity();
    }

    @Override
    public void onEmptyTagged() {
        Toast.makeText(this, getString(R.string.plsEnterAddressName), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmptyAddressLine() {
        Toast.makeText(this, getString(R.string.fetchingAddress), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailureFetchingAddresss() {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            switch (buttonView.getId()) {
                case R.id.rbHome:
                    etAddressHeader.setVisibility(View.GONE);
                    etAddressHeader.setText("home");
                    break;

                case R.id.rbOffice:
                    etAddressHeader.setVisibility(View.GONE);
                    etAddressHeader.setText("office");
                    break;

                case R.id.rbOthers:
                    etAddressHeader.setVisibility(View.VISIBLE);
                    etAddressHeader.setText("");
                    break;
            }
        }
    }

    @Override
    public void onAddress(final LocationPlaces places) {
        tvAddress.setText(places.getAddress());
        latitude = String.valueOf(places.getLatitude());
        longitude = String.valueOf(places.getLongitude());
        placeId = places.getPlace_id();

        city = places.getCity();
        state = places.getState();
        pincode = places.getPincode();

        Log.d("Addressss", "onAddress: " + city + "/n" + state + "/n" + pincode);

    }

    @Override
    public void onAddress(Address address) {
        if(address!=null) {
            city = address.getLocality();
            state = address.getAdminArea();
            pincode = address.getPostalCode();
            tvAddress.setText(address.getAddressLine(0));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE && resultCode == RESULT_OK) {
            Place place = PlaceAutocomplete.getPlace(this, data);
            LatLng MOUNTAIN_VIEW = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(MOUNTAIN_VIEW)
                    .zoom(17)
                    .tilt(30)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            tvAddress.setText(place.getAddress());
            etAddressHeader.setText(place.getName());
            latitude = String.valueOf(place.getLatLng().latitude);
            longitude = String.valueOf(place.getLatLng().longitude);
            placeId = place.getId();
        }

        if (requestCode == VariableConstant.REQUEST_CODE_GPS) {
            if (resultCode == RESULT_OK) {
                if (dialogGps.isShowing()) {
                    dialogGps.dismiss();
                }
                if (locationUtil.isGoogleApiClientConnected()) {
                    locationUtil.startLocationUpdates();
                } else {
                    locationUtil.connectGoogleApiClient();
                }
            } else if (resultCode == RESULT_CANCELED) {
                if (!dialogGps.isShowing()) {
                    dialogGps.show();
                }
            }
        }
    }


    @Override
    public void updateLocation(Location location) {
        if (googleMap != null && !isCameraMoved) {
            isCameraMoved = true;

            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).tilt(30).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            latitude = String.valueOf(latLng.latitude);
            longitude = String.valueOf(latLng.longitude);
        }
    }

    @Override
    public void location_Error(String error) {

    }

}
