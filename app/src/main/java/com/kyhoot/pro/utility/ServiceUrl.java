package com.kyhoot.pro.utility;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>ServiceUrl</h1>
 * ServiceUrl is the class that contain all api's for whole app
 */

public class ServiceUrl {
 private static final String HOST_URL = "https://api.kyhoot.com/";
 public static final String MQTT_HOST = "77.68.30.222";//mqtt.go-tasker.com
 public static final String MQTT_PORT = "2052";
 public static final String EARNING_URL = "https://superadmin.kyhoot.com/payrollform/?";


 private static final String PROVIDER_URL = HOST_URL+"provider/";
 private static final String ZENDESK_URL = HOST_URL+"zendesk/";
 public static final String SERVER_TIME = HOST_URL+"server/serverTime";

 /*                          Chat               */
 public static final String BOOKING_CHAT_CUSTOMER_LIST = PROVIDER_URL +"booking/chat";
 public static final String CHAT_SEND = HOST_URL + "message";
 public static final String CHAT_HISTORY = HOST_URL +"chatHistory";
 public static final String CHAT_DATA_UPLOAD = HOST_URL +"uploadImage";

 /*                       language                        */
 public static final String LANGUAGE = PROVIDER_URL +"language";

 /*           Signup, Login  and HelpWithPassword               */
 public static final String CITY = PROVIDER_URL +"city";
 public static final String SERVICE_CATEGORIES = PROVIDER_URL +"serviceCateogries";
 public static final String PHONE_VALIDATION = PROVIDER_URL +"phoneValidation";
 public static final String EMAIL_VALIDATION = PROVIDER_URL +"emailValidation";
 public static final String REFERAL_CODE_VALIDATION = PROVIDER_URL +"referralCodeValidation";
 public static final String SIGNUP = PROVIDER_URL +"signUp";
 public static final String VERIFY_OTP_SIGNUP = PROVIDER_URL +"verifyPhoneNumber";
 public static final String LOGIN = PROVIDER_URL +"signIn";
 public static final String FORGOT_PASSWORD = PROVIDER_URL +"forgotPassword";
 public static final String RESENT_OTP_FORGOT_PASSWORD = PROVIDER_URL +"resendOtp";
 public static final String VERIFY_OTP_FORGOT_PASSWORD_CHANGE_PHONE_NUMBER = PROVIDER_URL +"verifyVerificationCode";
 public static final String FORGOT_RESET_PASSWORD = PROVIDER_URL +"password";

 /*            My Booking  Fragment    */
 public static final String CONFIG = PROVIDER_URL +"config";
 public static final String BOOKING = PROVIDER_URL +"booking";
 public static final String BOOKINGS = PROVIDER_URL +"bookings";
 public static final String MASTER_STATUS = PROVIDER_URL +"status";
 public static final String UPDATE_LOCATION = PROVIDER_URL +"location";
 public static final String UPDATE_LOCATION_LOGS = PROVIDER_URL +"locationLogs";

 /*          Live Booking        */
 public static final String BOOKING_ACK = PROVIDER_URL +"bookingAck";
 public static final String BOOKING_RESPONSE = PROVIDER_URL +"bookingResponse";
 public static final String BOOKING_STATUS = PROVIDER_URL +"bookingStatus";
 public static final String CANCEL_REASONS = PROVIDER_URL +"cancelReasons";
 public static final String CANCEL_BOOKING = PROVIDER_URL +"cancelBooking";
 public static final String BOOKING_TIMER = PROVIDER_URL +"bookingTimer";
 public static final String REVIEW_AND_RATING = PROVIDER_URL +"reviewAndRating";
 public static final String CUSTOMER_REVIEW_AND_RATING = PROVIDER_URL +"customer/reviewAndRating";

 /*            My Profile         */
 public static final String REFRESH_TOKEN = PROVIDER_URL +"accessToken";
 public static final String LOGOUT = PROVIDER_URL +"logout";
 public static final String GET_REVIEW_RATING = PROVIDER_URL +"reviewAndRating";
 public static final String ADDRESS = PROVIDER_URL +"address";
 public static final String SUPPORT = PROVIDER_URL +"support";
 public static final String CHANGE_EMAIL = PROVIDER_URL +"email";
 public static final String CHANGE_PHONE_NUMBER = PROVIDER_URL +"phoneNumber";
 public static final String PROFILE = PROVIDER_URL +"profile/me";
 public static final String PROFILE_CHANGE_PASSWORD = PROVIDER_URL +"password/me";

 /*            My Rate Card, Document (Categories)           */
 public static final String GET_CATEGORY = PROVIDER_URL +"category";
 public static final String UPDATE_CATEGORY_SERVICE = PROVIDER_URL +"service";
 public static final String UPDATE_SERVICES = PROVIDER_URL +"services";
 public static final String UPDATE_DOCUMENT = PROVIDER_URL +"documents";

 /*       Bank Account  And Wallet   */
 public static final String BANK_CONNECT_ACCOUNT_STRIPE = HOST_URL+"connectAccount";
 public static final String BANK_EXTERNAL_ACCOUNT = HOST_URL+"externalAccount";

 public static final String GET_WALLET_PAYMENT_SETTINGS = PROVIDER_URL+"paymentsettings";
 public static final String WALLET_RECHARGE = PROVIDER_URL+"rechargeWallet";
 public static final String GET_WALLET_TRANSACTION = PROVIDER_URL+"wallet/transction";
 public static final String CARD = PROVIDER_URL+"card";

 /*          Schedule  and History */
 public static final String SCHEDULE = PROVIDER_URL +"schedule";
 public static final String BOOKING_HISTORY_BY_WEEK = PROVIDER_URL +"bookingHistoryByWeek";
 public static final String BOOKING_HISTORY = PROVIDER_URL +"bookingHistory";

 /*       Zendesk                */
 public static final String ZENDESK_GET_ALL_TICKET = ZENDESK_URL+"user/ticket";
 public static final String ZENDESK_RAISE_TICKET = ZENDESK_URL+"ticket";
 public static final String ZENDESK_GET_TICKET_HISTORY = ZENDESK_URL+"ticket/history";
 public static final String ZENDESK_PUT_TICKET_COMMENT = ZENDESK_URL+"ticket/comments";

 public static final String IMAGE_UPLOAD_ON_SERVER= HOST_URL + "uploadImageOnServer";

}
