package com.kyhoot.pro.utility;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatDelegate;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.kyhoot.pro.R;
import com.kyhoot.pro.account.AccountManagerHelper;
import com.kyhoot.pro.mqtt.MqttHelper;
import com.kyhoot.pro.service.NewBookingRingtoneService;

import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;

//import android.support.multidex.MultiDex;


/**
 * <h>AppController</h>
 * Created by murashid on 02-Oct-17.
 */

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;

    private MqttHelper mqttHelper;

    private MixpanelHelper mixpanelHelper;

    private AccountManagerHelper accountManagerHelper;

    private Intent newBookingRingtoneServiceIntent;

    private SessionManager sessionManager;
    private TimeZone timeZone;


    public static synchronized AppController getInstance() {
        return mInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        sessionManager = SessionManager.getSessionManager(this);
        mInstance = this;
        mixpanelHelper = new MixpanelHelper(this);
        mqttHelper = new MqttHelper(this);
    }

    public void getCorrectTimeZoneByLatLng() {
        if (!sessionManager.getCurrentLat().equals("0.0")) {
            String timeZoneString = TimezoneMapper.latLngToTimezoneString(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng()));
            timeZone = TimeZone.getTimeZone(timeZoneString);
        }
    }

    public TimeZone getTimeZone() {
        if (timeZone != null) {
            return timeZone;
        }
        return TimeZone.getTimeZone("Etc/UTC");
    }

    public MqttHelper getMqttHelper() {
        if (mqttHelper == null) {
            mqttHelper = new MqttHelper(this);
        }
        return mqttHelper;
    }

    public MixpanelHelper getMixpanelHelper() {
        if (mixpanelHelper == null) {
            mixpanelHelper = new MixpanelHelper(this);
        }
        return mixpanelHelper;
    }

    public AccountManagerHelper getAccountManagerHelper() {
        if (accountManagerHelper == null) {
            accountManagerHelper = new AccountManagerHelper(this);
        }
        return accountManagerHelper;
    }

    private Intent getNewBookingRingtoneServiceIntent() {
        if (newBookingRingtoneServiceIntent == null) {
            newBookingRingtoneServiceIntent = new Intent(this, NewBookingRingtoneService.class);
        }
        return newBookingRingtoneServiceIntent;
    }

    public void startNewBookingRingtoneService() {
        startService(getNewBookingRingtoneServiceIntent());
    }

    public void stopNewBookingRingtoneService() {
        stopService(getNewBookingRingtoneServiceIntent());
    }

    public static synchronized void toast() {
        Toast.makeText(getInstance(), getInstance().getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    public synchronized void toast(String msg) {
        Toast.makeText(getInstance(), msg, Toast.LENGTH_SHORT).show();
    }
}

