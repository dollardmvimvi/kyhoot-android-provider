package com.kyhoot.pro.utility

/*
created by yogesh G
at 2/8/2019
*/

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.kyhoot.pro.R
import kotlinx.android.synthetic.main.legal_activity.*

class Legal_option : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.legal_activity)

       val cliclListner= View.OnClickListener { view ->

            when (view.getId()) {
                R.id.ivCloseButton -> {
                        this.finish()
                }
                R.id.termsrl -> {
                    val webIntent = Intent(this, WebViewActivity::class.java)
                    webIntent.putExtra("URL", VariableConstant.TERMS_OF_SERVICE)
                    webIntent.putExtra("title", getString(R.string.termsOfServiceCapital))
                    startActivity(webIntent)
                    overridePendingTransition(R.anim.slide_left_to_center, R.anim.slide_right_to_center)
                }
                R.id.servicerl -> {
                    val webIntent = Intent(this, WebViewActivity::class.java)
                    webIntent.putExtra("URL", VariableConstant.PRIVACY_POLICY)
                    webIntent.putExtra("title", getString(R.string.privacy_policy))
                    startActivity(webIntent)
                    overridePendingTransition(R.anim.slide_left_to_center, R.anim.slide_right_to_center)
                }
            }
        }

        ivCloseButton.setOnClickListener(cliclListner)
        termsrl.setOnClickListener(cliclListner)
        servicerl.setOnClickListener(cliclListner)
    }
}