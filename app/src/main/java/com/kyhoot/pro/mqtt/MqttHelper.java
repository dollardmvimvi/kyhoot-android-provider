package com.kyhoot.pro.mqtt;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.kyhoot.pro.R;
import com.kyhoot.pro.bookingflow.ChatOnMessageCallback;
import com.kyhoot.pro.main.MainActivity;
import com.kyhoot.pro.pojo.chat.ChatData;
import com.kyhoot.pro.pojo.chat.ChatMqttResponce;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.NotificationHelper;
import com.kyhoot.pro.utility.ServiceUrl;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by murashid on 10-Jan-18.
 */

public class MqttHelper {

    private static final String TAG = "MqttHelper";
    private MqttAndroidClient mqttAndroidClient;
    private MqttConnectOptions mqttConnectOptions;
    private IMqttActionListener listener;

    private Context context;
    private SessionManager sessionManager;
    private ChatOnMessageCallback chatListener;

    public MqttHelper(Context context)
    {
        this.context = context;
        sessionManager = SessionManager.getSessionManager(context);
        listener = new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {
                updatePresence(1, false);
                subscribeToTopic(MqttEvents.Booking.value+"/"+sessionManager.getProviderId(), 1);
                subscribeToTopic(MqttEvents.JobStatus.value+"/"+sessionManager.getProviderId(), 1);
                subscribeToTopic(MqttEvents.Message.value + "/" + sessionManager.getProviderId(), 1);

                Log.d(TAG, "onSuccess: myqtt client ");
            }

            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                Log.d(TAG, "onFailure: myqtt client ");
            }
        };
    }

    public void setChatListener(ChatOnMessageCallback chatListener)
    {
        this.chatListener = chatListener;
    }

    // region Mqqtt connection
    @SuppressWarnings("unchecked")
    public void createMQttConnection(String clientId) {
        String serverUri = "tcp://" + ServiceUrl.MQTT_HOST + ":" + ServiceUrl.MQTT_PORT;

        mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId,new MemoryPersistence(), MqttAndroidClient.Ack.AUTO_ACK);
        mqttAndroidClient.setCallback(new MqttCallback() {

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                /*
                 * To parse the result of the message received on a MQtt topic
                 */
                Log.d(TAG, "messageArrived: "+message);
                JSONObject obj = convertMessageToJsonObject(message);
                Log.d(TAG, "messageArrived: "+topic);
                Log.d(TAG, "messageArrived: "+obj);

                if (topic.equals(MqttEvents.Booking.value+"/"+sessionManager.getProviderId()))
                {
                    JSONObject jsonObjectBooking = new JSONObject(obj.getJSONObject("data").toString());
                    //
                    if(!jsonObjectBooking.getString("bookingId").equals(sessionManager.getLastBookingId()))
                    {
                        sessionManager.setLastBookingId(jsonObjectBooking.getString("bookingId"));

                        Log.d(TAG, "Booking from Mqtt");

                        String title = context.getString(R.string.newBookingTitle);
                        String notificationMsg = context.getString(R.string.newBookingMsg);
                        boolean isAssignedBooking = false;
                        if(jsonObjectBooking.has("status") && jsonObjectBooking.getString("status").equals("3"))
                        {
                            sessionManager.setIsAssignedBooking(true);
                            isAssignedBooking=true;
                        }

                        if(Utility.isApplicationSentToBackground(context) || !VariableConstant.IS_MYBOOKING_OPENED)
                        {

                            VariableConstant.IS_BOOKING_UPDATED = true;
                            Intent intentOpen = new Intent(context, MainActivity.class);
                            intentOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            sessionManager.setIsNewBooking(true);
                            context.startActivity(intentOpen);
                        }
                        else if (VariableConstant.IS_ACCEPTEDBOOKING_OPENED && isAssignedBooking) {
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                            context.sendBroadcast(intent);
                        } else {
                            VariableConstant.IS_BOOKING_UPDATED = true;
                            Intent intentOpen = new Intent(context, MainActivity.class);
                            intentOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            sessionManager.setIsNewBooking(true);
                            context.startActivity(intentOpen);
                        }

                        Utility.updateBookingAck(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObjectBooking.getString("bookingId"),sessionManager.getCurrentLat(),sessionManager.getCurrentLng());
                    }
                }
                else if(topic.equals(MqttEvents.JobStatus.value+"/"+sessionManager.getProviderId()))
                {
                    JSONObject cancelJsonObject =obj.getJSONObject("data");
                    String bookingId = cancelJsonObject.getString("bookingId");
                    String header =  cancelJsonObject.has("statusMsg") ? cancelJsonObject.getString("statusMsg") :
                            context.getString(R.string.message);
                    String  msg  =  cancelJsonObject.has("cancellationReason") ? cancelJsonObject.getString("cancellationReason") :
                            context.getString(R.string.bookingUnassigned);

                    if(!bookingId.equals(sessionManager.getLastBookingIdCancel()))
                    {
                        sessionManager.setLastBookingIdCancel(bookingId);
                        Intent intent = new Intent();
                        intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                        intent.putExtra("cancelid",bookingId);
                        intent.putExtra("header",header);
                        intent.putExtra("msg",msg);
                        context.sendBroadcast(intent);

                        VariableConstant.IS_BOOKING_UPDATED = true;
                        NotificationHelper.sendNotification(context,"15",header , msg);

                        Log.d(TAG, "Booking cancel from Mqtt");

                    }
                }
                else if(topic.equals(MqttEvents.Message.value+"/"+sessionManager.getProviderId()))
                {
                    Gson gson = new Gson();
                    ChatMqttResponce chatMqttResponce = gson.fromJson(obj.toString(),ChatMqttResponce.class);
                    ChatData chatData = chatMqttResponce.getData();
                    if(chatData.getTimestamp() > sessionManager.getLastTimeStampMsg())
                    {
                        sessionManager.setLastTimeStampMsg(chatData.getTimestamp());

                        if(chatListener!=null)
                        {
                            chatListener.onMessageReceived(chatData);
                        }

                        if(!VariableConstant.IS_CHATTING_RESUMED )
                        {
                            sessionManager.setChatCount(String.valueOf(chatData.getBid()),sessionManager.getChatCount(String.valueOf(chatData.getBid()))+1);
                            sessionManager.setChatBookingID(String.valueOf(chatData.getBid()));
                            sessionManager.setChatCustomerName(chatData.getName());
                            sessionManager.setChatCustomerID(chatData.getFromID());

                            NotificationHelper.sendChatNotification(context,chatData.getName(),chatData.getContent(),sessionManager.getChatNotificationId());
                            sessionManager.setChatNotificationId(sessionManager.getChatNotificationId()+1);

                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_NEW_CHAT);
                            context.sendBroadcast(intent);
                        }
                    }

                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {


            }
        });

        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setAutomaticReconnect(true);

        JSONObject obj = new JSONObject();
        try
        {
            obj.put("id", sessionManager.getProviderId());
            obj.put("status", VariableConstant.TIME_OUT);
            obj.put("lat", sessionManager.getCurrentLat());
            obj.put("long", sessionManager.getCurrentLng());
            obj.put("time", Utility.getCurrentTime());
        }
        catch (JSONException e)
        {

        }
        mqttConnectOptions.setWill(MqttEvents.LastWillTopic.value+"/"+sessionManager.getProviderId(), obj.toString().getBytes(), 0, true);
        mqttConnectOptions.setKeepAliveInterval(Integer.parseInt(sessionManager.getProTimeOut()));

    /*
     * Has been removed from here to avoid the reace condition for the mqtt connection with the mqtt broker
     */
        connectMqttClient();

    }

    // endregion

    @SuppressWarnings("TryWithIdenticalCatches")
    public void publish(String topicName, JSONObject obj, int qos, boolean retained) {

        try {
            mqttAndroidClient.publish(topicName, obj.toString().getBytes(), qos, retained);
        } catch (MqttException e) {


        } catch (Exception e) {

        }

    }

    private JSONObject convertMessageToJsonObject(MqttMessage message) {

        JSONObject obj = new JSONObject();
        try {

            obj = new JSONObject(new String(message.getPayload()));
        } catch (JSONException e) {

        }
        return obj;
    }


    private void connectMqttClient() {
        try
        {
            mqttAndroidClient.connect(mqttConnectOptions, context, listener);

        } catch (MqttException e) {

        }
    }

    public void subscribeToTopic(String topic, int qos) {

        try {
            if (mqttAndroidClient != null) {
                mqttAndroidClient.subscribe(topic, qos);
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /**
     * @param topic Topic name from which to  unsubscribe
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    public void unsubscribeToTopic(String topic) {

        try {
            if (mqttAndroidClient != null) {
                mqttAndroidClient.unsubscribe(topic);
            }
        } catch (Exception e) {

        }
    }

    public void updatePresence(int status, boolean applicationKilled) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("id", 1);
            obj.put("status", "");


            publish(MqttEvents.PresenceTopic.value + "/" + "", obj, 0, true);
        } catch (JSONException w) {

        }
    }


    /**
     * @return boolean value depending on whther currently can publish or not
     */

    public boolean isMqttConnected() {
        try {
            return mqttAndroidClient != null && mqttAndroidClient.isConnected();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * To disconnect the MQtt client on app being killed
     */

    public void disconnect() {
        try {
            if (mqttAndroidClient != null)
            {
                unsubscribeToTopic(sessionManager.getProviderId());
                mqttAndroidClient.disconnect();
            }
        } catch (MqttException e) {

        }
        catch (Exception e)
        {

        }
    }

    /**
     * Callback when reconnection is made
     */
    public void updateReconnected() {
        updatePresence(1, false);
    }

}
