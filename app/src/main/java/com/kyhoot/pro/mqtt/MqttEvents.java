package com.kyhoot.pro.mqtt;

/**
 * Created by moda on 29/06/17.
 */


public enum MqttEvents {

    Booking("booking"),

    LastWillTopic("location"),

    PresenceTopic("PresenceTopic"),

    LiveTrack("liveTrack"),

    JobStatus("jobStatus"),

    Message("message");


    public String value;


    MqttEvents(String value) {
        this.value = value;
    }


}