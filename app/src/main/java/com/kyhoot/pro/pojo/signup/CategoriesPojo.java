package com.kyhoot.pro.pojo.signup;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 */

public class CategoriesPojo implements Serializable {
    private String message;
    private ArrayList<CategoriesData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CategoriesData> getData() {
        return data;
    }

    public void setData(ArrayList<CategoriesData> data) {
        this.data = data;
    }

}
