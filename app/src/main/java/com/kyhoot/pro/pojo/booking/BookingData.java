package com.kyhoot.pro.pojo.booking;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Oct-17.
 */

public class BookingData implements Serializable {
    private String status;
    private String profileActivationStatus;
    private String profileStatus;
    private ArrayList<Booking> request;
    private ArrayList<Booking> accepted;
//    private ArrayList<Booking> activeBid;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Booking> getRequest() {
        return request;
    }

    public void setRequest(ArrayList<Booking> request) {
        this.request = request;
    }

    public ArrayList<Booking> getAccepted() {
        return accepted;
    }

    public void setAccepted(ArrayList<Booking> accepted) {
        this.accepted = accepted;
    }

   /* public ArrayList<Booking> getActiveBid() {
        return activeBid;
    }

    public void setActiveBid(ArrayList<Booking> activeBid) {
        this.activeBid = activeBid;
    }*/

    public String getProfileActivationStatus() {
        return profileActivationStatus;
    }

    public void setProfileActivationStatus(String profileActivationStatus) {
        this.profileActivationStatus = profileActivationStatus;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }
}
