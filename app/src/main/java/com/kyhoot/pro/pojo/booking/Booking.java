package com.kyhoot.pro.pojo.booking;

import com.kyhoot.pro.pojo.history.Accounting;
import com.kyhoot.pro.pojo.history.AdditionalService;
import com.kyhoot.pro.pojo.history.CustomerData;
import com.kyhoot.pro.pojo.history.ReviewByProvider;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Nov-17.
 */

public class Booking implements Serializable {

    private String bookingId;
    private String customerId;
    private String firstName;
    private String lastName;
    private String phone;
    private String profilePic;
    private String bookingRequestedFor;
    private String bookingExpireTime;
    private String bookingRequestedAt;
    private String bookingEndtime;
    private String eventStartTime;
    private String bookingRequestedForProvider = "0";
    private String bookingExpireForProvider = "0";
    private String serverTime = "0";
    private long currentTime = 0;

    private String addLine1;
    private String addLine2;
    private String latitude;
    private String longitude;
    private String averageRating;
    private String typeofEvent;
    private GigTime service;
    private String distance;
    private String paymentMethod;
    private String status;
    private BookingTimer bookingTimer;

    private String amount;
    private String statusMsg;
    private String signatureUrl;

    public String getScheduleTime() {
        return scheduleTime;
    }

    private String scheduleTime;

    private Accounting accounting;
    private ArrayList<ServiceItem> item;
    private ArrayList<ServiceItem> cartData;
    private String serviceType = "1";
    private String jobDescription = "";

    public ArrayList<String> getJobImages() {
        return jobImages;
    }

    public void setJobImages(ArrayList<String> jobImages) {
        this.jobImages = jobImages;
    }

    private ArrayList<String> jobImages;


    private String catName = "";

    public String getCategory() {
        return category;
    }

    private String category = "";
    private String bookingType;
    private String bookingModel;
    private String reminderId;

    //This is for History and Schedule
    private CustomerData customerData;
    private ReviewByProvider reviewByProvider;
    private ArrayList<AdditionalService> additionalService;
    private String cancellationReason;


    public ArrayList<String> getDaysArr() {
        return daysArr;
    }

    private ArrayList<String> daysArr;



    public String getBookingId() {
        return bookingId;
    }


    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public String getPhone() {
        return phone;
    }


    public String getProfilePic() {
        return profilePic;
    }


    public String getBookingRequestedFor() {
        return bookingRequestedFor;
    }


    public String getBookingExpireTime() {
        return bookingExpireTime;
    }


    public String getBookingRequestedAt() {
        return bookingRequestedAt;
    }


    public String getBookingRequestedForProvider() {
        return bookingRequestedForProvider;
    }


    public String getBookingExpireForProvider() {
        return bookingExpireForProvider;
    }


    public String getAddLine1() {
        return addLine1;
    }


    public String getAddLine2() {
        return addLine2;
    }


    public String getLatitude() {
        return latitude;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public void setBookingRequestedFor(String bookingRequestedFor) {
        this.bookingRequestedFor = bookingRequestedFor;
    }

    public void setBookingExpireTime(String bookingExpireTime) {
        this.bookingExpireTime = bookingExpireTime;
    }

    public void setBookingRequestedAt(String bookingRequestedAt) {
        this.bookingRequestedAt = bookingRequestedAt;
    }

    public void setBookingEndtime(String bookingEndtime) {
        this.bookingEndtime = bookingEndtime;
    }

    public void setEventStartTime(String eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public void setBookingRequestedForProvider(String bookingRequestedForProvider) {
        this.bookingRequestedForProvider = bookingRequestedForProvider;
    }

    public void setBookingExpireForProvider(String bookingExpireForProvider) {
        this.bookingExpireForProvider = bookingExpireForProvider;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public void setAddLine1(String addLine1) {
        this.addLine1 = addLine1;
    }

    public void setAddLine2(String addLine2) {
        this.addLine2 = addLine2;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public void setTypeofEvent(String typeofEvent) {
        this.typeofEvent = typeofEvent;
    }

    public void setService(GigTime service) {
        this.service = service;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setBookingTimer(BookingTimer bookingTimer) {
        this.bookingTimer = bookingTimer;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public void setSignatureUrl(String signatureUrl) {
        this.signatureUrl = signatureUrl;
    }

    public void setAccounting(Accounting accounting) {
        this.accounting = accounting;
    }

    public void setItem(ArrayList<ServiceItem> item) {
        this.item = item;
    }

    public void setCartData(ArrayList<ServiceItem> cartData) {
        this.cartData = cartData;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public void setBookingModel(String bookingModel) {
        this.bookingModel = bookingModel;
    }

    public void setReminderId(String reminderId) {
        this.reminderId = reminderId;
    }

    public void setCustomerData(CustomerData customerData) {
        this.customerData = customerData;
    }

    public void setReviewByProvider(ReviewByProvider reviewByProvider) {
        this.reviewByProvider = reviewByProvider;
    }

    public void setAdditionalService(ArrayList<AdditionalService> additionalService) {
        this.additionalService = additionalService;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public String getLongitude() {
        return longitude;
    }


    public String getAverageRating() {
        return averageRating;
    }


    public String getTypeofEvent() {
        return typeofEvent;
    }



    public GigTime getService() {
        return service;
    }


    public String getDistance() {
        return distance;
    }


    public String getPaymentMethod() {
        return paymentMethod;
    }


    public String getStatus() {
        return status;
    }


    public BookingTimer getBookingTimer() {
        return bookingTimer;
    }


    public String getCustomerId() {
        return customerId;
    }


    public String getAmount() {
        return amount;
    }


    public String getStatusMsg() {
        return statusMsg;
    }


    public String getSignatureUrl() {
        return signatureUrl;
    }




    public String getBookingEndtime() {
        return bookingEndtime;
    }



    public String getEventStartTime() {
        return eventStartTime;
    }



    public Accounting getAccounting() {
        return accounting;
    }



    public String getServiceType() {
        return serviceType;
    }



    public String getBookingType() {
        return bookingType;
    }



    public String getBookingModel() {
        return bookingModel;
    }



    public String getCatName() {
        return catName;
    }


    public String getServerTime() {
        return serverTime;
    }



    public long getCurrentTime() {
        return currentTime;
    }



    public String getReminderId() {
        return reminderId;
    }



    public ArrayList<ServiceItem> getCartData() {
        return cartData;
    }



    public CustomerData getCustomerData() {
        return customerData;
    }



    public ReviewByProvider getReviewByProvider() {
        return reviewByProvider;
    }


    public ArrayList<AdditionalService> getAdditionalService() {
        return additionalService;
    }



    public String getCancellationReason() {
        return cancellationReason;
    }



    public String getJobDescription() {
        return jobDescription;
    }

}
