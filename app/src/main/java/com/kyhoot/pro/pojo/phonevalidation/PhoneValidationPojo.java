package com.kyhoot.pro.pojo.phonevalidation;

/**
 * Created by murashid on 06-Oct-17.
 */

public class PhoneValidationPojo {
    private String message;
    private PhoneValidationData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PhoneValidationData getData() {
        return data;
    }

    public void setData(PhoneValidationData data) {
        this.data = data;
    }
}
