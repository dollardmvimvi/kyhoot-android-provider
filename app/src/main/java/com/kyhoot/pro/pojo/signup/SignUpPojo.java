package com.kyhoot.pro.pojo.signup;

/**
 * Created by murashid on 07-Oct-17.
 */


public class SignUpPojo {

    private String message;
    private SignUpData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SignUpData getData() {
        return data;
    }

    public void setData(SignUpData data) {
        this.data = data;
    }
}
