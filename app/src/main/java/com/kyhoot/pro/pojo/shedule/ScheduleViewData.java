package com.kyhoot.pro.pojo.shedule;

import java.util.ArrayList;

/**
 * Created by murashid on 05-Jul-17.
 */

public class ScheduleViewData {
    private String _id;
    private String addresssId;
    private String price;
    private String radius;
    private String startDate;
    private String endDate;
    private ArrayList<String> days;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getAddresssId() {
        return addresssId;
    }

    public void setAddresssId(String addresssId) {
        this.addresssId = addresssId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ArrayList<String> getDays() {
        return days;
    }

    public void setDays(ArrayList<String> days) {
        this.days = days;
    }
}
