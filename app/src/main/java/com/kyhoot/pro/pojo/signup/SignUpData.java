package com.kyhoot.pro.pojo.signup;

/**
 * Created by murashid on 07-Oct-17.
 */

public class SignUpData {

    private String providerId;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
