package com.kyhoot.pro.bookingflow.review;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kyhoot.pro.R;
import com.kyhoot.pro.main.MainActivity;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.history.Accounting;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CircleTransform;
import com.kyhoot.pro.utility.DataBaseChat;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import static com.kyhoot.pro.utility.Utility.convertUTCToServerFormat;

public class RateCustomerActivity extends AppCompatActivity implements RateCustomerPresenter.RateCustomerActivityImple, View.OnClickListener {

    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private RateCustomerPresenter presenter;

    private EditText etReview;
    private RatingBar ratingStar;

    private String bookidId ="";
    private String name="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_customer);

        init();
    }

    private void init()
    {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);

        sessionManager = SessionManager.getSessionManager(this);
        presenter = new RateCustomerPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.submitting));
        progressDialog.setCancelable(false);

        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        TextView rateTitle1 = findViewById(R.id.rateTitle1);
        TextView tvDate = findViewById(R.id.tvDate);
        TextView tvPriceLabel = findViewById(R.id.tvPriceLabel);
        TextView tvPrice = findViewById(R.id.tvPrice);
        TextView tvCustomerName = findViewById(R.id.tvCustomerName);
        TextView tvSubmit = findViewById(R.id.tvSubmit);
        etReview = findViewById(R.id.etReview);
        ratingStar = findViewById(R.id.ratingStar);
        ImageView ivCustomer = findViewById(R.id.ivCustomer);

        rateTitle1.setTypeface(fontMedium);
        tvDate.setTypeface(fontBold);
        tvPriceLabel.setTypeface(fontRegular);
        tvPrice.setTypeface(fontBold);
        tvCustomerName.setTypeface(fontMedium);
        tvSubmit.setTypeface(fontBold);
        etReview.setTypeface(fontMedium);

        tvSubmit.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        Booking booking = (Booking) bundle.getSerializable("booking");
        Accounting accounting = booking.getAccounting();

        if(!booking.getProfilePic().equals(""))
        {
            Glide.with(this).
                    load(booking.getProfilePic())
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image)
                    .into(ivCustomer);
        }

        bookidId = booking.getBookingId();
        name = booking.getFirstName() +" "+ booking.getLastName();

        tvCustomerName.setText(booking.getFirstName()+" "+ booking.getLastName()) ;

        try
        {
            String endDate = displayDateFormat.format(serverFormat.parse(convertUTCToServerFormat(booking.getBookingRequestedFor())));
            String[] endDatearr = endDate.split(" ");
            tvDate.setText(Utility.getDayOfMonthSuffix(Integer.parseInt(endDatearr[0]),endDatearr[1]+" "+endDatearr[2]));

            sessionManager.clearChatCountPreference(bookidId);
            DataBaseChat  db = new DataBaseChat(this);
            db.deleteData(bookidId);

            tvPrice.setText(accounting.getTotal());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("showDialog",false);
        startActivity(intent);
        overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvSubmit:
                try {
                    Log.d("mura", "onClick: "+ratingStar.getRating());
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("bookingId",bookidId);
                    jsonObject.put("rating",""+ratingStar.getRating());
                    jsonObject.put("review",etReview.getText().toString());

                    presenter.setCustomerRating(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg) {

        AppController.getInstance().getMixpanelHelper().postingReview(name,bookidId);

        closeActivity();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmptyReview() {
        Toast.makeText(this,getString(R.string.plsEnterReview),Toast.LENGTH_SHORT).show();
    }

}
