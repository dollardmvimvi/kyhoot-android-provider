package com.kyhoot.pro.bookingflow;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kyhoot.pro.R;
import com.kyhoot.pro.bookingflow.review.CustomerReviewsActivity;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.booking.CancelPojo;
import com.kyhoot.pro.pojo.booking.ServiceItem;
import com.kyhoot.pro.pojo.history.Accounting;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CalendarEventHelper;
import com.kyhoot.pro.utility.CircleTransform;
import com.kyhoot.pro.utility.DialogHelper;
import com.kyhoot.pro.utility.MixpanelEvents;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;
import com.kyhoot.pro.utility.WorkaroundMapFragment;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import pub.devrel.easypermissions.EasyPermissions;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>AcceptRejectActivity</h1>
 * Activity for accept and reject jobs
 */
public class AcceptRejectActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, OnMapReadyCallback, EasyPermissions.PermissionCallbacks {

    Runnable runnable;
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private ImageView ivCustomer;
    private RatingBar ratingStar;
    private TextView tvCustomerName;
    private Handler handler;
    private long totalSecs, minutes, seconds;

    private double lat = 0, lng = 0;
    private String bookidId = "";
    private Booking booking;

    private BroadcastReceiver receiver;
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_reject);

        initView();
    }

    /**
     * initialize the views
     */
    @SuppressLint("SetTextI18n")
    private void initView() {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        SimpleDateFormat displayHourFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat displayReapeatDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);

        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontLight = Utility.getFontLight(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        ivCustomer = findViewById(R.id.ivCustomer);
        ratingStar = findViewById(R.id.ratingStar);
        ProgressBar pBRemainingTime = findViewById(R.id.pBRemainingTime);

        TextView tvAddress = findViewById(R.id.tvAddress);
        TextView tvDistance = findViewById(R.id.tvDistance);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        TextView tvRemainingTime = findViewById(R.id.tvRemainingTime);
        TextView tvAccept = findViewById(R.id.tvAccept);
        TextView tvReject = findViewById(R.id.tvReject);
        LinearLayout llCutomerDetails = findViewById(R.id.llCutomerDetails);
        final ImageView ivLocation = findViewById(R.id.ivLocation);

        LinearLayout llService = findViewById(R.id.llService);
        TextView tvTotalBillAmountLabel = findViewById(R.id.tvTotalBillAmountLabel);
        TextView tvTotalBillAmount = findViewById(R.id.tvTotalBillAmount);
        TextView tvDate = findViewById(R.id.tvDate);
        TextView tvTime = findViewById(R.id.tvTime);

        TextView tvPaymentBreakDownLabel = findViewById(R.id.tvPaymentBreakDownLabel);
        TextView tvTravelFeeLabel = findViewById(R.id.tvTravelFeeLabel);
        TextView tvTravelFee = findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = findViewById(R.id.tvVisitFeeLabel);
        TextView tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = findViewById(R.id.tvLastDueLabel);
        TextView tvLastDue = findViewById(R.id.tvLastDue);
        TextView tvDiscountLabel = findViewById(R.id.tvDiscountLabel);
        TextView tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotalLabel = findViewById(R.id.tvTotalLabel);
        TextView tvTotal = findViewById(R.id.tvTotal);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);
        TextView tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
        TextView tvJobDescriptionLabel = findViewById(R.id.tvJobDescriptionLabel);
//        TextView tvJobImageLabel = findViewById(R.id.tvJobImageLabel);
        TextView tvJobDescription = findViewById(R.id.tvJobDescription);
//        ImageView tvJobImage = findViewById(R.id.tvJobImage);

        //multiple shift layout
        LinearLayout llSchedule = findViewById(R.id.llSchedule);

        TextView tvScheduleStartTime = findViewById(R.id.tvScheduleStartTime);
        TextView tvScheduleStartDate = findViewById(R.id.tvScheduleStartDate);
        TextView tvScheduleEndTime = findViewById(R.id.tvScheduleEndTime);
        TextView tvScheduleEndDate = findViewById(R.id.tvScheduleEndDate);
        TextView tvTo = findViewById(R.id.tvTo);
        TextView tvTotalNoOfShift = findViewById(R.id.tvTotalNoOfShift);
        TextView tvShift = findViewById(R.id.tvShift);
        LayoutInflater inflater = getLayoutInflater();


        tvAddress.setTypeface(fontMedium);
        tvDistance.setTypeface(fontRegular);
        tvCustomerName.setTypeface(fontMedium);
        tvRemainingTime.setTypeface(fontLight);
        tvAccept.setTypeface(fontRegular);
        tvReject.setTypeface(fontRegular);

        tvTotalBillAmountLabel.setTypeface(fontMedium);
        tvTotalBillAmount.setTypeface(fontRegular);
        tvDate.setTypeface(fontRegular);
        tvTime.setTypeface(fontRegular);
        tvPaymentBreakDownLabel.setTypeface(fontMedium);
        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontRegular);
        tvTotal.setTypeface(fontRegular);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontRegular);
        tvJobDescriptionLabel.setTypeface(fontMedium);
//        tvJobImageLabel.setTypeface(fontMedium);
        tvJobDescription.setTypeface(fontRegular);

        tvAccept.setOnClickListener(this);
        tvReject.setOnClickListener(this);
        llCutomerDetails.setOnClickListener(this);
        ivLocation.setOnClickListener(this);

        final AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
        WorkaroundMapFragment sMapFrag = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        // Getting GoogleMap object from the fragment
        sMapFrag.getMapAsync(this);
        sMapFrag.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                appBarLayout.requestDisallowInterceptTouchEvent(true);
            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    ivLocation.setVisibility(View.GONE);
                } else if (verticalOffset == 0) {
                    // Expanded
                    ivLocation.setVisibility(View.VISIBLE);
                } else {
                    // Somewhere in between
                }
            }
        });

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");
        Accounting accounting = booking.getAccounting();

        if (!booking.getProfilePic().equals("")) {
            Glide.with(this).
                    load(booking.getProfilePic())
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image)
                    .into(ivCustomer);
        }

        tvCustomerName.setText(booking.getFirstName() + " " + booking.getLastName());
        ratingStar.setRating(Float.parseFloat(booking.getAverageRating()));
        tvAddress.setText(booking.getAddLine1() + " " + booking.getAddLine2());
        tvDistance.setText("" + Utility.getFormattedPrice(String.valueOf(Double.parseDouble(booking.getDistance()) * sessionManager.getDistanceUnitConverter())) + " " +
                sessionManager.getDistanceUnit() + "  " + getString(R.string.away));

        tvTotalBillAmount.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

       /* if(accounting.getTravelFee() !=null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.00") )
        {
            findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }*/
        if (accounting.getVisitFee() != null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00")) {
            findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getLastDues() != null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00")) {
            findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }


        if (accounting.getPaymentMethod().equals("1")) {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash, 0, 0, 0);
        } else {
            tvPaymentMethod.setText(getString(R.string.card) + "  " + accounting.getLast4());
        }

        if (accounting.getPaidByWallet() != null && accounting.getPaidByWallet().equals("1")) {
            tvPaymentMethod.setText(tvPaymentMethod.getText().toString() + " + " + getString(R.string.wallet));
        }

//        if(booking.getJobDescription() !=null && !booking.getJobDescription().equals(""))
//        {
//            findViewById(R.id.viewJobDescription).setVisibility(View.VISIBLE);
//            tvJobDescription.setVisibility(View.VISIBLE);
//            tvJobDescriptionLabel.setVisibility(View.VISIBLE);
//            tvJobDescription.setText(booking.getJobDescription());
//        }
//        if(booking.getJobImage() !=null && !booking.getJobImage().equals(""))
//        {
//            findViewById(R.id.viewJobDescription).setVisibility(View.VISIBLE);
//            tvJobImageLabel.setVisibility(View.VISIBLE);
//            tvJobImage.setVisibility(View.VISIBLE);
//
//            Glide.with(AcceptRejectActivity.this).
//                    load(booking.getJobImage())
//                    .error(R.drawable.profile_default_image)
////                    .transform(new CircleTransform(AcceptRejectActivity.this))
//                    .placeholder(R.drawable.profile_default_image)
//                    .into(tvJobImage);
//        }

        if (booking.getServiceType().equals("1")) {
            for (ServiceItem serviceItem : booking.getCartData()) {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        } else {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            //  tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
            tvServiceFeeLabel.setText(booking.getCategory());
            tvServiceFee.setText(Utility.getPrice(booking.getCartData().get(0).getUnitPrice(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()) + "/hr");
            llService.addView(serviceView);
        }

        try {
            if (booking.getBookingType().equals("3")) {
                findViewById(R.id.llSchedule).setVisibility(View.VISIBLE);
                LinearLayout llSheduleDays = findViewById(R.id.llSheduleDays);
                tvDate.setVisibility(View.GONE);
                tvTime.setVisibility(View.GONE);
                tvTotalBillAmountLabel.setVisibility(View.GONE);
                tvTotalBillAmount.setVisibility(View.GONE);

                TextView tvHoursPerShiftLabel = findViewById(R.id.tvHoursPerShiftLabel);
                TextView tvHoursPerShift = findViewById(R.id.tvHoursPerShift);
                TextView tvCostPerShiftLabel = findViewById(R.id.tvCostPerShiftLabel);
                TextView tvCostPerShift = findViewById(R.id.tvCostPerShift);
                TextView tvTotalShiftLabel = findViewById(R.id.tvTotalShiftLabel);
                TextView tvTotalShift = findViewById(R.id.tvTotalShift);

                tvHoursPerShiftLabel.setTypeface(fontRegular);
                tvHoursPerShift.setTypeface(fontMedium);
                tvCostPerShiftLabel.setTypeface(fontRegular);
                tvCostPerShift.setTypeface(fontMedium);
                tvTotalShiftLabel.setTypeface(fontRegular);
                tvTotalShift.setTypeface(fontMedium);

                try {

                    tvScheduleStartTime.setText(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
                    tvScheduleStartDate.setText(displayReapeatDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
                    tvScheduleEndTime.setText(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingEndtime()))));
                    tvScheduleEndDate.setText(displayReapeatDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingEndtime()))));
                    tvTotalNoOfShift.setText(booking.getAccounting().getTotalShiftBooking());
                    for (String days : booking.getDaysArr()) {
                        TextView tvDay = (TextView) inflater.inflate(R.layout.single_row_text_schedule_day, null);
                        tvDay.setText(days.substring(0, 1));
                        llSheduleDays.addView(tvDay);
                    }
                    double perShiftAmount, totalShiftAmount, perShiftHour;
                    int totalNoOfShift;
                    if (!booking.getBookingModel().equals("3")) {
                        if (booking.getBookingType().equals("3")) {
                            perShiftAmount = Double.parseDouble(booking.getAccounting().getPricePerHour());
                            perShiftHour = (Double.parseDouble(booking.getScheduleTime()) / 60);
                        } else {
                            perShiftAmount = Double.parseDouble(booking.getAccounting().getTotal());
                            perShiftHour = (Double.parseDouble(booking.getAccounting().getTotalActualJobTimeMinutes()) / 60);

                        }
                    } else {
                        perShiftAmount = Double.parseDouble(booking.getAccounting().getPricePerHour());
                        perShiftHour = (Double.parseDouble(booking.getScheduleTime()) / 60);
                    }

                    totalNoOfShift = (int) Double.parseDouble(booking.getAccounting().getTotalShiftBooking());
                    totalShiftAmount = totalNoOfShift * perShiftAmount;

                    tvTotalShift.setText(Utility.getPrice(String.valueOf(totalShiftAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                    tvHoursPerShift.setText(String.valueOf(perShiftHour) + " " + getString(R.string.hrsCaps));
                    tvCostPerShift.setText(Utility.getPrice(String.valueOf(perShiftAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                llSchedule.setVisibility(View.GONE);
                tvDate.setVisibility(View.VISIBLE);
                tvTime.setVisibility(View.VISIBLE);

                tvDate.setText(getString(R.string.date) + " : " + displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
                tvTime.setText(getString(R.string.time) + " : " + displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        setTimer(pBRemainingTime, tvRemainingTime, booking.getBookingRequestedForProvider(), booking.getBookingExpireForProvider(), booking.getCurrentTime());


        if (!booking.getLatitude().equals("")) {
            lat = Double.parseDouble(booking.getLatitude());
            lng = Double.parseDouble(booking.getLongitude());
        }

        bookidId = booking.getBookingId();

        CollapsingToolbarLayout ctlMyBooking = findViewById(R.id.ctlMyBooking);

        ctlMyBooking.setTitle(getText(R.string.jobId) + " " + booking.getBookingId());
        ctlMyBooking.setCollapsedTitleTextAppearance(R.style.TextCollapsedEventId);
        ctlMyBooking.setExpandedTitleTextAppearance(R.style.TextExpandHide);
        ctlMyBooking.setCollapsedTitleGravity(Gravity.CENTER);

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String cancelId = intent.getStringExtra("cancelid");
                String msg = intent.getStringExtra("msg");
                String header = intent.getStringExtra("header");
                if (cancelId.equals(booking.getBookingId())) {
                    VariableConstant.IS_BOOKING_UPDATED = true;
                    //closeActivity();
                    DialogHelper.customAlertDialogCloseActivity(AcceptRejectActivity.this, header, msg, getString(R.string.oK));
                }
            }
        };

        registerReceiver(receiver, filter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String[] perms = {Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR};

        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, getString(R.string.write_calendar_permission_message),
                    1000, perms);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            DialogHelper.aDialogOnPermissionDenied(this);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.write_calendar_permission_message),
                    1000, perms.toArray(new String[perms.size()]));
        }
    }

    /**
     * starting runnable for ca
     *
     * @param pBRemainingTime  Progresslevel in second
     * @param tvRemainingTime  textview for showing remainning time
     * @param bookingStartTime booking started timee
     * @param bookingEndTime   booking expiring time
     */
    private void setTimer(final ProgressBar pBRemainingTime, final TextView tvRemainingTime, String bookingStartTime, String bookingEndTime, long current) {

        long start = Utility.convertUTCToTimeStamp(bookingStartTime);
        long end = Utility.convertUTCToTimeStamp(bookingEndTime);

        totalSecs = (end - start) / 1000;
        minutes = (totalSecs % 3600) / 60;

        pBRemainingTime.setMax((int) totalSecs);

        totalSecs = (end - current) / 1000;
        minutes = (totalSecs % 3600) / 60;
        seconds = totalSecs % 60;
        minutes = minutes % 60;
        tvRemainingTime.setText(String.format("%02d", minutes) + ":" + String.format("%02d", seconds));

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                totalSecs = totalSecs - 1;
                minutes = (totalSecs % 3600) / 60;
                seconds = totalSecs % 60;
                minutes = minutes % 60;

                tvRemainingTime.setText(String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                pBRemainingTime.setProgress((int) totalSecs);

                handler.postDelayed(this, 1000);

            }
        };
        handler.post(runnable);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
        unregisterReceiver(receiver);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        try {
            progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(String msg) {
        closeActivity();
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this, getString(R.string.alert), msg, getString(R.string.oK));
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAccept:
                try {

                    String reminderId = "";
                    if (booking.getBookingType().equals("2")) {
                        CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                        reminderId = String.valueOf(calendarEventHelper.addEvent(booking));
                    }

                    JSONObject acceptJsonObject = new JSONObject();
                    acceptJsonObject.put("bookingId", bookidId);
                    acceptJsonObject.put("status", VariableConstant.ACCEPT);
                    acceptJsonObject.put("latitude", sessionManager.getCurrentLat());
                    acceptJsonObject.put("longitude", sessionManager.getCurrentLng());
                    acceptJsonObject.put("reminderId", reminderId);

                    VariableConstant.IS_BOOKING_UPDATED = true;
                    VariableConstant.IS_BOOKING_ACCEPTED = true;

                    progressDialog.setMessage(getString(R.string.updatingStatusForAccept));
                    presenter.acceptRejectJob(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), acceptJsonObject);

                    AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.BookingAccept.value, bookidId);

                    if (booking.getBookingType().equals("1")) {
                        AppController.getInstance().getMixpanelHelper().timeEndForNowBookingRespond(booking.getFirstName() + " " + booking.getLastName()
                                , bookidId, "Accepted");
                    } else {
                        AppController.getInstance().getMixpanelHelper().timeEndForLaterBookingRespond(booking.getFirstName() + " " + booking.getLastName()
                                , bookidId, "Accepted");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.tvReject:
                try {
                    JSONObject rejectJsonObject = new JSONObject();
                    rejectJsonObject.put("bookingId", bookidId);
                    rejectJsonObject.put("status", VariableConstant.REJECT);
                    rejectJsonObject.put("latitude", sessionManager.getCurrentLat());
                    rejectJsonObject.put("longitude", sessionManager.getCurrentLng());

                    VariableConstant.IS_BOOKING_UPDATED = true;

                    progressDialog.setMessage(getString(R.string.updatingStatusForDecline));
                    presenter.acceptRejectJob(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), rejectJsonObject);

                    AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.BookingDecling.value, bookidId);
                    if (booking.getBookingType().equals("1")) {
                        AppController.getInstance().getMixpanelHelper().timeEndForNowBookingRespond(booking.getFirstName() + " " + booking.getLastName()
                                , bookidId, "Rejected");
                    } else {
                        AppController.getInstance().getMixpanelHelper().timeEndForLaterBookingRespond(booking.getFirstName() + " " + booking.getLastName()
                                , bookidId, "Rejected");
                    }

                    sessionManager.setLastBookingId("");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.llCutomerDetails:
                Intent intent = new Intent(this, CustomerReviewsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("booking", booking);
                intent.putExtras(bundle);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.ivLocation:
                if (googleMap != null) {
                    LatLng latLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng()));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLng latLng = new LatLng(lat, lng);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_blue_dot_icon)));
        googleMap.getUiSettings().setCompassEnabled(false);
    }
}
