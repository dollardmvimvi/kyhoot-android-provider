package com.kyhoot.pro.bookingflow.review;

import android.util.Log;

import com.kyhoot.pro.utility.OkHttp3ConnectionStatusCode;
import com.kyhoot.pro.utility.ServiceUrl;
import com.kyhoot.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 06-Nov-17.
 * Model for RaterCustomerActivity
 * @see RateCustomerActivity
 */

public class RateCustomerModel {
    private static final String TAG = "ReviewModel";
    private RateCustomerModelImple modelImplement;

    RateCustomerModel(RateCustomerModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for set the customer reviews based on the page index and customer details
     * @param sessiontoken session Token
     */
    void setCustomerRating(String sessiontoken,JSONObject jsonObject)
    {
        try {
          /*  if(jsonObject.getString("review").equals(""))
            {
                modelImplement.onEmptyReview();
                return;
            }*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.REVIEW_AND_RATING, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(jsonResponse.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonResponse.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * ReviewModelImple interface for Presenter implementation
     */
    interface RateCustomerModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onEmptyReview();
        void onSuccess(String msg);
    }
}
