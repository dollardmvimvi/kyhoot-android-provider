package com.kyhoot.pro.bookingflow;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kyhoot.pro.R;
import com.kyhoot.pro.bookingflow.review.CustomerReviewsActivity;
import com.kyhoot.pro.bookingflow.review.RateCustomerActivity;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.booking.CancelPojo;
import com.kyhoot.pro.pojo.booking.ServiceItem;
import com.kyhoot.pro.pojo.history.Accounting;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CircleTransform;
import com.kyhoot.pro.utility.DecimalDigitsInputFilter;
import com.kyhoot.pro.utility.MixpanelEvents;
import com.kyhoot.pro.utility.MyImageHandler;
import com.kyhoot.pro.utility.ServiceUrl;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.UploadFileAmazonS3;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pub.devrel.easypermissions.EasyPermissions;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>InvoiceActivity</h1>
 * InvoiceActivity for showing the invoice detatils and take signature from customer
 */
public class InvoiceActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = "InvoiceActivity";
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private ImageView ivCustomer;
    private RatingBar ratingStar;
    private TextView tvCustomerName;
    private SeekBar seekBar;

    private SignaturePad signaturePad;
    private  File signatureFile;
    private Bitmap signatureBitmap;

    private Booking booking;

    private Typeface fontRegular;
    private LinearLayout llAddExtraItem, llSignature;
    private LayoutInflater inflater;
    private TextView tvAddNewItem, tvConfirm, tvTotalBillAmount, tvTotal;
    private ArrayList<EditText> etAmounts, etExtraFees;
    private double total=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);

        initView();
    }

    @SuppressLint("SetTextI18n")
    private void initView()
    {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        SimpleDateFormat displayHourFormat = new SimpleDateFormat("hh:mm a", Locale.US);

        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updatingStatusRaiseInvoice));
        progressDialog.setCancelable(false);

        Typeface fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.cusomerSignature));
        tvTitle.setTypeface(fontRegular);

        ivCustomer = findViewById(R.id.ivCustomer);
        ratingStar = findViewById(R.id.ratingStar);

        tvCustomerName = findViewById(R.id.tvCustomerName);

        etAmounts = new ArrayList<>();
        etExtraFees = new ArrayList<>();

        TextView tvSignatureHeader = findViewById(R.id.tvSignatureHeader);
        TextView tvRetake = findViewById(R.id.tvRetake);
        seekBar = findViewById(R.id.seekBar);
        signaturePad = findViewById(R.id.signaturePad);
        TextView tvSeekbarText = findViewById(R.id.tvSeekbarText);

        LinearLayout llService = findViewById(R.id.llService);
        TextView tvTotalBillAmountLabel = findViewById(R.id.tvTotalBillAmountLabel);
        tvTotalBillAmount = findViewById(R.id.tvTotalBillAmount);
        TextView tvDate = findViewById(R.id.tvDate);
        TextView tvTime = findViewById(R.id.tvTime);
        TextView tvPaymentBreakDownLabel = findViewById(R.id.tvPaymentBreakDownLabel);
        TextView tvTravelFeeLabel = findViewById(R.id.tvTravelFeeLabel);
        TextView tvTravelFee = findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = findViewById(R.id.tvVisitFeeLabel);
        TextView tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = findViewById(R.id.tvLastDueLabel);
        TextView tvLastDue = findViewById(R.id.tvLastDue);
        TextView tvDiscountLabel = findViewById(R.id.tvDiscountLabel);
        TextView tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotalLabel = findViewById(R.id.tvTotalLabel);
        tvTotal = findViewById(R.id.tvTotal);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);
        TextView tvPaymentMethod = findViewById(R.id.tvPaymentMethod);

        llAddExtraItem =  findViewById(R.id.llAddExtraItem);
        llSignature =  findViewById(R.id.llSignature);
        tvConfirm =  findViewById(R.id.tvConfirm);
        tvAddNewItem = findViewById(R.id.tvAddNewItem);
        inflater = LayoutInflater.from(this);

        tvSignatureHeader.setTypeface(fontMedium);
        tvRetake.setTypeface(fontMedium);
        tvCustomerName.setTypeface(fontMedium);
        tvSeekbarText.setTypeface(fontBold);

        tvTotalBillAmountLabel.setTypeface(fontMedium);
        tvTotalBillAmount.setTypeface(fontRegular);
        tvDate.setTypeface(fontRegular);
        tvTime.setTypeface(fontRegular);
        tvPaymentBreakDownLabel.setTypeface(fontMedium);
        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontRegular);
        tvTotal.setTypeface(fontRegular);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontRegular);
        tvAddNewItem.setTypeface(fontMedium);
        tvConfirm.setTypeface(fontMedium);


        tvAddNewItem.setVisibility(View.VISIBLE);
        tvAddNewItem.setOnClickListener(this);
        tvConfirm.setOnClickListener(this);
        tvRetake.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");
        Accounting accounting = booking.getAccounting();

        if(!booking.getProfilePic().equals(""))
        {
            Glide.with(this).
                    load(booking.getProfilePic())
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image)
                    .into(ivCustomer);
        }

        tvCustomerName.setText(booking.getFirstName()+" "+booking.getLastName()) ;

        tvTotalBillAmount.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        total = Double.parseDouble(accounting.getTotal());

        if(accounting.getTravelFee() !=null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.0") && !accounting.getTravelFee().equals("0.00") )
        {
            findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if(accounting.getVisitFee() !=null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00"))
        {
            findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if(accounting.getLastDues() !=null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00"))
        {
            findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if(accounting.getPaymentMethod().equals("1"))
        {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash,0,0,0);
        }
        else
        {
            tvPaymentMethod.setText(getString(R.string.card)+"  "+accounting.getLast4());
        }

        if(accounting.getPaidByWallet() !=null && accounting.getPaidByWallet().equals("1"))
        {
            tvPaymentMethod.setText(tvPaymentMethod.getText().toString() + " + "+ getString(R.string.wallet));
        }

        if(booking.getServiceType().equals("1"))
        {
            for(ServiceItem serviceItem : booking.getCartData())
            {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees,null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName() +" X "+ serviceItem.getQuntity());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        }
        else
        {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees,null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
            tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llService.addView(serviceView);
        }

        try {
            tvDate.setText(getString(R.string.date)+" : "+displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            tvTime.setText(getString(R.string.time)+" : "+displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));

            ratingStar.setRating(Float.parseFloat(booking.getAverageRating()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        final File dir = myImageHandler.getAlbumStorageDir(this, VariableConstant.SIGNATURE_PIC_DIR, true);
        signatureFile = new File(dir, booking.getBookingId()+".jpg");

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                signatureBitmap = signaturePad.getSignatureBitmap();
            }

            @Override
            public void onClear()
            {
                Toast.makeText(InvoiceActivity.this, getString(R.string.cleared), Toast.LENGTH_SHORT).show();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (seekBar.getProgress() > 75)
                {
                    if(!signaturePad.isEmpty())
                    {
                        seekBar.setProgress(100);
                        try {
                            Utility.saveImage(signatureBitmap,signatureFile);

                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            Uri contentUri = Uri.fromFile(signatureFile);
                            mediaScanIntent.setData(contentUri);
                            sendBroadcast(mediaScanIntent);

                            progressDialog.setMessage(getString(R.string.uploading));
                            progressDialog.show();
                            new UploadFileToServer().execute(signatureFile.getPath());
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            seekBar.setProgress(0);
                        }
                    }
                    else
                    {
                        seekBar.setProgress(0);
                        Toast.makeText(InvoiceActivity.this,getString(R.string.plsProvideSignature),Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    seekBar.setProgress(0);
                }

            }
        });

        sessionManager.clearDistance(booking.getBookingId());

        EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                1000, VariableConstant.STORAGE_CAMERA_PERMISSION);

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;

        AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.BookingCancelled.value,booking.getBookingId());

        finish();
        sessionManager.setBookingStr(presenter.getBookingStr(booking,sessionManager.getBookingStr(),VariableConstant.JOB_COMPLETED_RAISE_INVOICE));
        booking.getAccounting().setTotal(tvTotal.getText().toString());

        Intent intent = new Intent(this,RateCustomerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("booking",booking);
        intent.putExtras(bundle);

        startActivity(intent);
        overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);

        AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.RaiseInvoice.value,booking.getBookingId());
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.llCutomerDetails:
                Intent intent = new Intent(this,CustomerReviewsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){

                    startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.tvRetake:
                signaturePad.clear();
                break;

            case R.id.tvDone:
                break;

            case R.id.tvAddNewItem:
                llSignature.setVisibility(View.GONE);
                tvConfirm.setVisibility(View.VISIBLE);
                addRemoveDynamicView();
                break;

            case R.id.tvConfirm:
                if(isValidExtraFees())
                {
                    tvConfirm.setVisibility(View.GONE);
                    llSignature.setVisibility(View.VISIBLE);
                    tvAddNewItem.setVisibility(View.GONE);
                    addStaticView();
                }
                break;

        }
    }

    private boolean isValidExtraFees()
    {
        for (int i=0 ; i < etAmounts.size() ; i++)
        {
            if(etExtraFees.get(i).getText().toString().equals(""))
            {
                Toast.makeText(this,getString(R.string.plsEnterServiceName),Toast.LENGTH_SHORT).show();
                return false;
            }
            else if(etAmounts.get(i).getText().toString().equals(""))
            {
                Toast.makeText(this,getString(R.string.plsEnterPrice),Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    private void addRemoveDynamicView() {
        final View view =inflater.inflate(R.layout.add_new_item_invoice_view,null);
        llAddExtraItem.addView(view);
        ImageView ivDelete = view.findViewById(R.id.ivDelete);
        final EditText etAmount = view.findViewById(R.id.etAmount);
        final EditText etExtraFee = view.findViewById(R.id.etExtraFee);
        final TextView tvCurrencySymbol = view.findViewById(R.id.tvCurrencySymbol);
        final TextView tvCurrencySymbolSuffix = view.findViewById(R.id.tvCurrencySymbolSuffix);
        etAmount.setTypeface(fontRegular);
        etAmount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});
        etExtraFee.setTypeface(fontRegular);
        tvCurrencySymbol.setTypeface(fontRegular);
        tvCurrencySymbolSuffix.setTypeface(fontRegular);
        tvCurrencySymbol.setText(sessionManager.getCurrencySymbol());
        tvCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());

        etAmounts.add(etAmount);
        etExtraFees.add(etExtraFee);

        if(sessionManager.getCurrencyAbbrevation().equals("2"))
        {
            tvCurrencySymbolSuffix.setVisibility(View.VISIBLE);
            tvCurrencySymbol.setVisibility(View.GONE);
        }

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddExtraItem.removeView(view);
                etAmounts.remove(etAmount);
                etExtraFees.remove(etExtraFee);

                if(etAmounts.size() == 0)
                {
                    tvConfirm.setVisibility(View.GONE);
                    llSignature.setVisibility(View.VISIBLE);
                }

                calculateTotal();
            }
        });

        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                calculateTotal();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b)
                {
                    etAmount.setText(Utility.getFormattedPrice(etAmount.getText().toString()));
                }
                else
                {
                    etAmount.setCursorVisible(true);
                }
            }
        });

        etAmount.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    etAmount.setCursorVisible(false);
                    return true;
                }
                return false;
            }
        });

    }

    private void addStaticView() {
        llAddExtraItem.removeAllViews();
        for (int i=0 ; i < etAmounts.size() ; i++)
        {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees,null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(etExtraFees.get(i).getText().toString());
            tvServiceFee.setText(Utility.getPrice(etAmounts.get(i).getText().toString(),sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llAddExtraItem.addView(serviceView);
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i=0 ; i < etAmounts.size() ; i++)
            {
                JSONObject jsonObject1 = new JSONObject();

                jsonObject1.put("serviceName",etExtraFees.get(i).getText().toString());
                jsonObject1.put("price",etAmounts.get(i).getText().toString());
                jsonArray.put(jsonObject1);
            }
            jsonObject.put("additionalService",jsonArray);
            Log.d(TAG, "addStaticView: "+jsonObject);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void calculateTotal() {
        double serviceTotal = 0;
        for (EditText etAmount : etAmounts)
        {
            if(!etAmount.getText().toString().equals(""))
            {
                try {
                    serviceTotal += Double.parseDouble(etAmount.getText().toString());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        tvTotal.setText(Utility.getPrice(String.valueOf(total+serviceTotal), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotalBillAmount.setText(tvTotal.getText().toString());
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble( sessionManager.getCurrentLng()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_blue_dot_icon)));
        googleMap.getUiSettings().setCompassEnabled(false);
    }

    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result, responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(60, TimeUnit.SECONDS);
                builder.readTimeout(60, TimeUnit.SECONDS);
                builder.writeTimeout(60, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(ServiceUrl.IMAGE_UPLOAD_ON_SERVER)
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d(TAG, "doInBackground: " + responseCode);
                Log.d(TAG, "doInBackground: " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            progressDialog.dismiss();
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject1 = new JSONObject(result[1]);
                    String image = jsonObject1.getString("data");
                    UpdateStatus(image);

                } else {
                    UpdateStatus("");
                    Toast.makeText(InvoiceActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                UpdateStatus("");
                Toast.makeText(InvoiceActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }

    }

    private void UpdateStatus(String imageUrl){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("bookingId", booking.getBookingId());
            jsonObject.put("status", VariableConstant.JOB_COMPLETED_RAISE_INVOICE);
            jsonObject.put("latitude", sessionManager.getCurrentLat());
            jsonObject.put("longitude", sessionManager.getCurrentLng());
            jsonObject.put("longitude", sessionManager.getCurrentLng());
            jsonObject.put("signatureUrl", imageUrl);

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < etAmounts.size(); i++) {
                JSONObject jsonObjectExtra = new JSONObject();
                jsonObjectExtra.put("serviceName", etExtraFees.get(i).getText().toString());
                jsonObjectExtra.put("price", etAmounts.get(i).getText().toString());
                jsonArray.put(jsonObjectExtra);
            }

            jsonObject.put("additionalService", jsonArray);
            Log.d(TAG, "onStopTrackingTouch: " + jsonObject);
            presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject, signatureFile);
        }
        catch (Exception e){
            e.getMessage();
        }


    }
}
