package com.kyhoot.pro.bookingflow.review;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kyhoot.pro.R;
import com.kyhoot.pro.adapters.ReviewsListAdapter;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.profile.review.ReviewData;
import com.kyhoot.pro.pojo.profile.review.Reviews;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CircleTransform;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;

import java.util.ArrayList;

/**
 * Created by murashid on 01-Nov-17.
 * <h1>CustomerReviewsActivity</h1>
 * CustomerReviewsActivity activity for showing the customer Reviews and customer details
 */
public class CustomerReviewsActivity extends AppCompatActivity implements CustomerReviewsPresenter.CustomerReviewsPresenterImple, View.OnClickListener {

    private SessionManager sessionManager;
    private ReviewsListAdapter reviewsListAdapter;
    private CustomerReviewsPresenter presenter;
    private ArrayList<Reviews> reviewses;
    int i = 0;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean toLoad = true;
    private String customerId ;

    private ProgressBar pBReview;
    private TextView tvAboutMe,tvReadMoreLess;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_reviews);

        init();
    }

    private void  init()
    {
        presenter = new CustomerReviewsPresenter(this);
        sessionManager  = SessionManager.getSessionManager(this);
        reviewses = new ArrayList<>();

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            //getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_x_color_primary);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.reviewsHeader));
        tvTitle.setTypeface(fontBold);

        RatingBar ratingStar = findViewById(R.id.ratingStar);
        pBReview = findViewById(R.id.pBReview);
        TextView tvCustomerName = findViewById(R.id.tvCustomerName);
        TextView tvAboutMeLable = findViewById(R.id.tvAboutMeLable);
        ImageView ivCustomer = findViewById(R.id.ivCustomer);
        tvAboutMe = findViewById(R.id.tvAboutMe);
        tvReadMoreLess = findViewById(R.id.tvReadMoreLess);

        tvCustomerName.setTypeface(fontRegular);
        tvAboutMeLable.setTypeface(fontMedium);
        tvAboutMe.setTypeface(fontRegular);
        tvReadMoreLess.setTypeface(fontRegular);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        RecyclerView rvCustomerReviews = findViewById(R.id.rvCustomerReviews);
        rvCustomerReviews.setLayoutManager(linearLayoutManager);
        reviewsListAdapter = new ReviewsListAdapter(this,reviewses);
        rvCustomerReviews.setAdapter(reviewsListAdapter);

        ImageView ivCloseButton = findViewById(R.id.ivCloseButton);
        ivCloseButton.setVisibility(View.VISIBLE);
        ivCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
            }
        });

        Bundle bundle = getIntent().getExtras();
        Booking booking = (Booking) bundle.getSerializable("booking");

        tvCustomerName.setText(booking.getFirstName()+" "+booking.getLastName()) ;
        ratingStar.setRating(Float.parseFloat(booking.getAverageRating()));
        customerId = booking.getCustomerId();


        tvReadMoreLess.setOnClickListener(this);
        if(!booking.getProfilePic().equals(""))
        {
            Glide.with(this).
                    load(booking.getProfilePic())
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image)
                    .into(ivCustomer);
        }

        rvCustomerReviews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (toLoad)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            toLoad = false;
                            i++;
                            presenter.getReview(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),i,customerId);
                        }
                    }
                }
            }
        });
        presenter.getReview(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),i,customerId);

        AppController.getInstance().getMixpanelHelper().viewCustomerReview(booking.getFirstName() +" "+ booking.getLastName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
        }
    }

    @Override
    public void onSuccess(ReviewData reviewData) {
        reviewses.addAll(reviewData.getReviews());
        reviewsListAdapter.notifyDataSetChanged();

        if(i == 0)
        {
            i++;
            tvAboutMe.setText(reviewData.getAbout());

            tvAboutMe.post(new Runnable() {
                @Override
                public void run() {
                    int lineCount = tvAboutMe.getLineCount();
                    if(lineCount > 3)
                    {
                        tvReadMoreLess.setVisibility(View.VISIBLE);
                        tvAboutMe.setMaxLines(3);
                        tvReadMoreLess.setText(getText(R.string.readMore));
                    }
                }
            });

            presenter.getReview(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),i,customerId);
        }
    }

    @Override
    public void startProgressBar() {
        pBReview.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopProgressBar() {
        toLoad = true;
        pBReview.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tvReadMoreLess:
                if(tvReadMoreLess.getText().toString().equals(getString(R.string.readMore)))
                {
                    tvReadMoreLess.setText(getString(R.string.readLess));
                    tvAboutMe.setMaxLines(100);
                }
                else
                {
                    tvReadMoreLess.setText(getString(R.string.readMore));
                    tvAboutMe.setMaxLines(3);
                }
                break;
        }
    }
}
