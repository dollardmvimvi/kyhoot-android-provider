package com.kyhoot.pro.bookingflow.review;

import org.json.JSONObject;

/**
 * Created by murashid on 06-Nov-17.
 * <h1>RateCustomerPresenter</h1>
 * RateCustomerPresenter presenter for RateCustomerActivity
 * @see RateCustomerActivity
 */

public class RateCustomerPresenter implements RateCustomerModel.RateCustomerModelImple {
    private RateCustomerModel model;
    private RateCustomerActivityImple presenterImple;

    RateCustomerPresenter(RateCustomerActivityImple presenterImple) {
        model = new RateCustomerModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing value from view to model
     * @param sessionToken session Token
     */
    void setCustomerRating(String sessionToken, JSONObject jsonObject){
        presenterImple.startProgressBar();
        model.setCustomerRating(sessionToken, jsonObject);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onEmptyReview() {
        presenterImple.stopProgressBar();
        presenterImple.onEmptyReview();
    }

    @Override
    public void onSuccess(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(msg);
    }

    /**
     * interface for view implementation
     */
    interface RateCustomerActivityImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onEmptyReview();
        void onSuccess(String msg);
    }

}
