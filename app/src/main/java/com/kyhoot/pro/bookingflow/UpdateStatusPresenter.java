package com.kyhoot.pro.bookingflow;


import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.booking.CancelPojo;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.UploadFileAmazonS3;

import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;


/**
 * Created by murashid on 31-Oct-17.
 * <h1>UpdateStatusPresenter</h1>
 * UpdateStatusPresenter presenter for update status in AcceptRejectAcitivty
 * @see AcceptRejectActivity
 */

public class UpdateStatusPresenter implements UpdateStatusModel.UpdateStatusModelImple {
    private UpdateStatusModel model;
    private UpdateStatusPresenterImple presenterImple;

    UpdateStatusPresenter(UpdateStatusPresenterImple presenterImple) {
        model = new UpdateStatusModel(this);
        this.presenterImple = presenterImple;
    }

    void acceptRejectJob(String sessionToken,JSONObject jsonObject)
    {
        presenterImple.startProgressBar();
        model.acceptRejectJob(sessionToken,jsonObject);
    }

    void updateTimer(String sessionToken, final JSONObject jsonObject)
    {
        presenterImple.startProgressBar();
        model.updateTimer(sessionToken,jsonObject,false);
    }

    /**
     * Method for calling api for update the job status
     * @param sessionToken session Token
     */
    void updateStaus(final String sessionToken, final JSONObject jsonObject, final  JSONObject jsonObjectTimer)
    {
        presenterImple.startProgressBar();
        model.updateStaus(sessionToken,jsonObject,jsonObjectTimer,true);
    }

    void updateStaus(final String sessionToken, final JSONObject jsonObject,  final File mFileTemp)
    {
        presenterImple.startProgressBar();
        model.updateStaus(sessionToken,jsonObject,mFileTemp);
    }

    void updateStaus(final String sessionToken, final JSONObject jsonObject)
    {
        presenterImple.startProgressBar();
        model.updateStaus(sessionToken,jsonObject);
    }

    String getBookingStr(Booking booking,String oldStr,String updatedStatus )
    {
        return  oldStr.replace(booking.getBookingId()+"|"+booking.getStatus(),booking.getBookingId()+"|"+ updatedStatus);
    }

    /**
     * method for returning hours and minutes from seconds
     * @param seconds second
     * @return hours and minutes
     */
    String getDurationString(long seconds)
    {
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        int hours = (int) (TimeUnit.SECONDS.toHours(seconds) - (day *24));
        int minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60));
        int second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60));

        String timer=String.format("%02d",hours)+" : "+String.format("%02d",minute)+" : "+String.format("%02d",second);

        return timer;
    }

    long getTimeWhileInBackground(SessionManager sessionManager)
    {
        return  sessionManager.getElapsedTime() + (long) (Math.round((System.currentTimeMillis() - sessionManager.getTimeWhilePaused()) / 1000f));
    }

    void getCancelReason()
    {
        presenterImple.startProgressBar();
        model.getCancelReasons();
    }

    void cancelBooking(String sessionToken,JSONObject jsonObject)
    {
        model.cancelBooking(sessionToken,jsonObject);
        presenterImple.startProgressBar();
    }

    @Override
    public void onSuccess(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(msg);
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessCancelReason(cancelPojo);
    }

    @Override
    public void onCancelBooking(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onCancelBooking(msg);
    }

    @Override
    public void onSuccessTimer() {
        presenterImple.stopProgressBar();
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onNewToken(String newToken) {
        presenterImple.onNewToken(newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.sessionExpired(msg);
    }


    interface UpdateStatusPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onSuccess(String msg);
        void onSuccessCancelReason(CancelPojo cancelPojo);
        void onCancelBooking(String msg);
        void onFailure(String failureMsg);
        void onFailure();
        void onNewToken(String newToken);
        void sessionExpired(String msg);
    }
}
