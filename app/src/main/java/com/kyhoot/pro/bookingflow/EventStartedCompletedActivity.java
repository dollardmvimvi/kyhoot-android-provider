package com.kyhoot.pro.bookingflow;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kyhoot.pro.R;
import com.kyhoot.pro.bookingflow.review.CustomerReviewsActivity;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.booking.BookingTimer;
import com.kyhoot.pro.pojo.booking.CancelPojo;
import com.kyhoot.pro.pojo.booking.ServiceItem;
import com.kyhoot.pro.pojo.history.Accounting;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CalendarEventHelper;
import com.kyhoot.pro.utility.CircleTransform;
import com.kyhoot.pro.utility.DialogHelper;
import com.kyhoot.pro.utility.MixpanelEvents;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>EventStartedCompletedActivity</h1>
 * Activity for starting and enting the event time
 */
public class EventStartedCompletedActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener {

    private static final String TAG = "EventStarted";
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private ImageView ivCustomer;
    private RatingBar ratingStar;
    private TextView tvCustomerName, tvStartPauseTimer ,tvTimer, tvStatus;
    TextView tvSeekbarText;
    private SeekBar seekBar;

    private long timerSecond =0;
    private Handler handler;
    private Runnable myRunnable;
    boolean runTimer = false,isRunning = false;

    private TextView tvMessageCount;

    private Booking booking;
    private BookingTimer bookingTimer;

    private String bookidId ="", timerStatus = "0",updatingStatus = "";

    private BroadcastReceiver receiver;

    private int normalTextColor,lightestTextColor;

    private double hourFee = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_started_completed);

        initView();
    }


    @SuppressLint("SetTextI18n")
    private void initView()
    {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        SimpleDateFormat displayHourFormat = new SimpleDateFormat("hh:mm a", Locale.US);

        normalTextColor = ContextCompat.getColor(EventStartedCompletedActivity.this,R.color.normalTextColor);
        lightestTextColor = ContextCompat.getColor(EventStartedCompletedActivity.this,R.color.lightestTextColor);

        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updatingStatus));
        progressDialog.setCancelable(false);

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontLight = Utility.getFontLight(this);

        ivCustomer = findViewById(R.id.ivCustomer);
        ratingStar = findViewById(R.id.ratingStar);

        TextView tvGigTimeHeader = findViewById(R.id.tvGigTimeHeader);
        tvTimer = findViewById(R.id.tvTimer);
        TextView tvHour = findViewById(R.id.tvHour);
        TextView tvMinute = findViewById(R.id.tvMinute);
        TextView tvSecond = findViewById(R.id.tvMinute);
        tvStartPauseTimer = findViewById(R.id.tvStartPauseTimer);

        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvSeekbarText = findViewById(R.id.tvSeekbarText);
        tvMessageCount = findViewById(R.id.tvMessageCount);
        TextView tveventId = findViewById(R.id.tveventId);
        tvStatus = findViewById(R.id.tvStatus);
        LinearLayout llCutomerDetails = findViewById(R.id.llCutomerDetails);
        seekBar = findViewById(R.id.seekBar);

        LinearLayout llService = findViewById(R.id.llService);
        TextView tvTotalBillAmountLabel = findViewById(R.id.tvTotalBillAmountLabel);
        TextView tvTotalBillAmount = findViewById(R.id.tvTotalBillAmount);
        TextView tvDate = findViewById(R.id.tvDate);
        TextView tvTime = findViewById(R.id.tvTime);
        TextView tvPaymentBreakDownLabel = findViewById(R.id.tvPaymentBreakDownLabel);
        TextView tvTravelFeeLabel = findViewById(R.id.tvTravelFeeLabel);
        TextView tvTravelFee = findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = findViewById(R.id.tvVisitFeeLabel);
        TextView tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = findViewById(R.id.tvLastDueLabel);
        TextView tvLastDue = findViewById(R.id.tvLastDue);
        TextView tvDiscountLabel = findViewById(R.id.tvDiscountLabel);
        TextView tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotalLabel = findViewById(R.id.tvTotalLabel);
        TextView tvTotal = findViewById(R.id.tvTotal);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);
        TextView tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
        TextView tvJobDescriptionLabel = findViewById(R.id.tvJobDescriptionLabel);
        TextView tvJobDescription = findViewById(R.id.tvJobDescription);
        LayoutInflater inflater = getLayoutInflater();

        tveventId.setTypeface(fontMedium);
        tvStatus.setTypeface(fontMedium);
        tvGigTimeHeader.setTypeface(fontMedium);
        tvTimer.setTypeface(fontLight);
        tvHour.setTypeface(fontRegular);
        tvMinute.setTypeface(fontRegular);
        tvSecond.setTypeface(fontRegular);
        tvStartPauseTimer.setTypeface(fontBold);
        tvCustomerName.setTypeface(fontMedium);
        tvMessageCount.setTypeface(fontMedium);
        tvSeekbarText.setTypeface(fontBold);

        tvTotalBillAmountLabel.setTypeface(fontMedium);
        tvTotalBillAmount.setTypeface(fontRegular);
        tvDate.setTypeface(fontRegular);
        tvTime.setTypeface(fontRegular);
        tvPaymentBreakDownLabel.setTypeface(fontMedium);
        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontRegular);
        tvTotal.setTypeface(fontRegular);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontRegular);
        tvJobDescriptionLabel.setTypeface(fontMedium);
        tvJobDescription.setTypeface(fontRegular);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ImageView ivCall = findViewById(R.id.ivCall);
        ImageView ivMessage = findViewById(R.id.ivMessage);

        ivBackButton.setOnClickListener(this);
        ivCall.setOnClickListener(this);
        ivMessage.setOnClickListener(this);
        tvStartPauseTimer.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");
        bookingTimer = booking.getBookingTimer();
        Accounting accounting = booking.getAccounting();

        if(!booking.getProfilePic().equals(""))
        {
            Glide.with(this).
                    load(booking.getProfilePic())
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image)
                    .into(ivCustomer);
        }

        tvCustomerName.setText(booking.getFirstName()+" "+booking.getLastName()) ;

        tvTotalBillAmount.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation() ));
        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if(accounting.getTravelFee() !=null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.00") )
        {
            findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if(accounting.getVisitFee() !=null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00"))
        {
            findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if(accounting.getLastDues() !=null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00"))
        {
            findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }


        if(accounting.getPaymentMethod().equals("1"))
        {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash,0,0,0);
        }
        else
        {
            tvPaymentMethod.setText(getString(R.string.card)+"  "+accounting.getLast4());
        }

        if(accounting.getPaidByWallet() !=null && accounting.getPaidByWallet().equals("1"))
        {
            tvPaymentMethod.setText(tvPaymentMethod.getText().toString() + " + "+ getString(R.string.wallet));
        }

        if(booking.getJobDescription() !=null && !booking.getJobDescription().equals(""))
        {
            findViewById(R.id.viewJobDescription).setVisibility(View.VISIBLE);
            tvJobDescription.setVisibility(View.VISIBLE);
            tvJobDescriptionLabel.setVisibility(View.VISIBLE);
            tvJobDescription.setText(booking.getJobDescription());
        }

        if(booking.getServiceType().equals("1"))
        {
            for(ServiceItem serviceItem : booking.getCartData())
            {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees,null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName() +" X "+ serviceItem.getQuntity());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        }
        else
        {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees,null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
            tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llService.addView(serviceView);
            hourFee = Double.parseDouble(accounting.getTotalActualHourFee());
        }

        try {
            tvDate.setText(getString(R.string.date)+" : "+displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            tvTime.setText(getString(R.string.time)+" : "+displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            ratingStar.setRating(Float.parseFloat(booking.getAverageRating()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        bookidId = booking.getBookingId();

        initTimer();

        if(booking.getStatus().equals(VariableConstant.ARRIVED))
        {
            progressDialog.setMessage(getString(R.string.updatingStatusEventStarted));
            updatingStatus = VariableConstant.JOB_TIMER_STARTED;
            timerStatus = VariableConstant.TIMER_STARTED;
            tvTimer.setText(presenter.getDurationString(timerSecond));

            tvStatus.setText(getString(R.string.Arrived));
        }
        else
        {
            progressDialog.setMessage(getString(R.string.updatingStatusEventCompleted));
            updatingStatus = VariableConstant.JOB_TIMER_COMPLETED;
            timerStatus = VariableConstant.TIMER_PAUSED;
            jobStarted(true);

            tvStatus.setText(getString(R.string.eventStarted));
        }

        tveventId.setText(getText(R.string.jobId)+" "+booking.getBookingId());


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75)
                {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject jsonObjectTimer = new JSONObject();
                    try {

                        jsonObject.put("bookingId",bookidId);
                        jsonObject.put("status", updatingStatus);
                        jsonObject.put("latitude",sessionManager.getCurrentLat());
                        jsonObject.put("longitude",sessionManager.getCurrentLng());

                        if(updatingStatus.equals(VariableConstant.JOB_TIMER_COMPLETED))
                        {
                            jsonObject.put("distance",""+sessionManager.getOnJobDistance(booking.getBookingId()));
                            jsonObject.put("second",timerSecond);
                        }
                        Log.d(TAG, "c: "+jsonObject);

                        jsonObjectTimer.put("bookingId",bookidId);
                        jsonObjectTimer.put("status",timerStatus);
                        jsonObjectTimer.put("second", timerSecond);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    seekBar.setProgress(100);
                    presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject, jsonObjectTimer);
                }
                else
                {
                    seekBar.setProgress(0);
                }
            }
        });


        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT))
                {
                    tvMessageCount.setText(""+sessionManager.getChatCount(booking.getBookingId()));
                    tvMessageCount.setVisibility(View.VISIBLE);
                }
                else
                {
                    String cancelId = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    if (cancelId.equals(booking.getBookingId()))
                    {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        closeActivity();
                        // DialogHelper.customAlertDialogCloseActivity(EventStartedCompletedActivity.this, header, msg, getString(R.string.oK));
                    }
                }
            }
        };

        registerReceiver(receiver, filter);

    }

    private void jobStarted(boolean isAlreadyStarted)
    {
        tvSeekbarText.setText(R.string.eventCompleted);
        tvStartPauseTimer.setVisibility(View.VISIBLE);
        tvStartPauseTimer.setEnabled(true);
        tvStartPauseTimer.setText(getString(R.string.pauseTimer));
        runTimer = true ;

        if(isAlreadyStarted)
        {
            timerSecond =Integer.parseInt(bookingTimer.getSecond());
            if(bookingTimer.getStatus().equals("0"))
            {
                runTimer = false ;
                tvTimer.setText("" +presenter.getDurationString(timerSecond));
                tvStartPauseTimer.setText(getString(R.string.startTimer));
                return;
            }
            long timeElapsed =  (System.currentTimeMillis() - Utility.convertUTCToTimeStamp(bookingTimer.getStartTimeStamp())) / 1000 ;
            timerSecond = timerSecond + timeElapsed;
        }
        if(!isRunning)
        {
            handler.post(myRunnable);
        }
    }

    private void initTimer() {
        handler = new Handler();
        myRunnable=new Runnable() {
            @Override
            public void run()
            {
                isRunning = true ;
                if(runTimer)
                {
                    timerSecond = timerSecond +1;

                    if(timerSecond %2 == 0)
                    {
                        tvTimer.setTextColor(normalTextColor);
                    }
                    else
                    {
                        tvTimer.setTextColor(lightestTextColor);
                    }

                    tvTimer.setText("" +presenter.getDurationString(timerSecond));
                    handler.postDelayed(this, 1000);
                }
                else
                {
                    tvTimer.setTextColor(normalTextColor);
                    isRunning = false ;
                }
            }
        };
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);
    }



    @Override
    protected void onResume() {
        super.onResume();
        if(sessionManager.getChatCount(booking.getBookingId()) != 0)
        {
            tvMessageCount.setVisibility(View.VISIBLE);
            tvMessageCount.setText(""+sessionManager.getChatCount(booking.getBookingId()));
        }
        else
        {
            tvMessageCount.setVisibility(View.GONE);
        }

        if(isRunning && sessionManager.getElapsedTime() != 0)
        {
            timerSecond = presenter.getTimeWhileInBackground(sessionManager);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isRunning)
        {
            sessionManager.setElapsedTime(timerSecond);
            sessionManager.setTimeWhilePaused(System.currentTimeMillis());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sessionManager.setElapsedTime(0);
        unregisterReceiver(receiver);
        handler.removeCallbacks(myRunnable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;

        seekBar.setProgress(0);


        if(updatingStatus.equals(VariableConstant.JOB_TIMER_STARTED))
        {
            progressDialog.setMessage(getString(R.string.updatingStatusEventCompleted));
            updatingStatus = VariableConstant.JOB_TIMER_COMPLETED;
            timerStatus = VariableConstant.JOB_TIMER_STARTED;
            jobStarted(false);

            sessionManager.setBookingStr(presenter.getBookingStr(booking,sessionManager.getBookingStr(),VariableConstant.JOB_TIMER_STARTED));

            booking.setStatus(VariableConstant.JOB_TIMER_STARTED);

            tvStatus.setText(getString(R.string.eventStarted));

            AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.GigStarts.value,bookidId);


        }
        else
        {
            double travelFee = 0;
            double total = 0;
            try {

                JSONObject jsonObjectResponse = new JSONObject(msg);
                JSONObject jsonData = jsonObjectResponse.getJSONObject("data");

                total = Double.parseDouble(booking.getAccounting().getTotal());
                travelFee = Double.parseDouble(jsonData.getString("travelFee"));
                total += travelFee;

                if(booking.getServiceType().equals("2"))
                {
                    total = total - hourFee;
                    booking.getAccounting().setTotalActualJobTimeMinutes(jsonData.getString("totalActualJobTimeMinutes"));
                    booking.getAccounting().setTotalActualHourFee(jsonData.getString("totalActualHourFee"));
                    total = total + Double.parseDouble(jsonData.getString("totalActualHourFee"));
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            booking.getAccounting().setTravelFee(""+travelFee);
            booking.getAccounting().setTotal(""+total);

            sessionManager.setBookingStr(presenter.getBookingStr(booking,sessionManager.getBookingStr(),VariableConstant.JOB_TIMER_COMPLETED));

            booking.setStatus(VariableConstant.JOB_TIMER_COMPLETED);
            finish();
            Intent intent = new Intent(this,InvoiceActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("booking",booking);
            intent.putExtras(bundle);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
            }
            else {
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
            }

            AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.JobCompleted.value,bookidId);

        }
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this,getString(R.string.alert),msg,getString(R.string.oK));
        try {
            if(booking.getReminderId() !=null && !booking.getReminderId().equals(""))
            {
                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                calendarEventHelper.deleteEvent(booking.getReminderId());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvStartPauseTimer:
                VariableConstant.IS_BOOKING_UPDATED = true;
                if(runTimer)
                {
                    tvStartPauseTimer.setText(getString(R.string.startTimer));
                    runTimer=false;
                    startStopTimer(VariableConstant.TIMER_PAUSED);

                    AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.GigPause.value,bookidId);
                }
                else
                {
                    jobStarted(false);
                    startStopTimer(VariableConstant.TIMER_STARTED);

                    AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.GigStarts.value,bookidId);

                }
                break;

            case R.id.llCutomerDetails:
                Intent intent = new Intent(this,CustomerReviewsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.ivCall:
                      String uri = "tel:"+booking.getPhone() ;
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);
/*
                AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.CallOpen.value);
                break;*/


                break;

            case R.id.ivMessage:
               /* Intent msgIntent = new Intent(Intent.ACTION_VIEW);
                msgIntent.setData(Uri.parse("sms:"+booking.getPhone()));
                startActivity(msgIntent);*/

                sessionManager .setChatBookingID(booking.getBookingId());
                sessionManager.setChatCustomerName(booking.getFirstName() + " "+booking.getLastName());
                sessionManager.setChatCustomerID(booking.getCustomerId());

                Intent chatIntent = new Intent(this,ChattingActivity.class);
                startActivity(chatIntent);

                AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.ChatOpen.value);
                break;

            case R.id.ivBackButton:
                closeActivity();
                break;
        }
    }

    /**
     * method for calling presenter method for start and stop the timer
     * @param status 0 => Stop 1 => Start
     */
    private void startStopTimer(String status) {
        try {

            long consumedTime = timerSecond;
            Log.d(TAG, "startStopTimer: "+consumedTime);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("bookingId",bookidId);
            jsonObject.put("status",status);
            jsonObject.put("second", consumedTime);

            presenter.updateTimer(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
