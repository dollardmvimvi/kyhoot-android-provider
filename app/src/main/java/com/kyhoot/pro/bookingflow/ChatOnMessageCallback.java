package com.kyhoot.pro.bookingflow;

import com.kyhoot.pro.pojo.chat.ChatData;

/**
 * Created by murashid on 04-Jan-18.
 */

public interface ChatOnMessageCallback {
    void onMessageReceived(ChatData chatData);
}
