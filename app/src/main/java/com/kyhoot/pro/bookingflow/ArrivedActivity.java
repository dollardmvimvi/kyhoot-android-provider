package com.kyhoot.pro.bookingflow;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kyhoot.pro.R;
import com.kyhoot.pro.adapters.CancelListAdapter;
import com.kyhoot.pro.bookingflow.review.CustomerReviewsActivity;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.booking.CancelPojo;
import com.kyhoot.pro.pojo.booking.ServiceItem;
import com.kyhoot.pro.pojo.history.Accounting;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CalendarEventHelper;
import com.kyhoot.pro.utility.CircleTransform;
import com.kyhoot.pro.utility.DialogHelper;
import com.kyhoot.pro.utility.GoogleRoute;
import com.kyhoot.pro.utility.MixpanelEvents;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;
import com.kyhoot.pro.utility.WorkaroundMapFragment;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>ArrivedActivity</h1>
 * Activity for updating status
 */
public class ArrivedActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, OnMapReadyCallback, AppBarLayout.OnOffsetChangedListener, CancelListAdapter.CancelSelected {

    private static final String TAG = "ArrivedActivity";
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private ImageView ivCustomer,ivUpDownButton;
    private TextView tvCustomerName;
    private SeekBar seekBar;
    private AppBarLayout appBarLayout;
    private boolean isExpanded = true;
    private CardView cvGoogleNavigation,cvWazeNavigation;

    private Typeface fontMedium;
    private Typeface fontBold;
    private String cancelId = "";

    private TextView tvMessageCount;

    private Booking booking;

    private double lat=0,lng=0 ;
    private LayoutInflater  inflater;

    private GoogleMap googleMap;
    private LatLng providerLatLng;
    private Marker providerMarker;

    private Handler locatingUpdateHandler,markeMoveHandler;
    private Runnable locationUpdateRunnable;

    private BroadcastReceiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrived);

        initView();
    }

    @SuppressLint("SetTextI18n")
    private void initView()
    {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        SimpleDateFormat displayHourFormat = new SimpleDateFormat("hh:mm a", Locale.US);

        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updatingStatusArrived));
        progressDialog.setCancelable(false);
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvCancel = findViewById(R.id.tvCancel);
        tvCancel.setTypeface(fontMedium);
        tvCancel.setOnClickListener(this);

        ivCustomer = findViewById(R.id.ivCustomer);
        ivUpDownButton = findViewById(R.id.ivUpDownButton);

        TextView tvAddress = findViewById(R.id.tvAddress);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvMessageCount = findViewById(R.id.tvMessageCount);

        TextView tvSeekbarText = findViewById(R.id.tvSeekbarText);
        TextView tvGoogleNavigation = findViewById(R.id.tvGoogleNavigation);
        TextView tvWazeNavigation = findViewById(R.id.tvWazeNavigation);
        LinearLayout llCutomerDetails = findViewById(R.id.llCutomerDetails);
        seekBar = findViewById(R.id.seekBar);

        LinearLayout llService = findViewById(R.id.llService);
        TextView tvTotalBillAmountLabel = findViewById(R.id.tvTotalBillAmountLabel);
        TextView tvTotalBillAmount = findViewById(R.id.tvTotalBillAmount);
        TextView tvDate = findViewById(R.id.tvDate);
        TextView tvTime = findViewById(R.id.tvTime);
        TextView tvPaymentBreakDownLabel = findViewById(R.id.tvPaymentBreakDownLabel);
        TextView tvTravelFeeLabel = findViewById(R.id.tvTravelFeeLabel);
        TextView tvTravelFee = findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = findViewById(R.id.tvVisitFeeLabel);
        TextView tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = findViewById(R.id.tvLastDueLabel);
        TextView tvLastDue = findViewById(R.id.tvLastDue);
        TextView tvDiscountLabel = findViewById(R.id.tvDiscountLabel);
        TextView tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotalLabel = findViewById(R.id.tvTotalLabel);
        TextView tvTotal = findViewById(R.id.tvTotal);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);
        TextView tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
        TextView tvJobDescriptionLabel = findViewById(R.id.tvJobDescriptionLabel);
        TextView tvJobDescription = findViewById(R.id.tvJobDescription);
        LayoutInflater inflater = getLayoutInflater();

        tvAddress.setTypeface(fontRegular);
        tvCustomerName.setTypeface(fontMedium);
        tvMessageCount.setTypeface(fontMedium);
        tvSeekbarText.setTypeface(fontBold);
        tvGoogleNavigation.setTypeface(fontRegular);
        tvWazeNavigation.setTypeface(fontRegular);

        tvTotalBillAmountLabel.setTypeface(fontMedium);
        tvTotalBillAmount.setTypeface(fontRegular);
        tvDate.setTypeface(fontRegular);
        tvTime.setTypeface(fontRegular);
        tvPaymentBreakDownLabel.setTypeface(fontMedium);
        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontRegular);
        tvTotal.setTypeface(fontRegular);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontRegular);
        tvJobDescriptionLabel.setTypeface(fontMedium);
        tvJobDescription.setTypeface(fontRegular);

        appBarLayout = findViewById(R.id.appBarLayout);
        final AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
        WorkaroundMapFragment sMapFrag = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        // Getting GoogleMap object from the fragment
        sMapFrag.getMapAsync(this);
        sMapFrag.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                appBarLayout.requestDisallowInterceptTouchEvent(true);
            }
        });

        cvGoogleNavigation = findViewById(R.id.cvGoogleNavigation);
        cvWazeNavigation = findViewById(R.id.cvWazeNavigation);

        RelativeLayout rlUpDownButton = findViewById(R.id.rlUpDownButton);
        ImageView ivCall = findViewById(R.id.ivCall);
        ImageView ivMessage = findViewById(R.id.ivMessage);
        ImageView ivLocation = findViewById(R.id.ivLocation);

        ivCall.setOnClickListener(this);
        ivMessage.setOnClickListener(this);
        ivLocation.setOnClickListener(this);
        //llCutomerDetails.setOnClickListener(this);
        rlUpDownButton.setOnClickListener(this);
        appBarLayout.addOnOffsetChangedListener(this);

        cvGoogleNavigation.setOnClickListener(this);
        cvWazeNavigation.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");
        Accounting accounting = booking.getAccounting();

        if(!booking.getProfilePic().equals(""))
        {
            Glide.with(this).
                    load(booking.getProfilePic())
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image)
                    .into(ivCustomer);
        }

        tvCustomerName.setText(booking.getFirstName()+" "+booking.getLastName()) ;
        tvAddress.setText(booking.getAddLine1() + " "+booking.getAddLine2());

        tvTotalBillAmount.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if(accounting.getTravelFee() !=null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.00") )
        {
            findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if(accounting.getVisitFee() !=null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00"))
        {
            findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if(accounting.getLastDues() !=null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00"))
        {
            findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }


        if(accounting.getPaymentMethod().equals("1"))
        {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash,0,0,0);
        }
        else
        {
            tvPaymentMethod.setText(getString(R.string.card)+"  "+accounting.getLast4());
        }

        if(accounting.getPaidByWallet() !=null && accounting.getPaidByWallet().equals("1"))
        {
            tvPaymentMethod.setText(tvPaymentMethod.getText().toString() + " + "+ getString(R.string.wallet));
        }

        if(booking.getJobDescription() !=null && !booking.getJobDescription().equals(""))
        {
            findViewById(R.id.viewJobDescription).setVisibility(View.VISIBLE);
            tvJobDescription.setVisibility(View.VISIBLE);
            tvJobDescriptionLabel.setVisibility(View.VISIBLE);
            tvJobDescription.setText(booking.getJobDescription());
        }

        if(booking.getServiceType().equals("1"))
        {
            for(ServiceItem serviceItem : booking.getCartData())
            {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees,null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName() +" X "+ serviceItem.getQuntity());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        }
        else
        {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees,null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
            tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llService.addView(serviceView);
        }

        try {
            tvDate.setText(getString(R.string.date)+" : "+displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            tvTime.setText(getString(R.string.time)+" : "+displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(!booking.getLatitude().equals(""))
        {
            lat = Double.parseDouble(booking.getLatitude());
            lng = Double.parseDouble(booking.getLongitude());
        }

        locatingUpdateHandler = new Handler();
        markeMoveHandler = new Handler();
        final float durationInMs = 5000;
        locationUpdateRunnable = new Runnable() {
            @Override
            public void run() {
                locatingUpdateHandler.postDelayed(this,5000);

                if(googleMap !=null && providerMarker != null)
                {
                    providerLatLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng()));
                    final LatLng startPosition = providerMarker.getPosition();
                    final LatLng finalPosition = providerLatLng;

                    try {
                        double distanceInMeter = Utility.distanceInMeter(startPosition.latitude,startPosition.longitude,finalPosition.latitude,finalPosition.longitude);
                        if(!Double.isNaN(distanceInMeter) && distanceInMeter >= 5)
                        {
                            googleMap.animateCamera(CameraUpdateFactory.newLatLng(providerLatLng));
                            final long start = SystemClock.uptimeMillis();
                            final Interpolator interpolator = new AccelerateDecelerateInterpolator();

                            markeMoveHandler.post(new Runnable() {
                                long elapsed;
                                float t;
                                float v;
                                @Override
                                public void run() {
                                    // Calculate progress using interpolator
                                    elapsed = SystemClock.uptimeMillis() - start;
                                    t = elapsed / durationInMs;
                                    v = interpolator.getInterpolation(t);
                                    LatLng currentPosition = new LatLng(startPosition.latitude*(1-t)+finalPosition.latitude*t,
                                            startPosition.longitude*(1-t)+finalPosition.longitude*t);
                                    providerMarker.setPosition(currentPosition);
                                    // Repeat till progress is complete.
                                    if (t < 1) {
                                        // Post again 16ms later.
                                        markeMoveHandler.postDelayed(this, 16);
                                    }
                                }
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            }
        };

        CollapsingToolbarLayout ctlMyBooking = findViewById(R.id.ctlMyBooking);

        ctlMyBooking.setTitle(getText(R.string.jobId)+" "+booking.getBookingId());
        ctlMyBooking.setCollapsedTitleTextAppearance(R.style.TextCollapsedEventId);
        ctlMyBooking.setExpandedTitleTextAppearance(R.style.TextExpandHide);
        ctlMyBooking.setCollapsedTitleGravity(Gravity.CENTER);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75)
                {
                    progressDialog.setMessage(getString(R.string.updatingStatusArrived));
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("bookingId",booking.getBookingId());
                        jsonObject.put("status",VariableConstant.ARRIVED);
                        jsonObject.put("latitude",sessionManager.getCurrentLat());
                        jsonObject.put("longitude",sessionManager.getCurrentLng());
                        jsonObject.put("distance",""+sessionManager.getArrivedDistance(booking.getBookingId()));
                        Log.d(TAG, "initView: "+jsonObject);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    seekBar.setProgress(100);
                    presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                }
                else
                {
                    seekBar.setProgress(0);
                }
            }
        });

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT))
                {
                    tvMessageCount.setText(""+sessionManager.getChatCount(booking.getBookingId()));
                    tvMessageCount.setVisibility(View.VISIBLE);
                }
                else
                {
                    String cancelId = intent.getStringExtra("cancelid");
                    String header = intent.getStringExtra("header");
                    String msg = intent.getStringExtra("msg");
                    if (cancelId.equals(booking.getBookingId()))
                    {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        closeActivity();
                        //DialogHelper.customAlertDialogCloseActivity(ArrivedActivity.this, header, msg, getString(R.string.oK));
                    }
                }
            }
        };

        registerReceiver(receiver, filter);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sessionManager.getChatCount(booking.getBookingId()) != 0)
        {
            tvMessageCount.setVisibility(View.VISIBLE);
            tvMessageCount.setText(""+sessionManager.getChatCount(booking.getBookingId()));
        }
        else
        {
            tvMessageCount.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        locatingUpdateHandler.removeCallbacks(locationUpdateRunnable);

        unregisterReceiver(receiver);

        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;

        finish();

        sessionManager.setBookingStr(presenter.getBookingStr(booking,sessionManager.getBookingStr(),VariableConstant.ARRIVED));

        Intent intent = new Intent(this,EventStartedCompletedActivity.class);
        Bundle bundle = new Bundle();
        booking.setStatus(VariableConstant.ARRIVED);
        bundle.putSerializable("booking",booking);
        intent.putExtras(bundle);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
        else {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }

        AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.Arrived.value,booking.getBookingId());

    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_cancel,null);
        alertDialogBuilder.setView(view);
        TextView tvTitle= view.findViewById(R.id.tvTitle);
        TextView tvSubmit= view.findViewById(R.id.tvSubmit);
        ImageView ivClose  = view.findViewById(R.id.ivClose);

        tvTitle.setTypeface(fontMedium);
        tvSubmit.setTypeface(fontBold);
        RecyclerView rvCancel = view.findViewById(R.id.rvCancel);
        rvCancel.setLayoutManager(new LinearLayoutManager(this));
        CancelListAdapter cancelListAdapter = new CancelListAdapter(this,cancelPojo.getData(),this);
        rvCancel.setAdapter(cancelListAdapter);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cancelId.equals(""))
                {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("bookingId",booking.getBookingId());
                        jsonObject.put("resonId",cancelId);
                        progressDialog.setMessage(getString(R.string.loading));
                        presenter.cancelBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject);
                        alertDialog.dismiss();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Toast.makeText(ArrivedActivity.this,getString(R.string.plsSelectCancel),Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelId = "";
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this,getString(R.string.alert),msg,getString(R.string.oK));
        try {
            if(booking.getReminderId() !=null && !booking.getReminderId().equals(""))
            {
                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                calendarEventHelper.deleteEvent(booking.getReminderId());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvCancel:
                progressDialog.setMessage(getString(R.string.loading));
                presenter.getCancelReason();
                break;

            case R.id.llCutomerDetails:
                Intent cutomerReviewIntent = new Intent(this,CustomerReviewsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(cutomerReviewIntent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else {
                    startActivity(cutomerReviewIntent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.ivCall:
                           String uri = "tel:"+booking.getPhone() ;
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);

             /*   AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.CallOpen.value);
                break;*/

break;
            case R.id.ivMessage:
               /* Intent msgIntent = new Intent(Intent.ACTION_VIEW);
                msgIntent.setData(Uri.parse("sms:"+booking.getPhone()));
                startActivity(msgIntent);*/

                sessionManager.setChatBookingID(booking.getBookingId());
                sessionManager.setChatCustomerName(booking.getFirstName() + " "+booking.getLastName());
                sessionManager.setChatCustomerID(booking.getCustomerId());

                Intent chatIntent = new Intent(this,ChattingActivity.class);
                startActivity(chatIntent);

                AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.ChatOpen.value);
                break;

            case R.id.rlUpDownButton:
                if(!isExpanded)
                {
                    isExpanded = true;
                    ivUpDownButton.setImageResource(R.drawable.arrived_up_arrow_icon);
                    appBarLayout.setExpanded(true,true);
                    cvGoogleNavigation.setVisibility(View.VISIBLE);
                    cvWazeNavigation.setVisibility(View.VISIBLE);
                }
                else
                {
                    isExpanded = false;
                    ivUpDownButton.setImageResource(R.drawable.arrived_down_arrow_icon);
                    appBarLayout.setExpanded(false,true);
                    cvGoogleNavigation.setVisibility(View.GONE);
                    cvWazeNavigation.setVisibility(View.GONE);
                }
                break;

            case R.id.cvGoogleNavigation:
                String muri = "google.navigation:q=" + lat + "," + lng;
                Intent  googleIntent= new Intent(Intent.ACTION_VIEW, Uri.parse(muri));
                googleIntent.setPackage("com.google.android.apps.maps");
                startActivity(googleIntent);
                break;

            case R.id.cvWazeNavigation:
                try {
                    String url = "waze://?ll=" + lat + "," + lng + "&navigate=yes";
                    Intent wazeIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(wazeIntent);
                } catch (ActivityNotFoundException ex) {
                    Intent intent =
                            new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
                    startActivity(intent);
                }

            case R.id.ivLocation:
                if(googleMap != null)
                {
                    LatLng latLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng()));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }
                break;

        }
    }

    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;
        LatLng customerLatLng = new LatLng(lat,lng);
        providerLatLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng()));
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(customerLatLng, 15));

        final LatLngBounds bounds = new LatLngBounds.Builder()
                .include(providerLatLng)
                .include(customerLatLng)
                .build();

        this.googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds,200));
            }
        });

        googleMap.addMarker(new MarkerOptions().position(customerLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_blue_dot_icon)));
        googleMap.getUiSettings().setCompassEnabled(false);
        String url = GoogleRoute.makeURL(providerLatLng.latitude,providerLatLng.longitude,customerLatLng.latitude,customerLatLng.longitude);
        GoogleRoute.startPlotting(googleMap,url);

        final View customerMarkerView = inflater.inflate(R.layout.custom_marker, null);
        final ImageView ivProfile = customerMarkerView.findViewById(R.id.ivProfile);

        final Bitmap customerMarker = Utility.createDrawableFromView(ArrivedActivity.this,customerMarkerView);
        providerMarker = googleMap.addMarker(new MarkerOptions().position(providerLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(customerMarker))
                .flat(false));

        Glide.with(this)
                .load(sessionManager.getProfilePic())
                .asBitmap()
                .placeholder(R.drawable.profile_default_image)
                .transform(new CircleTransform(this))
                .error(R.drawable.profile_default_image)
                .into(new BitmapImageViewTarget(ivProfile) {
                    @Override
                    public void onResourceReady(Bitmap  drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        ivProfile.setImageBitmap(drawable);
                        Bitmap customerMarker = Utility.createDrawableFromView(ArrivedActivity.this,customerMarkerView);
                        providerMarker.setIcon(BitmapDescriptorFactory.fromBitmap(customerMarker));
                    }
                });

        locatingUpdateHandler.postDelayed(locationUpdateRunnable,5000);

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0)
        {
            cvGoogleNavigation.setVisibility(View.VISIBLE);
            cvWazeNavigation.setVisibility(View.VISIBLE);
            isExpanded = true;
            ivUpDownButton.setImageResource(R.drawable.arrived_up_arrow_icon);
        }
        else
        {
            cvGoogleNavigation.setVisibility(View.GONE);
            cvWazeNavigation.setVisibility(View.GONE);
            isExpanded = false;
            ivUpDownButton.setImageResource(R.drawable.arrived_down_arrow_icon);
        }
    }

    @Override
    public void onCancelSeleted(String id, String name) {
        cancelId = id;
    }
}
