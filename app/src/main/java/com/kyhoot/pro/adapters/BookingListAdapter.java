package com.kyhoot.pro.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kyhoot.pro.R;
import com.kyhoot.pro.bookingflow.AcceptRejectActivity;
import com.kyhoot.pro.bookingflow.ArrivedActivity;
import com.kyhoot.pro.bookingflow.EventStartedCompletedActivity;
import com.kyhoot.pro.bookingflow.InvoiceActivity;
import com.kyhoot.pro.bookingflow.OnTheWayActivity;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CircleTransform;
import com.kyhoot.pro.utility.NotificationHelper;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by murashid on 09-Sep-17.
 * <h1>BookingStatus List Adapter</h1>
 * BookingStatus recycler view adapter for inflating list of booking in BookingFragment under home screen
 * @see com.kyhoot.pro.main.booking.BookingFragment
 */

public class BookingListAdapter extends RecyclerView.Adapter<BookingListAdapter.ViewHolder> implements View.OnClickListener {

    private String TAG = getClass().getName();

    private String type;
    private Typeface fontRegular,fontMedium;
    private Activity mAcitvity;
    private ArrayList<Booking> bookings;
    private SimpleDateFormat serverFormat, displayFormat;
    private SessionManager sessionManager;

    private long currentTime = 0;
    private Handler handler;
    private Runnable runnable;
    private ArrayList<CountDownTimer> timers;

    public BookingListAdapter(Activity mAcitvity,ArrayList<Booking> bookings, String type)
    {
        this.mAcitvity = mAcitvity;
        this.bookings = bookings;
        this.type = type;
        timers = new ArrayList<>();
        sessionManager = SessionManager.getSessionManager(mAcitvity);
        fontRegular = Utility.getFontRegular(mAcitvity);
        fontMedium = Utility.getFontMedium(mAcitvity);
        displayFormat = new SimpleDateFormat("dd MMMM yyyy | hh:mm a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView ivCustomer;
        TextView tvBookingHeader,tvCustomerName,tvBookingTime,tvDistance,tvRemainingTime,tvBookingStatus,tvBookingLabel;
        ProgressBar pBRemainingTime;
        RelativeLayout rlProgressBar;
        LinearLayout llBooking;

        CountDownTimer timer;

        ViewHolder(View itemView) {
            super(itemView);
            ivCustomer = itemView.findViewById(R.id.ivCustomer);
            tvBookingHeader = itemView.findViewById(R.id.tvBookingHeader);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvBookingTime = itemView.findViewById(R.id.tvBookingTime);
            tvDistance = itemView.findViewById(R.id.tvDistance);
            tvRemainingTime = itemView.findViewById(R.id.tvRemainingTime);
            pBRemainingTime = itemView.findViewById(R.id.pBRemainingTime);
            rlProgressBar = itemView.findViewById(R.id.rlProgressBar);
            llBooking = itemView.findViewById(R.id.llBooking);
            tvBookingStatus = itemView.findViewById(R.id.tvBookingStatus);
            tvBookingLabel = itemView.findViewById(R.id.tvBookingLabel);

            tvBookingHeader.setTypeface(fontRegular);
            tvCustomerName.setTypeface(fontMedium);
            tvBookingTime.setTypeface(fontRegular);
            tvDistance.setTypeface(fontRegular);
            tvRemainingTime.setTypeface(fontRegular);
            tvBookingStatus.setTypeface(fontRegular);
            tvBookingLabel.setTypeface(fontRegular);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_booking, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(bookings.get(position).getBookingType().equals("1"))
        {
           holder.tvBookingStatus.setText(R.string.asap);
        }
        else if(bookings.get(position).getBookingType().equals("2") )
        {
            holder.tvBookingStatus.setText(R.string.scheduled);
        }

        holder.tvCustomerName.setText(bookings.get(position).getFirstName());
        if(!bookings.get(position).getLastName().equals(""))
        {
            holder.tvCustomerName.setText(bookings.get(position).getFirstName()+" "+bookings.get(position).getLastName()) ;
        }
        try {
            holder.tvBookingTime.setText(displayFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getEventStartTime()))));
            holder.tvDistance.setText(""+Utility.getFormattedPrice(String.valueOf((Double.parseDouble(bookings.get(position).getDistance())) * sessionManager.getDistanceUnitConverter())) +" " +
                    sessionManager.getDistanceUnit()+"  " + mAcitvity.getString(R.string.away));

            if(!bookings.get(position).getProfilePic().equals(""))
            {
                Glide.with(mAcitvity).
                        load(bookings.get(position).getProfilePic())
                        .error(R.drawable.profile_default_image)
                        .transform(new CircleTransform(mAcitvity))
                        .placeholder(R.drawable.profile_default_image)
                        .into(holder.ivCustomer);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(type.equals("offers"))
        {
            holder.rlProgressBar.setVisibility(View.VISIBLE);

            if(currentTime == 0)
            {
                currentTime = Utility.convertUTCToTimeStamp(bookings.get(position).getServerTime());
                runTimer();
            }
            setTimer(holder,bookings.get(position).getBookingRequestedForProvider(),bookings.get(position).getBookingExpireForProvider());
        }
        else
        {
            holder.rlProgressBar.setVisibility(View.GONE);
        }
        holder.llBooking.setOnClickListener(this);
        holder.llBooking.setTag(holder);

    }

    private void runTimer() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                currentTime = currentTime + 1000;
                handler.postDelayed(this,1000);
            }
        };
        handler.postDelayed(runnable,1000);
    }

    private void setTimer(final ViewHolder viewHolder, String bookingStartTime, final String bookingEndTime)
    {
        final int position = viewHolder.getAdapterPosition();

        if(position != RecyclerView.NO_POSITION)
        {
            if (viewHolder.timer != null) {
                viewHolder.timer.cancel();
            }
            long start = Utility.convertUTCToTimeStamp(bookingStartTime);
            long end =  Utility.convertUTCToTimeStamp(bookingEndTime);

            final long[] totalSecs = {(end - start) / 1000};
            final long[] hours = {0};
            final long[] minutes = {0};
            final long[] seconds = {0};

            //totalSecs[0] for Maximum Progress
            viewHolder.pBRemainingTime.setMax((int) totalSecs[0]);

            totalSecs[0] = (long) (end - currentTime) / 1000 ;
            hours[0] = TimeUnit.SECONDS.toHours(totalSecs[0]);
            minutes[0] = TimeUnit.SECONDS.toMinutes(totalSecs[0]) % 60;
            seconds[0] = totalSecs[0] % 60;

            viewHolder.tvRemainingTime.setText(String.format("%02d", minutes[0])+":"+String.format("%02d", seconds[0]));

            viewHolder.timer = new CountDownTimer(totalSecs[0] * 1000, 1000) {
                @Override
                public void onTick(long l) {

                    totalSecs[0] = totalSecs[0] - 1 ;
                    hours[0] = TimeUnit.SECONDS.toHours(totalSecs[0]);
                    minutes[0] = TimeUnit.SECONDS.toMinutes(totalSecs[0]) % 60;
                    seconds[0] = totalSecs[0] % 60;

                    viewHolder.tvRemainingTime.setText(String.format("%02d", minutes[0])+":"+String.format("%02d", seconds[0]));
                    viewHolder.pBRemainingTime.setProgress((int) totalSecs[0]);

                    if(position < bookings.size())
                    {
                        bookings.get(position).setCurrentTime(currentTime);
                    }
                }

                @Override
                public void onFinish() {
                    try {
                        if(((Utility.convertUTCToTimeStamp(bookingEndTime) - currentTime) == 1000))
                        {
                            sessionManager.setLastBookingId("");
                            String bookingId  = bookings.get(position).getBookingId();
                            if(!bookingId.equals(sessionManager.getLastBookingIdCancel()))
                            {
                                sessionManager.setLastBookingIdCancel(bookingId);
                                Intent intent = new Intent();
                                intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                                intent.putExtra("cancelid",bookingId);
                                intent.putExtra("msg",mAcitvity.getString(R.string.bookingExpired));
                                intent.putExtra("header",mAcitvity.getString(R.string.alert));
                                mAcitvity.sendBroadcast(intent);
                                VariableConstant.IS_BOOKING_UPDATED = true;

                                Log.d(TAG, "Booking cancel from Booking adapter");
                                NotificationHelper.sendNotification(mAcitvity,"15",mAcitvity.getString(R.string.app_name),mAcitvity.getString(R.string.bookingExpired));
                            }
                            Log.d(TAG, "Music stopeeedd ");
                            AppController.getInstance().stopNewBookingRingtoneService();
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }.start();

            if(viewHolder.timer !=null)
            {
                timers.add(viewHolder.timer);
            }
        }
    }
    
    public void clearHandlers()
    {
        if(handler !=null && runnable != null)
        {
            handler.removeCallbacks(runnable);
        }
    }

    public void clearTimer()
    {
        if(timers != null)
        {
            for(CountDownTimer timer : timers)
            {
                timer.cancel();
            }
        }

    }


    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position  = viewHolder.getAdapterPosition();

        switch (v.getId())
        {
            case R.id.llBooking:
                Intent intent;
                if(type.equals("offers"))
                {
                    intent = new Intent(mAcitvity,AcceptRejectActivity.class);
                }
                else
                {
                    switch (bookings.get(position).getStatus())
                    {
                        case VariableConstant.ACCEPT:
                            intent = new Intent(mAcitvity,OnTheWayActivity.class);
                            break;

                        case VariableConstant.ON_THE_WAY:
                            intent = new Intent(mAcitvity,ArrivedActivity.class);
                            break;

                        case VariableConstant.ARRIVED:
                        case VariableConstant.JOB_TIMER_STARTED:
                            intent = new Intent(mAcitvity,EventStartedCompletedActivity.class);
                            break;

                        case VariableConstant.JOB_TIMER_COMPLETED:
                            intent = new Intent(mAcitvity,InvoiceActivity.class);
                            break;

                        default:
                            intent = new Intent(mAcitvity,OnTheWayActivity.class);
                    }
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable("booking",bookings.get(position));
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){

                    mAcitvity.startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(mAcitvity).toBundle());
                }
                else {
                    mAcitvity.startActivity(intent);
                    mAcitvity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

}
