package com.kyhoot.pro.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kyhoot.pro.R;
import com.kyhoot.pro.bookingflow.AcceptRejectActivity;
import com.kyhoot.pro.bookingflow.ArrivedActivity;
import com.kyhoot.pro.bookingflow.EventStartedCompletedActivity;
import com.kyhoot.pro.bookingflow.InvoiceActivity;
import com.kyhoot.pro.bookingflow.OnTheWayActivity;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.NotificationHelper;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by murashid on 09-Sep-17.
 * <h1>BookingStatus List Adapter</h1>
 * BookingStatus recycler view adapter for inflating list of booking in BookingFragment under home screen
 * @see com.kyhoot.pro.main.booking.BookingFragment
 */

public class BidBookingListAdapter extends RecyclerView.Adapter<BidBookingListAdapter.ViewHolder> implements View.OnClickListener {

    private String TAG = getClass().getName();

    private String type;
    private Typeface fontRegular,fontMedium;
    private Activity mAcitvity;
    private ArrayList<Booking> bookings;
    private SimpleDateFormat serverFormat, displayFormat;
    private ArrayList<Handler> handlers;
    private ArrayList<Runnable> runnables;
    private SessionManager sessionManager;


    public BidBookingListAdapter(Activity mAcitvity, ArrayList<Booking> bookings, String type)
    {
        this.mAcitvity = mAcitvity;
        this.bookings = bookings;
        this.type = type;
        sessionManager = SessionManager.getSessionManager(mAcitvity);
        handlers = new ArrayList<>();
        runnables = new ArrayList<>();
        fontRegular = Utility.getFontRegular(mAcitvity);
        fontMedium = Utility.getFontMedium(mAcitvity);
        displayFormat = new SimpleDateFormat("dd MMMM yyyy | hh:mm a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvCustomerName, tvBookingTypeBidAmount, tvCategory, tvToBid, tvBookingTime, tvDistance, tvRemainingTime, tvPrice, tvBookingStatus ;
        CardView cvBooking;
        private LinearLayout llRemainingTime;

        ViewHolder(View itemView) {
            super(itemView);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvBookingTypeBidAmount = itemView.findViewById(R.id.tvBookingTypeBidAmount);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvToBid = itemView.findViewById(R.id.tvToBid);
            tvBookingTime = itemView.findViewById(R.id.tvBookingTime);
            tvDistance = itemView.findViewById(R.id.tvDistance);
            tvRemainingTime = itemView.findViewById(R.id.tvRemainingTime);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvBookingStatus = itemView.findViewById(R.id.tvBookingStatus);

            cvBooking = itemView.findViewById(R.id.cvBooking);
            llRemainingTime = itemView.findViewById(R.id.llRemainingTime);


            tvCustomerName.setTypeface(fontMedium);
            tvBookingTypeBidAmount.setTypeface(fontRegular);
            tvCategory.setTypeface(fontRegular);
            tvToBid.setTypeface(fontRegular);
            tvBookingTime.setTypeface(fontRegular);
            tvDistance.setTypeface(fontRegular);
            tvRemainingTime.setTypeface(fontRegular);
            tvPrice.setTypeface(fontRegular);
            tvBookingStatus.setTypeface(fontRegular);

            if(type.equals("accepted"))
            {
                llRemainingTime.setVisibility(View.GONE);
                tvBookingStatus.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_bid_booking, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvCustomerName.setText(bookings.get(position).getFirstName()+" "+bookings.get(position).getLastName()) ;
        holder.tvCategory.setText(bookings.get(position).getCatName());
        holder.tvBookingStatus.setText(bookings.get(position).getStatusMsg());

        try {
            holder.tvBookingTime.setText(displayFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getEventStartTime()))));
            holder.tvDistance.setText(""+Utility.getFormattedPrice(String.valueOf((Double.parseDouble(bookings.get(position).getDistance())) * sessionManager.getDistanceUnitConverter())) +" " +
                    sessionManager.getDistanceUnit()+"  " + mAcitvity.getString(R.string.away));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(type.equals("accepted"))
        {
            holder.tvBookingTypeBidAmount.setText(Utility.getPrice(bookings.get(position).getAccounting().getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        else if(!bookings.get(position).getBookingModel().equals("3"))
        {
            if(bookings.get(position).getBookingType().equals("1"))
            {
                holder.tvBookingTypeBidAmount.setText(R.string.asap);
            }
            else
            {
                holder.tvBookingTypeBidAmount.setText(R.string.scheduled);
            }
        }
        else
        {
            holder.tvBookingTypeBidAmount.setText(Utility.getPrice(bookings.get(position).getAccounting().getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if(type.equals("offers") || type.equals("accepted"))
        {
            holder.tvPrice.setText(Utility.getPrice(bookings.get(position).getAccounting().getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        else
        {
            holder.tvPrice.setText(mAcitvity.getString(R.string.myQuote)+":"+Utility.getPrice(bookings.get(position).getAccounting().getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if(!type.equals("accepted"))
        {
            setTimer(holder,bookings.get(position).getBookingRequestedForProvider(),bookings.get(position).getBookingExpireForProvider(), bookings.get(position).getServerTime());
        }

        holder.cvBooking.setOnClickListener(this);
        holder.cvBooking.setTag(holder);

    }

    private void setTimer(final ViewHolder viewHolder, String bookingStartTime, String bookingEndTime, String serverTime)
    {
        long start = Utility.convertUTCToTimeStamp(bookingStartTime);
        long end =  Utility.convertUTCToTimeStamp(bookingEndTime);
        final long[] current = {Utility.convertUTCToTimeStamp(serverTime)};

        final long[] totalSecs = {(end - start) / 1000};
        final long[] hours = {0};
        final long[] minutes = {0};
        final long[] seconds = {0};

        totalSecs[0] = (end - current[0]) /1000 ;
        hours[0] = (totalSecs[0] % 3600) / 60;
        seconds[0] = totalSecs[0] % 60;
        minutes[0] = hours[0] %60 ;

        viewHolder.tvRemainingTime.setText(String.format("%02d", hours[0])+" HRS : "+String.format("%02d", minutes[0])+" MINS");

        handlers.add(new Handler());

        runnables.add(new Runnable() {
            @Override
            public void run() {
                totalSecs[0] = totalSecs[0] - 1 ;
                hours[0] = (totalSecs[0] % 3600) / 60;
                seconds[0] = totalSecs[0] % 60;
                minutes[0] = hours[0] %60 ;

                viewHolder.tvRemainingTime.setText(String.format("%02d", hours[0])+" HRS : "+String.format("%02d", minutes[0])+" MINS");

                current[0] = current[0] + 1000;
                bookings.get(viewHolder.getAdapterPosition()).setCurrentTime(current[0]);

                try {
                    if(totalSecs[0] > 0)
                    {
                        handlers.get(viewHolder.getAdapterPosition()).postDelayed(this, 1000);
                    }
                    else
                    {
                        sessionManager.setLastBookingId("");
                        String bookingId  = bookings.get(viewHolder.getAdapterPosition()).getBookingId();
                        if(!bookingId.equals(sessionManager.getLastBookingIdCancel()))
                        {
                            sessionManager.setLastBookingIdCancel(bookingId);
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                            intent.putExtra("cancelid",bookingId);
                            intent.putExtra("msg",mAcitvity.getString(R.string.bookingExpired));
                            intent.putExtra("header",mAcitvity.getString(R.string.alert));
                            mAcitvity.sendBroadcast(intent);
                            VariableConstant.IS_BOOKING_UPDATED = true;

                            Log.d(TAG, "Booking cancel from Booking adapter");
                            NotificationHelper.sendNotification(mAcitvity,"15",mAcitvity.getString(R.string.app_name),mAcitvity.getString(R.string.bookingExpired));
                        }

                        Log.d(TAG, "Music stopeeedd ");
                        AppController.getInstance().stopNewBookingRingtoneService();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });
        handlers.get(viewHolder.getAdapterPosition()).post(runnables.get(viewHolder.getAdapterPosition()));
    }



    public void clearHandlers()
    {
        try {
            for(int i = 0; i < handlers.size() ; i++)
            {
                Log.d(TAG, "setTimer: clear old handlers "+i);
                handlers.get(i).removeCallbacks(runnables.get(i));
            }
            runnables.clear();
            handlers.clear();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position  = viewHolder.getAdapterPosition();

        switch (v.getId())
        {
            case R.id.cvBooking:
                Intent intent;
                if(type.equals("offers") || type.equals("bid"))
                {
                    intent = new Intent(mAcitvity,AcceptRejectActivity.class);
                }
                else
                {
                    switch (bookings.get(position).getStatus())
                    {
                        case VariableConstant.ACCEPT:
                            intent = new Intent(mAcitvity,OnTheWayActivity.class);
                            break;

                        case VariableConstant.ON_THE_WAY:
                            intent = new Intent(mAcitvity,ArrivedActivity.class);
                            break;

                        case VariableConstant.ARRIVED:
                        case VariableConstant.JOB_TIMER_STARTED:
                            intent = new Intent(mAcitvity,EventStartedCompletedActivity.class);
                            break;

                        case VariableConstant.JOB_TIMER_COMPLETED:
                            intent = new Intent(mAcitvity,InvoiceActivity.class);
                            break;

                        default:
                            intent = new Intent(mAcitvity,OnTheWayActivity.class);
                    }
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable("booking",bookings.get(position));
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){

                    mAcitvity.startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(mAcitvity).toBundle());
                }
                else {
                    mAcitvity.startActivity(intent);
                    mAcitvity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

}

