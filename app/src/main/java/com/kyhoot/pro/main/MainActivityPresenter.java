package com.kyhoot.pro.main;

import com.kyhoot.pro.pojo.appconfig.AppConfigData;

/**
 * Created by murashid on 25-Oct-17.
 * MainActivityPresenter presenter for MainActivity
 */

public class MainActivityPresenter implements MainActivityModel.MainActivityModelImple {

    private MainActivityModel model;
    private MainActivityPresenterImple presenterImple;

    MainActivityPresenter(MainActivityPresenterImple presenterImple)
    {
        model = new MainActivityModel(this);
        this.presenterImple = presenterImple;
    }

    void getAppConfig(String sessionToken)
    {
        model.getAppConfig(sessionToken);
    }

    @Override
    public void onSuccess(AppConfigData appConfigData) {
        presenterImple.onSuccess(appConfigData);
    }

    interface  MainActivityPresenterImple
    {
        void onSuccess(AppConfigData appConfigData);
    }

}
