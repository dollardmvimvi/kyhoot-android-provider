package com.kyhoot.pro.main.normal.history;

import android.util.Log;

import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.Gson;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.history.HistoryPojo;
import com.kyhoot.pro.pojo.history.HistoryWeekData;
import com.kyhoot.pro.pojo.history.HistoryWeekPojo;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by murashid on 13-Nov-17.
 * <h1>HistoryGraphPresenter</h1>
 * HistoryGraphPresenter presenter for history fragment
 * @see HistoryGraphFragment
 */


public class HistoryGraphPresenter implements HistoryGraphModel.HistoryModelImple {
    private HistoryGraphModel model;
    private HistoryPresenterImple presenterImple;
    private SimpleDateFormat XAxisFormat;
    private Gson gson ;
    private String weekData="", weekHistory = "";
    private SessionManager sessionManager;
    private boolean isFragmentAttached = false, isCurrentTab = false;


    HistoryGraphPresenter(SessionManager sessionManager, HistoryPresenterImple presenterImple) {
        this.sessionManager = sessionManager;
        this.presenterImple = presenterImple;
        isFragmentAttached = true;
        model = new HistoryGraphModel(this);
        gson = new Gson();
        XAxisFormat = new SimpleDateFormat("EEE", Locale.US);
    }

    void detach()
    {
        isFragmentAttached = false;
    }
    /**
     * method for passing values from view to model
     * @param sessionToken sessionToken
     */
    void getHistoryWeek(final String sessionToken)
    {
        if(sessionManager.getHistoryWeekData().equals(""))
        {
            presenterImple.startProgressBar();
        }
        else
        {
            handleHistoryWeek(sessionManager.getHistoryWeekData());
        }
        model.getHistoryWeek(sessionToken);
    }

    /**
     * method for passing values from view to model
     * @param sessionToken sessionToken
     */
    void getHistory(final String sessionToken, String apiSelectedDate, boolean isCurrentTab)
    {
        this.isCurrentTab = isCurrentTab;
        if(!isCurrentTab || sessionManager.getHistoryData().equals(""))
        {
            presenterImple.startProgressBar();
        }
        else
        {
            onSuccessBooking(sessionManager.getHistoryData());
        }
        model.getHistory(sessionToken,apiSelectedDate);
    }

    @Override
    public void onFailure(String failureMsg) {
        if(isFragmentAttached)
        {
            presenterImple.stopProgressBar();
            presenterImple.onFailure(failureMsg);
        }

    }

    @Override
    public void onFailure() {
        if(isFragmentAttached)
        {
            presenterImple.stopProgressBar();
            presenterImple.onFailure();
        }
    }

    @Override
    public void handleHistoryWeek(String result) {
        if(weekData.equals(result))
        {
            return;
        }
        weekData = result;
        sessionManager.setHistoryWeekData(result);


        HistoryWeekPojo historyWeekPojo = gson.fromJson(result,HistoryWeekPojo.class);
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        ArrayList<ArrayList<BarEntry>> wholeBarEntries = new ArrayList<>();
        ArrayList<Integer> highestPosition = new ArrayList<>();
        ArrayList<String> apiFormatDates = new ArrayList<>();
        ArrayList<String> tabFormatDates = new ArrayList<>();

        SimpleDateFormat tabDateFormat = new SimpleDateFormat("MMM dd", Locale.US);

        if(historyWeekPojo.getData().size() > 0)
        {
            for(HistoryWeekData historyWeekData : historyWeekPojo.getData())
            {
                c.setTimeInMillis(Utility.convertUTCToTimeStamp(historyWeekData.getEndDate()));

                apiFormatDates.add(historyWeekData.getsDate());
                tabFormatDates.add(tabDateFormat.format(Utility.convertUTCToTimeStamp(historyWeekData.getStartDate()))+"-"+tabDateFormat.format(Utility.convertUTCToTimeStamp(historyWeekData.getEndDate())));

                ArrayList<BarEntry> barEntries = new ArrayList<>();
                int highestVal=0, position = 0;
                for(int i= 0 ; i < historyWeekData.getCount().size() ; i++)
                {
                    barEntries.add(new BarEntry(i, historyWeekData.getCount().get(i)));
                    if(highestVal < historyWeekData.getCount().get(i))
                    {
                        highestVal =  historyWeekData.getCount().get(i);
                        position = i;
                    }
                }
                wholeBarEntries.add(barEntries);
                highestPosition.add(position);
            }
        }
        else if(isFragmentAttached)
        {
            presenterImple.stopProgressBar();
        }

        ArrayList<String> days = new ArrayList<>();

        c.add(Calendar.DATE , -6);
        days.add(XAxisFormat.format(c.getTime()).toUpperCase());
        c.add(Calendar.DATE , +1);
        days.add(XAxisFormat.format(c.getTime()).toUpperCase());
        c.add(Calendar.DATE , +1);
        days.add(XAxisFormat.format(c.getTime()).toUpperCase());
        c.add(Calendar.DATE , +1);
        days.add(XAxisFormat.format(c.getTime()).toUpperCase());
        c.add(Calendar.DATE , +1);
        days.add(XAxisFormat.format(c.getTime()).toUpperCase());
        c.add(Calendar.DATE , +1);
        days.add(XAxisFormat.format(c.getTime()).toUpperCase());
        c.add(Calendar.DATE , +1);
        days.add(XAxisFormat.format(c.getTime()).toUpperCase());

        if(isFragmentAttached)
        {
            presenterImple.initTabBarChart(wholeBarEntries,highestPosition,apiFormatDates,tabFormatDates,days);
        }
    }

    @Override
    public void onSuccessBooking(String result) {
        if(isFragmentAttached)
        {
            presenterImple.stopProgressBar();
        }

        if(weekHistory.equals(result))
        {
            return;
        }
        weekHistory = result;

        if(isCurrentTab)
        {
            sessionManager.setHistoryData(result);
        }

        Log.d("onSuccessBooking s: ",result);
        try {
            HistoryPojo historyPojo = gson.fromJson(result,HistoryPojo.class);
            double amountEarned=0;
            for(int i=0;i<historyPojo.getData().size();i++)
            {
                amountEarned+=Double.parseDouble(historyPojo.getData().get(i).getAccounting().getTotal());
            }

            if(isFragmentAttached)
            {
                presenterImple.onSuccessBooking(historyPojo.getData(),String.valueOf(amountEarned));
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }


    @Override
    public void onNewToken(String token) {
        if(isFragmentAttached)
        {
            presenterImple.onNewToken(token);
        }
    }

    @Override
    public void sessionExpired(String msg) {
        if(isFragmentAttached)
        {
            presenterImple.stopProgressBar();
            presenterImple.sessionExpired(msg);
        }
    }

    /**
     * HistoryGraphPresenter interface for view implementation
     */
    interface HistoryPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();

        void onNewToken(String token);
        void sessionExpired(String msg);

        void initTabBarChart(ArrayList<ArrayList<BarEntry>> wholeBarEntries, ArrayList<Integer> highestPosition, ArrayList<String> apiFormatDates, ArrayList<String> tabFormatDates, ArrayList<String> days);
        void onSuccessBooking(ArrayList<Booking> bookings, String total);
    }
}
