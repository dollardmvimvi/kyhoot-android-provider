package com.kyhoot.pro.main.schedule.addschedule;

import android.util.Log;

import com.kyhoot.pro.utility.OkHttp3ConnectionStatusCode;
import com.kyhoot.pro.utility.ServiceUrl;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 02-Oct-17.
 * <h1>ScheduleAddModel</h1>
 * ScheduleAddModel model for ScheduleAddActivity
 * @see ScheduleAddActivity
 */

public class ScheduleAddModel {
    private static final String TAG = "ReviewModel";
    private ScheduleAddModelImple modelImplement;

    ScheduleAddModel(ScheduleAddModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for adding schedule and check the local validation
     * @param sessiontoken sessionToken
     * @param jsonObject required field
     */
    void getServerTime(final String sessiontoken, final JSONObject jsonObject)
    {
        try {
            if(jsonObject.getString("repeatDay").equals(""))
            {
                modelImplement.onScheduleError();
                return;
            }
            else if(jsonObject.getString("repeatDay").equals("4") && jsonObject.getJSONArray("days").length() == 0)
            {
                modelImplement.onDaysError();
                return;
            }
            else if(jsonObject.getString("startDate").equals(""))
            {
                modelImplement.onDurationError();
                return;
            }
            else if(jsonObject.getString("startTime").equals(""))
            {
                modelImplement.onStartTimeError();
                return;
            }
            else if(jsonObject.getString("endTime").equals(""))
            {
                modelImplement.onEndTimeError();
                return;
            }
          /*  if(returnNumberFromTime(jsonObject.getString("endTime")) - returnNumberFromTime(jsonObject.getString("startTime")) < 0.5)
            {
                modelImplement.onTimeSelectedError();
                return;
            }*/
            if(jsonObject.getString("addresssId").equals(""))
            {
                modelImplement.onLocationError();
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.SERVER_TIME, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        Log.d("GetServerTime", "onSuccess: "+result);
                        try {
                            if(statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                            {
                                jsonObject.put("deviceTime",""+ Utility.convertUTCToServerFormat(new JSONObject(result).getString("data"),""));
                                addSchedule(sessiontoken, jsonObject);
                                Log.d(TAG, "onSuccess: "+jsonObject);
                            }
                            else
                            {
                                modelImplement.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    private void addSchedule(String sessiontoken, JSONObject jsonObject)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.SCHEDULE , OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * method for returin the number from time
     * @param time time
     * @return number
     */
    private double returnNumberFromTime(String time)
    {
        String[] timeWithMinute = time.split(":");
        double hours = Double.parseDouble(timeWithMinute[0]);
        if(timeWithMinute[1].equals("30"))
        {
            hours = hours + 0.5 ;
        }
        return hours;
    }

    /**
     * ScheduleAddModelImple interface for presener implementation
     */
    interface ScheduleAddModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(String msg);

        void onScheduleError();
        void onDaysError();
        void onDurationError();
        void onStartTimeError();
        void onEndTimeError();
        void onTimeSelectedError();
        void onLocationError();
    }
}
