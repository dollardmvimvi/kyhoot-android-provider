package com.kyhoot.pro.main.normal.history;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kyhoot.pro.R;
import com.kyhoot.pro.main.schedule.bookingschedule.BookingSchedulePresenter;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.booking.CancelPojo;
import com.kyhoot.pro.pojo.booking.ServiceItem;
import com.kyhoot.pro.pojo.history.Accounting;
import com.kyhoot.pro.pojo.history.AdditionalService;
import com.kyhoot.pro.pojo.history.CustomerData;
import com.kyhoot.pro.pojo.history.ReviewByProvider;
import com.kyhoot.pro.pojo.shedule.ScheduleBookngPojo;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CircleTransform;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class HistoryInvoiceActivity extends AppCompatActivity implements BookingSchedulePresenter.BookingSchedulePresenterImple {

    private SessionManager sessionManager;
    private BookingSchedulePresenter presenter;
    private ProgressDialog progressDialog;

    private TextView tveventId, tvDate, tvPrice, tvStatus, tvCustomerName, tvYouRated;
    private TextView tvJobLocation;
    private TextView tvDiscount, tvCancelFee, tvTravelFee, tvVisitFee, tvLastDue;
    private TextView tvTotal, tvPaymentMethod, tvPaymentCardCash, tvPaymentWallet, tvEarnedAmount, tvAppComssion, tvCancelReason;
    private TextView tvpgComission;
    private RelativeLayout rlWalletPaymentDetails;

    private ImageView ivCustomer, ivSignature;
    private LinearLayout llRating, llAmount, llSignature, llCancel;
    private RelativeLayout rlCancelFee, rlPgCommisionn;
    private LinearLayout llService;

    private boolean isFromSchedule;
    private Booking booking;
    private ReviewByProvider reviewByProvider ;
    private Accounting accounting ;

    private Typeface fontRegular;
    private LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_invoice);

        init();
    }

    private void init()
    {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new BookingSchedulePresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingDetails));

        inflater = getLayoutInflater();

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);

        tveventId = findViewById(R.id.tveventId);
        tvDate = findViewById(R.id.tvDate);
        tvStatus = findViewById(R.id.tvStatus);
        tvPrice = findViewById(R.id.tvPrice);
        TextView tvamountPaidLabel = findViewById(R.id.tvamountPaidLabel);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        TextView tvYouRatedLabel = findViewById(R.id.tvYouRatedLabel);
        tvYouRated = findViewById(R.id.tvYouRated);
        TextView tvJobLocationLabel = findViewById(R.id.tvJobLocationLabel);
        tvJobLocation = findViewById(R.id.tvJobLocation);
        TextView tvPaymentBreakDownLabel = findViewById(R.id.tvPaymentBreakDownLabel);
        TextView tvDiscountLabel = findViewById(R.id.tvDiscountLabel);
        TextView tvCancelFeeLabel = findViewById(R.id.tvCancelFeeLabel);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvCancelFee = findViewById(R.id.tvCancelFee);

        TextView tvTravelFeeLabel = findViewById(R.id.tvTravelFeeLabel);
        tvTravelFee = findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = findViewById(R.id.tvVisitFeeLabel);
        tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = findViewById(R.id.tvLastDueLabel);
        tvLastDue = findViewById(R.id.tvLastDue);

        TextView tvTotalLabel = findViewById(R.id.tvTotalLabel);
        tvTotal = findViewById(R.id.tvTotal);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);

        tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
        tvPaymentCardCash = findViewById(R.id.tvPaymentCardCash);
        TextView tvPaymentMethodWallet = findViewById(R.id.tvPaymentMethodWallet);
        tvPaymentWallet = findViewById(R.id.tvPaymentWallet);
        rlWalletPaymentDetails = findViewById(R.id.rlWalletPaymentDetails);

        TextView tvSignatureHeader = findViewById(R.id.tvSignatureHeader);
        TextView tvCancelReasonLabel = findViewById(R.id.tvCancelReasonLabel);

        TextView tvYourEarningsLabel = findViewById(R.id.tvYourEarningsLabel);
        TextView tvEarnedAmountLabel = findViewById(R.id.tvEarnedAmountLabel);
        tvEarnedAmount = findViewById(R.id.tvEarnedAmount);
        TextView tvAppComssionLabel = findViewById(R.id.tvAppComssionLabel);
        tvAppComssion = findViewById(R.id.tvAppComssion);
        tvCancelReason = findViewById(R.id.tvCancelReason);
        TextView tvpgComssionLabel = findViewById(R.id.tvpgComssionLabel);
        tvpgComission = findViewById(R.id.tvpgComission);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ivCustomer = findViewById(R.id.ivCustomer);
        ivSignature = findViewById(R.id.ivSignature);

        llRating = findViewById(R.id.llRating);
        llAmount = findViewById(R.id.llAmount);
        llSignature = findViewById(R.id.llSignature);
        llCancel = findViewById(R.id.llCancel);
        rlCancelFee = findViewById(R.id.rlCancelFee);
        rlPgCommisionn = findViewById(R.id.rlPgCommisionn);

        llService = findViewById(R.id.llService);

        tveventId.setTypeface(fontMedium);
        tvDate.setTypeface(fontBold);
        tvStatus.setTypeface(fontMedium);
        tvPrice.setTypeface(fontBold);
        tvamountPaidLabel.setTypeface(fontRegular);
        tvCustomerName.setTypeface(fontMedium);
        tvYouRatedLabel.setTypeface(fontRegular);
        tvYouRated.setTypeface(fontRegular);
        tvJobLocationLabel.setTypeface(fontMedium);
        tvJobLocation.setTypeface(fontRegular);
        tvCancelReasonLabel.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontMedium);
        tvTotal.setTypeface(fontMedium);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontMedium);
        tvPaymentCardCash.setTypeface(fontMedium);
        tvPaymentMethodWallet.setTypeface(fontMedium);
        tvPaymentWallet.setTypeface(fontMedium);
        tvSignatureHeader.setTypeface(fontMedium);

        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);

        tvYourEarningsLabel.setTypeface(fontMedium);
        tvEarnedAmountLabel.setTypeface(fontRegular);
        tvEarnedAmount.setTypeface(fontRegular);
        tvAppComssionLabel.setTypeface(fontRegular);
        tvAppComssion.setTypeface(fontRegular);
        tvCancelReason.setTypeface(fontRegular);
        tvpgComssionLabel.setTypeface(fontRegular);
        tvpgComission.setTypeface(fontRegular);

        tvPaymentBreakDownLabel.setTypeface(fontMedium);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvCancelFeeLabel.setTypeface(fontRegular);
        tvCancelFee.setTypeface(fontRegular);

        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
            }
        });

        isFromSchedule = getIntent().getBooleanExtra("isFromSchedule",false);
        if(!isFromSchedule)
        {
            Bundle bundle = getIntent().getExtras();
            booking = (Booking) bundle.getSerializable("booking");
            reviewByProvider = booking.getReviewByProvider();
            accounting = booking.getAccounting();
            setValues();
        }
        else
        {
            presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),getIntent().getStringExtra("bookingId"));
        }
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
        }
    }


    @SuppressLint("SetTextI18n")
    private void setValues()
    {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        SimpleDateFormat displayMonthYearFormat = new SimpleDateFormat("MMMM yyyy", Locale.US);
        SimpleDateFormat dateOftheDay = new SimpleDateFormat("dd", Locale.US);

        tveventId.setText(getText(R.string.jobId)+" "+booking.getBookingId());
        tvStatus.setText(booking.getStatusMsg());
        tvPrice.setText(Utility.getPrice(booking.getAccounting().getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvCustomerName.setText(booking.getFirstName() + " "+ booking.getLastName());
        tvJobLocation.setText(booking.getAddLine1() + " "+booking.getAddLine2());
        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvEarnedAmount.setText(Utility.getPrice(accounting.getProviderEarning(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvAppComssion.setText(Utility.getPrice(accounting.getAppEarningPgComm(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if(reviewByProvider !=null && reviewByProvider.getRating() != null)
        {
            tvYouRated.setText(Utility.roundString(reviewByProvider.getRating(),1));
        }

        if(accounting.getTravelFee() !=null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.00") )
        {
            findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if(accounting.getVisitFee() !=null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00"))
        {
            findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if(accounting.getLastDues() !=null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00"))
        {
            findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if(accounting.getPaymentMethod().equals("1"))
        {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash,0,0,0);
        }
        else
        {
            tvPaymentMethod.setText(getString(R.string.card)+"  "+accounting.getLast4());
        }

        tvPaymentCardCash.setText(Utility.getPrice(accounting.getRemainingAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if(accounting.getPaidByWallet() !=null && accounting.getPaidByWallet().equals("1"))
        {
            rlWalletPaymentDetails.setVisibility(View.VISIBLE);
            tvPaymentWallet.setText(Utility.getPrice(accounting.getCaptureAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if(!accounting.getPgCommissionProvider().equals("") && !accounting.getPgCommissionProvider().equals("0"))
        {
            rlPgCommisionn.setVisibility(View.VISIBLE);
            tvpgComission.setText(Utility.getPrice(accounting.getPgCommissionProvider(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if(booking.getServiceType().equals("1"))
        {
            if(booking.getCartData() != null)
            {
                for(ServiceItem serviceItem : booking.getCartData())
                {
                    View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history,null);
                    TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                    TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                    tvServiceFeeLabel.setTypeface(fontRegular);
                    tvServiceFee.setTypeface(fontRegular);
                    tvServiceFeeLabel.setText(serviceItem.getServiceName() +" X "+ serviceItem.getQuntity());
                    tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                    llService.addView(serviceView);
                }
            }
        }
        else
        {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees,null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
            tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llService.addView(serviceView);
        }

        if(booking.getAdditionalService() !=null && booking.getAdditionalService().size() > 0)
        {
            for(AdditionalService serviceItem : booking.getAdditionalService())
            {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history,null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getPrice(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        }

        switch (booking.getStatus())
        {
            case VariableConstant.JOB_COMPLETED_RAISE_INVOICE:
                tvStatus.setBackgroundColor(ContextCompat.getColor(this,R.color.jobStatusCompleted));
                break;

            case VariableConstant.CANCELLED_BY_PROVIDER:
            case VariableConstant.CANCELLED_BY_CUTOMER:
                llCancel.setVisibility(View.VISIBLE);
                rlCancelFee.setVisibility(View.VISIBLE);
                tvCancelReason.setText(booking.getCancellationReason());
                tvCancelFee.setText(Utility.getPrice(accounting.getCancellationFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                tvStatus.setBackgroundColor(ContextCompat.getColor(this,R.color.jobStatusCanceled));
                llRating.setVisibility(View.GONE);
                llSignature.setVisibility(View.GONE);
                break;

            case VariableConstant.BOOKING_EXPIRED:
                tvStatus.setBackgroundColor(ContextCompat.getColor(this,R.color.jobStatusExpired));
                llRating.setVisibility(View.GONE);
                llAmount.setVisibility(View.GONE);
                llSignature.setVisibility(View.GONE);
                break;

            case VariableConstant.REJECT:
                tvStatus.setBackgroundColor(ContextCompat.getColor(this,R.color.jobStatusDeclined));
                llRating.setVisibility(View.GONE);
                llAmount.setVisibility(View.GONE);
                llSignature.setVisibility(View.GONE);
                break;

            default:
                tvStatus.setBackgroundColor(ContextCompat.getColor(this,R.color.jobStatusDeclined));
                llRating.setVisibility(View.GONE);
                llAmount.setVisibility(View.GONE);
                llSignature.setVisibility(View.GONE);
                break;
        }

        if(isFromSchedule)
        {
            CustomerData customerData = booking.getCustomerData();
            tvCustomerName.setText(customerData.getFirstName() + " "+ customerData.getLastName());
            if(!customerData.getProfilePic().equals(""))
            {
                Glide.with(this).
                        load(customerData.getProfilePic())
                        .error(R.drawable.profile_default_image)
                        .transform(new CircleTransform(this))
                        .placeholder(R.drawable.profile_default_image)
                        .into(ivCustomer);
            }

        }
        else if(!booking.getProfilePic().equals(""))
        {
            Glide.with(this).
                    load(booking.getProfilePic())
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image)
                    .into(ivCustomer);
        }

        if(!booking.getSignatureUrl().equals(""))
        {
            Glide.with(this).
                    load(booking.getSignatureUrl())
                    .into(ivSignature);
        }

        try {
            String monthYear = displayMonthYearFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingRequestedFor())));
            int date = Integer.parseInt(dateOftheDay.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingRequestedFor()))));
            tvDate.setText(Utility.getDayOfMonthSuffix(date,monthYear));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(ScheduleBookngPojo scheduleBookngPojo) {
        booking = scheduleBookngPojo.getData();
        reviewByProvider = booking.getReviewByProvider();
        accounting = booking.getAccounting();
        setValues();
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {

    }

    @Override
    public void onCancelBooking(String msg) {
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }
}

