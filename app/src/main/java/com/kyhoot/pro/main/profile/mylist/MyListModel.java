package com.kyhoot.pro.main.profile.mylist;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.kyhoot.pro.pojo.profile.ProfileData;
import com.kyhoot.pro.pojo.profile.ProfilePojo;
import com.kyhoot.pro.utility.OkHttp3ConnectionStatusCode;
import com.kyhoot.pro.utility.ServiceUrl;
import com.kyhoot.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by murashid on 13-Sep-17.
 * <h1>MyListModel</h1>
 * MyListModel model for MyListAcitivty
 * @see MyListActivity
 */

public class MyListModel{
    private static final String TAG = "MyListModel";
    private MyListModelImple modelImplement;

    MyListModel(MyListModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for getting profile details
     * @param sessiontoken session Token
     */
    void getProfile(String sessiontoken)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        ProfilePojo profilePojo = gson.fromJson(result,ProfilePojo.class);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(profilePojo.getData());
                        } else {
                            modelImplement.onFailure(profilePojo.getMessage());
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * method for update the profile status
     * @param sessiontoken session token
     * @param param key of the value
     * @param value new value
     */
    void updateProfileStatus(String sessiontoken, final String param, String value)
    {
        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put(param,value);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessUpdateStatus(jsonObject.getString("message"));
                        } else {
                            if(param.equals("profileStatus"))
                            {
                                modelImplement.onFailureUpdateStatus();
                            }
                            else
                            {
                                modelImplement.onFailureLocation();
                            }
                        }
                    }
                    else
                        {
                        if(param.equals("profileStatus"))
                        {
                            modelImplement.onFailureUpdateStatus();
                        }
                        else
                        {
                            modelImplement.onFailureLocation();
                        }

                    }
                }
                catch (Exception e)
                {
                    if(param.equals("profileStatus"))
                    {
                        modelImplement.onFailureUpdateStatus();
                    }
                    else
                    {
                        modelImplement.onFailureLocation();
                    }

                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                if(param.equals("profileStatus"))
                {
                    modelImplement.onFailureUpdateStatus();
                }
                else
                {
                    modelImplement.onFailureLocation();
                }

            }
        });
    }


    /**
     * method for calling api for upadater the mylist field based on values and param
     * @param sessiontoken session Token
     * @param jsonObject jsonObject
     */
    void updateWorkImage(String sessiontoken, JSONObject jsonObject)
    {

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessUpdateStatus(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }


    public void uploadImageOnServer(File mFileTemp) {
        new UploadFileToServer().execute(mFileTemp.getPath());
    }

    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result, responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(60, TimeUnit.SECONDS);
                builder.readTimeout(60, TimeUnit.SECONDS);
                builder.writeTimeout(60, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(ServiceUrl.IMAGE_UPLOAD_ON_SERVER)
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d(TAG, "doInBackground: " + responseCode);
                Log.d(TAG, "doInBackground: " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    String image = jsonObject.getString("data");
                    modelImplement.onImageUploadSuccess(image);

                } else
                    modelImplement.onImageUploadError();

            } catch (JSONException e) {
                e.printStackTrace();
                modelImplement.onImageUploadError();
            }
            super.onPostExecute(result);
        }

    }




    /**
     * MyListModelImple interface for presenter implementation
     */
    interface MyListModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onFailureUpdateStatus();
        void onFailureLocation();
        void onSuccessUpdateStatus(String msg);
        void onSuccess(ProfileData profileData);
        void onImageUploadSuccess(String path);
        void onImageUploadError();
    }
}
