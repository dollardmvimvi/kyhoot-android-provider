package com.kyhoot.pro.main.profile.bank;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.kyhoot.pro.R;
import com.kyhoot.pro.adapters.BankListAdapter;
import com.kyhoot.pro.pojo.profile.bank.BankList;
import com.kyhoot.pro.pojo.profile.bank.LegalEntity;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankDetailsActivity</h1>
 * BankDetailsActivity activity for showing the BankList and Stripe Account
 */

public class BankDetailsActivity extends AppCompatActivity implements BankDetailsPresenter.BankDetailsPresenterImplement, BankListAdapter.RefreshBankDetails, View.OnClickListener {

    SessionManager sessionManager;

    private ProgressDialog pDialog;
    private TextView tvStatus;
    private TextView tvStipeAccountNo;
    private TextView tvAddBankAccount;
    private TextView tvAddStripeAccount;
    private CardView cvStipeDetails,cvLinkBankAcc;
    private BankListAdapter bankListAdapter;
    private ArrayList<BankList> bankLists;
    private BankDetailsPresenter bankListFragPresenter;
    private Bundle bundleBankDetails;
    private ImageView ivStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);

        init();
    }

    /**
     * init the views
     */
    private void init() {
        bundleBankDetails = new Bundle();
        sessionManager = SessionManager.getSessionManager(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.gettingBankDetails));
        pDialog.setCancelable(false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.bankDetails));
        tvTitle.setTypeface(fontBold);


        tvStatus = findViewById(R.id.tvStatus);
        TextView tvStep2 = findViewById(R.id.tvStep2);
        TextView tvStep1 = findViewById(R.id.tvStep1);
        ivStatus = findViewById(R.id.ivStatus);
        tvStipeAccountNo = findViewById(R.id.tvStipeAccountNo);
        tvAddStripeAccount = findViewById(R.id.tvAddStripeAccount);
        tvAddBankAccount = findViewById(R.id.tvAddBankAccount);
        cvStipeDetails = findViewById(R.id.cvStipeDetails);
        cvLinkBankAcc = findViewById(R.id.cvLinkBankAcc);


        tvStep2.setTypeface(fontRegular);
        tvStep1.setTypeface(fontRegular);
        tvStatus.setTypeface(fontRegular);
        tvStipeAccountNo.setTypeface(fontRegular);
        tvAddBankAccount.setTypeface(fontRegular);
        tvAddStripeAccount.setTypeface(fontRegular);

        RecyclerView rvBank = findViewById(R.id.rvBank);
        rvBank.setLayoutManager(new LinearLayoutManager(this));
        bankLists = new ArrayList<>();
        bankListAdapter = new BankListAdapter(this, bankLists, getSupportFragmentManager(), this);
        rvBank.setAdapter(bankListAdapter);

        bankListFragPresenter = new BankDetailsPresenter(this);

        tvAddStripeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BankDetailsActivity.this, BankNewStripeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
            }
        });

        cvStipeDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent intent = new Intent(BankDetailsActivity.this, BankNewStripeActivity.class);
                intent.putExtras(bundleBankDetails);
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);*/
            }
        });

        tvAddBankAccount.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        bankListFragPresenter.getBankDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }

    @Override
    public void startProgressBar() {
        pDialog.show();
    }

    @Override
    public void stopProgressBar() {
        pDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        stripeError();
    }

    private void stripeError() {
        tvAddStripeAccount.setVisibility(View.VISIBLE);
        tvStatus.setVisibility(View.GONE);
        ivStatus.setVisibility(View.GONE);
        tvStipeAccountNo.setVisibility(View.GONE);
        tvAddStripeAccount.setOnClickListener(null);
        tvAddStripeAccount.setBackground(ContextCompat.getDrawable(this,R.drawable.rectangle_corner_gray_background));
        tvAddStripeAccount.setTextColor(ContextCompat.getColor(this,R.color.gunsmoke));
        tvAddBankAccount.setOnClickListener(null);
        tvAddBankAccount.setBackground(ContextCompat.getDrawable(this,R.drawable.rectangle_corner_gray_background));
        tvAddBankAccount.setTextColor(ContextCompat.getColor(this,R.color.gunsmoke));
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
        stripeError();
    }

    @Override
    public void onSuccess(LegalEntity legalEntity, ArrayList<BankList> bankLists) {
        VariableConstant.IS_STRIPE_ADDED = true;
        tvAddStripeAccount.setVisibility(View.GONE);
        tvStatus.setVisibility(View.VISIBLE);
        ivStatus.setVisibility(View.VISIBLE);
        tvStipeAccountNo.setVisibility(View.VISIBLE);

        tvStipeAccountNo.setText(getText(R.string.stripeAccountNo) + " xxxxxx");
        String status = legalEntity.getVerification().getStatus() ;
        tvStatus.setText(status.substring(0,1).toUpperCase() + status.substring(1));
        if (legalEntity.getVerification().getStatus().equals("verified")) {
            ivStatus.setImageResource(R.drawable.vector_tick_account_no_verified);
            ivStatus.setVisibility(View.VISIBLE);
            tvStatus.setTextColor(ContextCompat.getColor(this, R.color.verifiedBank));
            tvAddBankAccount.setFocusable(true);
            tvAddBankAccount.setOnClickListener(this);
            tvAddBankAccount.setBackground(ContextCompat.getDrawable(this,R.drawable.selector_for_rectangle_color_primary_with_stroke));
            tvAddBankAccount.setTextColor(ContextCompat.getColor(this,R.color.selector_colorprimary_white));

            if(bankLists.size()>0)
            {
                tvAddBankAccount.setVisibility(View.GONE);
            }

        } else {
            tvStatus.setText(getString(R.string.waitingForVerification));
            ivStatus.setImageResource(R.drawable.vector_error);
            tvStatus.setTextColor(ContextCompat.getColor(this, R.color.nonVerifiedBank));
            tvAddBankAccount.setFocusable(false);
            tvAddBankAccount.setOnClickListener(null);
            tvAddBankAccount.setBackground(ContextCompat.getDrawable(this,R.drawable.rectangle_corner_gray_background));
            tvAddBankAccount.setTextColor(ContextCompat.getColor(this,R.color.gunsmoke));
        }

        this.bankLists.clear();
        this.bankLists.addAll(bankLists);
        bankListAdapter.notifyDataSetChanged();

        bundleBankDetails.putString("fname", legalEntity.getFirstName());
        bundleBankDetails.putString("lname", legalEntity.getLastName());
        bundleBankDetails.putString("country", legalEntity.getAddress().getCountry());
        bundleBankDetails.putString("state", legalEntity.getAddress().getState());
        bundleBankDetails.putString("city", legalEntity.getAddress().getCity());
        bundleBankDetails.putString("address", legalEntity.getAddress().getLine1());
        bundleBankDetails.putString("postalcode", legalEntity.getAddress().getPostalCode());
        bundleBankDetails.putString("month", legalEntity.getDob().getMonth());
        bundleBankDetails.putString("day", legalEntity.getDob().getDay());
        bundleBankDetails.putString("year", legalEntity.getDob().getYear());
    }

    @Override
    public void showAddStipe(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

        tvAddStripeAccount.setVisibility(View.VISIBLE);
        tvStatus.setVisibility(View.GONE);
        ivStatus.setVisibility(View.GONE);
        tvStipeAccountNo.setVisibility(View.GONE);
        tvAddBankAccount.setOnClickListener(null);
        tvAddBankAccount.setBackground(ContextCompat.getDrawable(this,R.drawable.rectangle_corner_gray_background));
        tvAddBankAccount.setTextColor(ContextCompat.getColor(this,R.color.gunsmoke));
    }

    @Override
    public void onRefresh() {
        bankListFragPresenter.getBankDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.tvAddBankAccount){
            Intent intent = new Intent(this, BankNewAccountActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
        }
    }
}
