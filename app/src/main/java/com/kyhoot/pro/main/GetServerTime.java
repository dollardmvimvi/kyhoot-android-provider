package com.kyhoot.pro.main;

import android.util.Log;

import com.kyhoot.pro.utility.OkHttp3ConnectionStatusCode;
import com.kyhoot.pro.utility.ServiceUrl;

import org.json.JSONObject;

/**
 * Created by murashid on 28-Apr-18.
 */

public class GetServerTime {

    private GetServerTimeResponse getServerTimeResponse;

    public GetServerTime(GetServerTimeResponse getServerTimeResponse)
    {
        this.getServerTimeResponse = getServerTimeResponse;
    }

    public void getServerTime()
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.SERVER_TIME, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        Log.d("GetServerTime", "onSuccess: "+result);
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            getServerTimeResponse.onSuccessServerTime(jsonObject.getString("data"));
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {

                    }
                });
    }

    public interface GetServerTimeResponse
    {
        void onSuccessServerTime(String timeStamp);
    }

}
