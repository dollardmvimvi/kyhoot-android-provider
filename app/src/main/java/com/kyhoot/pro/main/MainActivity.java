package com.kyhoot.pro.main;

import android.animation.Animator;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kyhoot.pro.R;
import com.kyhoot.pro.main.booking.BookingFragment;
import com.kyhoot.pro.main.normal.history.HistoryGraphFragment;
import com.kyhoot.pro.main.normal.earning.EarningWebFragment;
import com.kyhoot.pro.main.profile.ProfileFragment;
import com.kyhoot.pro.main.schedule.ScheduleFragment;
import com.kyhoot.pro.pojo.appconfig.AppConfigData;
import com.kyhoot.pro.service.LocationPublishService;
import com.kyhoot.pro.service.OfflineLocationPublishService;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.CustomTypefaceSpan;
import com.kyhoot.pro.utility.DialogHelper;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import org.json.JSONArray;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>MainActivity</h1>
 * MainActivity that contain bottom Navigation view
 */

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, MainActivityPresenter.MainActivityPresenterImple, BookingFragment.BookingFragmentInteraction {

    private SessionManager sessionManager;
    private FragmentManager fragmentManager;
    private Fragment fragment;
    private FrameLayout flContainer;
    private long backPressed;
    private NotificationManager notificationManager;
    private Animation fade_open;
    private boolean isResume= false;

    private AlertDialog dialogGps;
    private Status status;
    private BroadcastReceiver receiver;

    private int fragmentSelectedPosition = 0;
    private int fragmentOldPosition = -1;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);

        if(hasFocus)
        {
            if(!Utility.isGpsEnabled(this))
            {
                if(status != null)
                {
                    showGpsAlert();
                }
             /*   else if(!dialogGps.isShowing())
                {
                    dialogGps.show();
                }*/
            }
            else if(dialogGps!=null && dialogGps.isShowing())
            {
                dialogGps.dismiss();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sessionManager = SessionManager.getSessionManager(this);
        fragmentManager = getSupportFragmentManager();
        fragment = BookingFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.flContainer, fragment).commit();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);

        Typeface fontBold = Utility.getFontBold(this);

        flContainer = findViewById(R.id.flContainer);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        removeShiftModeAndApplyFont(navigation,fontBold);

        MainActivityPresenter presenter = new MainActivityPresenter(this);
        presenter.getAppConfig(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));

        if(sessionManager.getIsNewBooking())
        {
            sessionManager.setIsNewBooking(false);
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            if(sessionManager.getIsNowBooking())
            {
                AppController.getInstance().getMixpanelHelper().timeStartForNowBookingRespond();
            }
            else
            {
                AppController.getInstance().getMixpanelHelper().timeStartForLaterBookingRespond();
            }

            if(sessionManager.getIsBidBooking() && sessionManager.getIsAssignedBooking())
            {
                setAcceptedBookingFragment();
            }

            AppController.getInstance().startNewBookingRingtoneService();
        }

        isResume = true;
        VariableConstant.IS_APPLICATION_RUNNING = true ;

        dialogGps = DialogHelper.adEnableGPS( this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_GPS_OFF);
        receiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                try {
                    status = intent.getParcelableExtra("gps");
                    showGpsAlert();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        registerReceiver(receiver, filter);
        AppController.getInstance().getMixpanelHelper().timeForAppOpened();
        AppController.getInstance().getCorrectTimeZoneByLatLng();

        boolean showDialog = getIntent().getBooleanExtra("showDialog",true);
        if(showDialog)
        {
            sessionManager.setAppOpenTime(sessionManager.getAppOpenTime() + 1);
            if(sessionManager.getAppOpenTime() % 5 == 0 && !sessionManager.getDontShowRate())
            {
                DialogHelper.rateApp(this, sessionManager);
            }
        }
    }

    private void showGpsAlert()
    {
        try {
            status.startResolutionForResult(MainActivity.this, VariableConstant.REQUEST_CODE_GPS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        fragmentOldPosition = fragmentSelectedPosition;

        switch (item.getItemId())
        {
            case R.id.navigation_booking:
                fragment = BookingFragment.newInstance();
                fragmentSelectedPosition = 1;
                break;

            case R.id.navigation_history:
                fragment = HistoryGraphFragment.newInstance();
                fragmentSelectedPosition = 2;
                break;

            case R.id.navigation_schedule:
                fragment = ScheduleFragment.newInstance();
                fragmentSelectedPosition = 3;
                break;

            case R.id.navigation_earning:
                fragment = EarningWebFragment.newInstance();
                fragmentSelectedPosition = 4;
                break;

            case R.id.navigation_profile:
                fragment = ProfileFragment.newInstance();
                fragmentSelectedPosition = 5;
                break;
        }

        if(fragmentSelectedPosition == fragmentOldPosition)
        {
            return true;
        }

        if(fragment!=null)
        {
            fragmentManager.beginTransaction().replace(R.id.flContainer, fragment).commit();
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
        {
            Animator anim = ViewAnimationUtils.createCircularReveal(flContainer, flContainer.getWidth(), flContainer.getHeight(),0 ,flContainer.getHeight());
            anim.setDuration(400);
            anim.start();
        }
        else
        {
            flContainer.startAnimation(fade_open);
        }

        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        //notificationManager.cancelAll();
    }

    /**
     * customise the bottom navigation view for showing icons and texts
     * @param bottomNavigationView MainActivity's bottomNavigationView
     * @param font typeface for bottom navigation texts
     */
    void removeShiftModeAndApplyFont(BottomNavigationView bottomNavigationView, Typeface font) {

        BottomNavigationMenuView menuView = (BottomNavigationMenuView)bottomNavigationView.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShifting");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            if (menuView != null)
            {
                for (int i = 0; i < menuView.getChildCount(); i++)
                {
                    BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                    TextView smallText = item.findViewById(R.id.smallLabel);
                    smallText.setVisibility(View.GONE);
                    ImageView icon = item.findViewById(R.id.icon);
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) icon.getLayoutParams();
                    params.gravity = Gravity.CENTER;
                    item.setShifting(false);
//                    item.setShiftingMode(false);
                    item.setPadding(0, 0, 0, 0);
                    item.setChecked(item.getItemData().isChecked());
                }

                Menu m = bottomNavigationView.getMenu();
                for (int i=0;i<m.size();i++) {
                    MenuItem mi = m.getItem(i);
                    SpannableString mNewTitle = new SpannableString(mi.getTitle());
                    mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                    mi.setTitle(mNewTitle);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if(fragment instanceof  ScheduleFragment)
        {
            if(((ScheduleFragment) fragment).isFabOpen )
            {
                ((ScheduleFragment) fragment).animateFAB();
                return;
            }
        }

        if(fragment instanceof EarningWebFragment)
        {
            if(((EarningWebFragment)fragment).canGoBack())
            {
                return;
            }
        }

        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
        }
        else
        {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.doublePressExit), Toast.LENGTH_SHORT).show();

        }
        backPressed = System.currentTimeMillis();
    }

    @Override
    public void onSuccess(AppConfigData appConfigData)
    {
        try {
            sessionManager.setLocationPublishInterval(appConfigData.getProviderFrequency().getLocationPublishInterval());
            sessionManager.setLiveTrackInterval(appConfigData.getProviderFrequency().getLiveTrackInterval());
            sessionManager.setProTimeOut(appConfigData.getProviderFrequency().getProTimeOut());
            sessionManager.setLatLongDisplacement(appConfigData.getLatLongDisplacement());
            sessionManager.setStripeKey(appConfigData.getStripeKeys());

            sessionManager.setCurrency(appConfigData.getCurrency());
            sessionManager.setCurrencySymbol(appConfigData.getCurrencySymbol());
            sessionManager.setCurrencyAbbrevation(appConfigData.getCurrencyAbbr());
            sessionManager.setDistanceUnit(appConfigData.getDistanceMatrix());

            if(appConfigData.getWalletData() != null)
            {
                sessionManager.setWalletEnable(appConfigData.getWalletData().getEnableWallet());
            }

            boolean showDialog = getIntent().getBooleanExtra("showDialog",true);
            if(Utility.isLatestVersion(appConfigData.getAppVersion()) && isResume && showDialog)
            {
                if(appConfigData.getMandatory().equals("true"))
                {
                    //show Mandatory
                    DialogHelper.appUpdateMandatory(this);
                }
                else
                {
                    //show nonMandatory
                    DialogHelper.appUpdateNonMandatory(this);
                }
            }

            FirebaseMessaging.getInstance().subscribeToTopic(appConfigData.getPushTopics().getAllCities());
            FirebaseMessaging.getInstance().subscribeToTopic(appConfigData.getPushTopics().getAllProvider());

            if(appConfigData.getPushTopics().getCity() != null && !appConfigData.getPushTopics().getCity().equals(""))
                FirebaseMessaging.getInstance().subscribeToTopic(appConfigData.getPushTopics().getCity());

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(Utility.isMyServiceRunning(this,OfflineLocationPublishService.class))
        {
            Intent stopIntent = new Intent(this, OfflineLocationPublishService.class);
            stopIntent.setAction(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE);
            startService(stopIntent);

            if(AppController.getInstance().getMqttHelper().isMqttConnected() && !sessionManager.getIsDriverOnline())
            {
                AppController.getInstance().getMqttHelper().disconnect();
            }
        }

        unregisterReceiver(receiver);

        AppController.getInstance().getMixpanelHelper().timeForAppClosed();
        isResume = false;
        VariableConstant.IS_APPLICATION_RUNNING = false ;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == VariableConstant.REQUEST_CODE_GPS)
        {
            if(resultCode == RESULT_OK)
            {
                Intent startIntent = new Intent(this, LocationPublishService.class);
                startIntent.setAction(VariableConstant.ACTION.STARTFOREGROUND_ACTION);
                startService(startIntent);

                if(dialogGps.isShowing())
                {
                    dialogGps.dismiss();
                }

            }
            else if(resultCode == RESULT_CANCELED)
            {
                if(!dialogGps.isShowing())
                {
                    dialogGps.show();
                }
            }
        }
    }

    void check()
    {
        /*
        * difference bettween forecah and for loop
        * for each automatically use getIndex
        *
        * */

        List<String> fruitList = new ArrayList<>();
        //adding String Objects to fruitsList ArrayList
        fruitList.add("Mango");
        fruitList.add("Banana");
        fruitList.add("Apple");
        fruitList.add("Strawberry");
        fruitList.add("Pineapple");
        System.out.println("Converting ArrayList to Array" );
        String[] item = fruitList.toArray(new String[fruitList.size()]);
        for(String s : item){
            System.out.println(s);
        }
        System.out.println("Converting Array to ArrayList" );
        List<String> l2 =  Arrays.asList(item);
        System.out.println(l2);


        String mStringArray[] = { "String1", "String2" };
        JSONArray mJSONArray = new JSONArray(Arrays.asList(mStringArray));

        ArrayList<String> list = new ArrayList<String>();
        list.add("blah");
        list.add("bleh");
        JSONArray jsArray = new JSONArray(list);


        String bind = "My name is billa val djsfsjfj dsf dfe  pooghathe ooru illa ya aiduf k jdfjkdljf jdfj zkjdjf  Mu df Murasidf Murasdifh ";
    }

    @Override
    public void setAcceptedBookingFragment() {
        fragment = BookingFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.flContainer, fragment).commit();
    }
}
