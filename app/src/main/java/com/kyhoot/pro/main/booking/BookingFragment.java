package com.kyhoot.pro.main.booking;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kyhoot.pro.R;
import com.kyhoot.pro.adapters.ViewPagerAdapter;
import com.kyhoot.pro.main.bid.acceptedbooking.AcceptedBookingFragment;
import com.kyhoot.pro.pojo.booking.Booking;
import com.kyhoot.pro.pojo.booking.BookingPojo;
import com.kyhoot.pro.service.LocationPublishService;
import com.kyhoot.pro.service.OfflineLocationPublishService;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.MixpanelEvents;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;

import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>BookingFragment</h1>
 * BookingFragment for showing Events Details
 */

public class BookingFragment extends Fragment implements View.OnClickListener, BookingPresenter.View, BookingListFragment.BookingListFragmentInteraction {

    private Typeface fondBold, fontRegular;
    private LayoutInflater inflater;
    private TextView tvCountFirstTab, tvCountSecondTab;

    private static final String TAG = "BookingFragment";
    private TextView tvOnlineOffline, tvStatus;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private BookingPresenter presenter;
    private BookingListFragment offersFragment/*, dynamicFragment*/;
    private AcceptedBookingFragment acceptedBookingFragment;
    private ViewPager vpBooking;

    private ArrayList<Booking> offersBookings, dynamicBooking;
    private int acceptedBookingSize = 0;

    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private Gson gson;

    private long lastRefreshTime;

    private BookingFragmentInteraction mListener;

    public static BookingFragment newInstance() {
        return new BookingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        this.inflater = inflater;
        View rootView = inflater.inflate(R.layout.fragment_booking, container, false);

        init(rootView);

        return rootView;
    }

    /**
     * init  the views
     * @param rootView   parent View
     */
    private void init(View rootView) {

        filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_MAKE_ONLINE);
        filter.addAction(VariableConstant.INTENT_ACTION_MAKE_OFFLINE);
        filter.addAction(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION);

        receiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                //notificationManager.cancelAll();
                if(intent.getAction().equals(VariableConstant.INTENT_ACTION_REFRESH_BOOKING))
                {
                    if (lastRefreshTime + 700 < System.currentTimeMillis())
                    {
                        AppController.getInstance().startNewBookingRingtoneService();

                        progressDialog.setMessage(getString(R.string.refreshing));
                        presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),false);

                        if(vpBooking !=null )
                        {
                            if(!sessionManager.getIsAssignedBooking())
                            {
                                vpBooking.setCurrentItem(0);
                            }
                            else if(!sessionManager.getIsBidBooking())
                            {
                                vpBooking.setCurrentItem(1);
                            }
                            else
                            {
                                if(mListener != null)
                                {
                                    mListener.setAcceptedBookingFragment();
                                }
                            }
                        }

                    }
                    lastRefreshTime = System.currentTimeMillis();
                }
                else if(intent.getAction().equals(VariableConstant.INTENT_ACTION_CANCEL_BOOKING))
                {
                    String cancelid = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    for(Booking booking : offersBookings)
                    {
                        if(booking.getBookingId().equals(cancelid))
                        {
                            if (lastRefreshTime + 700 < System.currentTimeMillis())
                            {
                                progressDialog.setMessage(getString(R.string.refreshing));
                                presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),false);
                            }
                            lastRefreshTime = System.currentTimeMillis();
                            return;
                        }
                    }
                    /*for(Booking booking : dynamicBooking)
                    {
                        if(booking.getBookingId().equals(cancelid))
                        {
                            //DialogHelper.customAlertDialog(getActivity(), header, msg, getString(R.string.oK));
                            dynamicBooking.remove(booking);
                            dynamicFragment.notifyiDataChanged(dynamicBooking);
                            tvCountSecondTab.setText(""+dynamicBooking.size());
                            return;
                        }
                    }*/
                }
                else if(intent.getAction().equals(VariableConstant.INTENT_ACTION_MAKE_ONLINE ) && !sessionManager.getIsDriverOnline())
                {
                    progressDialog.setMessage(getString(R.string.goingOnline));
                    presenter.updateProviderStatues(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),VariableConstant.ONLINE_STATUS,sessionManager.getCurrentLat(),sessionManager.getCurrentLng());
                }
                else if(intent.getAction().equals(VariableConstant.INTENT_ACTION_MAKE_OFFLINE))
                {
                    setOnOffTheJob(false);
                }
                else if(intent.getAction().equals(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION))
                {
                    progressDialog.setMessage(getString(R.string.refreshing));
                    presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),false);
                }
            }
        };

        presenter = new BookingPresenter(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(getActivity());

        offersBookings = new ArrayList<>();
        dynamicBooking = new ArrayList<>();
        fondBold = Utility.getFontBold(getActivity());
        fontRegular = Utility.getFontRegular(getActivity());

        Log.d(TAG, "sessionToken: "+AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));

        offersFragment = BookingListFragment.newInstance("offers");
        if(sessionManager.getIsBidBooking())
        {
//            dynamicFragment = BookingListFragment.newInstance("bid");
            acceptedBookingFragment = AcceptedBookingFragment.newInstance("bid");
        }
        else
        {
//            dynamicFragment = BookingListFragment.newInstance("accepted");
             acceptedBookingFragment = AcceptedBookingFragment.newInstance("accepted");
        }
        offersFragment.setBookingListFragmentInteraction(this);
//        dynamicFragment.setBookingListFragmentInteraction(this);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        TabLayout tablayoutBooking = rootView.findViewById(R.id.tablayoutBooking);
        vpBooking = rootView.findViewById(R.id.vpBooking);
        viewPagerAdapter.addFragment(offersFragment,"");
//        viewPagerAdapter.addFragment(dynamicFragment,"");
        viewPagerAdapter.addFragment(acceptedBookingFragment,"");
        vpBooking.setAdapter(viewPagerAdapter);
        tablayoutBooking.setupWithViewPager(vpBooking);

        for(int i =0 ; i< tablayoutBooking.getTabCount() ; i++)
        {
            tablayoutBooking.getTabAt(i).setCustomView(getCustomTabView(i));
        }

        tvOnlineOffline = rootView.findViewById(R.id.tvOnlineOffline);
        tvOnlineOffline.setTypeface(fondBold);
        tvOnlineOffline.setOnClickListener(this);

        tvStatus = rootView.findViewById(R.id.tvStatus);
        tvStatus.setTypeface(fondBold);

        progressDialog.setMessage(getString(R.string.gettingBookings));

        gson = new Gson();

        if(sessionManager.getMyBooking().equals(""))
        {
            presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),
                    false);
        }
        else
        {
            presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onSuccesBooking(sessionManager.getMyBooking(),false);
                }
            },750);
        }

    }

    private View getCustomTabView(int i) {
        View view = inflater.inflate(R.layout.custom_tab_view_header_with_count, null, false);
        TextView tvHeader = view.findViewById(R.id.tvHeader);
        TextView tvCount = view.findViewById(R.id.tvCount);
        tvHeader.setTypeface(fondBold);
        tvCount.setTypeface(fontRegular);
        if(i==0)
        {
            if(sessionManager.getIsBidBooking())
            {
                tvHeader.setText(getString(R.string.offers));
            }
            else
            {
                tvHeader.setText(getString(R.string.requests));
            }
            tvCountFirstTab = tvCount;
        }
        else
        {
            if(sessionManager.getIsBidBooking())
            {
                tvHeader.setText(getString(R.string.activeBids));
            }
            else
            {
                tvHeader.setText(getString(R.string.upcoming));
            }
            tvCountSecondTab = tvCount;
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver,filter);
        VariableConstant.IS_MYBOOKING_OPENED = true;

        if(VariableConstant.IS_BOOKING_UPDATED)
        {
            VariableConstant.IS_BOOKING_UPDATED = false;
            progressDialog.setMessage(getString(R.string.refreshing));
            presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),false);
        }

        if(!sessionManager.getIsProfileAcivated())
        {
            enableOnlineOffline(false);
        }
        else if(sessionManager.getIsDriverDeactive())
        {
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_grey);
            tvOnlineOffline.setText(getString(R.string.deactivated));
        }
        else if(sessionManager.getIsDriverOnline())
        {
            tvOnlineOffline.setText(getResources().getString(R.string.goOffline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_green);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        AppController.getInstance().stopNewBookingRingtoneService();

        getActivity().unregisterReceiver(receiver);
        VariableConstant.IS_MYBOOKING_OPENED = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BookingFragmentInteraction) {
            mListener = (BookingFragmentInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        presenter.detach();
        super.onDestroyView();
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tvOnlineOffline:
                if(sessionManager.getIsDriverOnline())
                {
                    progressDialog.setMessage(getString(R.string.goingOffline));
                    presenter.updateProviderStatues(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),VariableConstant.OFFLINE_STATUS,sessionManager.getCurrentLat(),sessionManager.getCurrentLng());
                    AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.GoesOffline.value);
                    AppController.getInstance().getMixpanelHelper().offline();
                }
                else
                {
                    progressDialog.setMessage(getString(R.string.goingOnline));
                    presenter.updateProviderStatues(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),VariableConstant.ONLINE_STATUS,sessionManager.getCurrentLat(),sessionManager.getCurrentLng());
                    AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.GoesOnline.value);
                    AppController.getInstance().getMixpanelHelper().online();
                }
                break;
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
     /*   if(dynamicFragment !=null)
            dynamicFragment.stopRefreshing();*/
        if(offersFragment !=null)
            offersFragment.stopRefreshing();
        progressDialog.dismiss();
    }


    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,getActivity());
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        //Toast.makeText(getActivity(),getActivity().getString(R.string.serverError),Toast.LENGTH_SHORT).show();
        AppController.toast();
    }

    @Override
    public void onSuccesBooking(final String result, boolean isFromApi) {

        try {
            BookingPojo bookingPojo = gson.fromJson(result,BookingPojo.class);
            if(isAdded())
            {
                if(!sessionManager.getMyBooking().equals(result))
                {
                    sessionManager.setMyBooking(result);
                }

                offersBookings.clear();
                dynamicBooking.clear();
                offersBookings.addAll(bookingPojo.getData().getRequest());
                offersFragment.notifyiDataChanged(offersBookings);
                /*if(sessionManager.getIsBidBooking())
                {
                    dynamicBooking.addAll(bookingPojo.getData().getActiveBid());
                }
                else
                {*/
                    dynamicBooking.addAll(bookingPojo.getData().getAccepted());
               /* }*/
//                dynamicFragment.notifyiDataChanged(dynamicBooking);
                tvCountFirstTab.setText(""+offersBookings.size());
                tvCountSecondTab.setText(""+dynamicBooking.size());

                if(bookingPojo.getData().getAccepted().size()>0)
                {
                    sessionManager.setIsDriverOnJob(true);
                }
                else
                {
                    sessionManager.setIsDriverOnJob(false);
                }

                acceptedBookingSize = bookingPojo.getData().getAccepted().size();

                if(isFromApi && offersBookings.size() == 0  && !sessionManager.getIsAssignedBooking())
                {
                    AppController.getInstance().stopNewBookingRingtoneService();
                }

                if(isFromApi)
                {
                    if(bookingPojo.getData().getStatus().equals("3"))
                    {
                        setOnOffTheJob(true);
                    }
                    else
                    {
                        setOnOffTheJob(false);
                    }

                    if(bookingPojo.getData().getProfileStatus().equals("1"))
                    {
                        sessionManager.setIsDriverDeactive(false);
                    }
                    else
                    {
                        sessionManager.setIsDriverDeactive(true);
                        setOnOffTheJob(false);
                        tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_grey);
                        tvOnlineOffline.setText(getString(R.string.deactivated));
                    }

                    if(bookingPojo.getData().getProfileActivationStatus().equals("1"))
                    {
                        enableOnlineOffline(true);
                    }
                    else
                    {
                        enableOnlineOffline(false);
                    }
                }

                if(isFromApi && sessionManager.getIsAssignedBooking())
                {
                    sessionManager.setIsAssignedBooking(false);
                    VariableConstant.IS_BOOKING_ACCEPTED = true;
                }

                vpBooking.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(VariableConstant.IS_BOOKING_ACCEPTED)
                        {
                            VariableConstant.IS_BOOKING_ACCEPTED = false;
                            vpBooking.setCurrentItem(1);
                        }
                    }
                },350);

                StringBuilder bookingStr = new StringBuilder();
                String prefix = "";
                for (Booking booking : bookingPojo.getData().getAccepted())
                {
                    bookingStr.append(prefix);
                    prefix = ",";
                    bookingStr.append(booking.getBookingId()).append("|").append(booking.getStatus());
                }
                sessionManager.setBookingStr(bookingStr.toString());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccessUpdateProviderStatus(String msg) {
        //Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
        setOnOffTheJob(!sessionManager.getIsDriverOnline());
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    private void setOnOffTheJob(boolean b) {
        Log.d(TAG, "setOnOffTheJob: "+b);
        if(b)
        {
            sessionManager.setIsDriverOnline(true);
            tvOnlineOffline.setText(getResources().getString(R.string.goOffline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_green);

            if(!Utility.isMyServiceRunning(getActivity(),LocationPublishService.class))
            {
                Intent startIntent = new Intent(getActivity(), LocationPublishService.class);
                startIntent.setAction(VariableConstant.ACTION.STARTFOREGROUND_ACTION);
                getActivity().startService(startIntent);
            }

            if(!AppController.getInstance().getMqttHelper().isMqttConnected())
            {
                AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getPhoneNumber());
            }

            if(Utility.isMyServiceRunning(getActivity(),OfflineLocationPublishService.class))
            {
                Intent stopIntent = new Intent(getActivity(), OfflineLocationPublishService.class);
                stopIntent.setAction(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE);
                getActivity().startService(stopIntent);
            }
        }
        else
        {
            sessionManager.setIsDriverOnline(false);
            tvOnlineOffline.setText(getResources().getString(R.string.goOnline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_red);

            if(Utility.isMyServiceRunning(getActivity(),LocationPublishService.class))
            {
                Intent stopIntent = new Intent(getActivity(), LocationPublishService.class);
                stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
                getActivity().startService(stopIntent);
            }

            if(acceptedBookingSize > 0)
            {
                if(!Utility.isMyServiceRunning(getActivity(),OfflineLocationPublishService.class))
                {
                    Intent startIntent = new Intent(getActivity(), OfflineLocationPublishService.class);
                    startIntent.setAction(VariableConstant.ACTION.STARTOFFLINELOCATIONSERVICE);
                    getActivity().startService(startIntent);
                }
                if(!AppController.getInstance().getMqttHelper().isMqttConnected())
                {
                    AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getPhoneNumber());
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sessionManager.setLastBookingId("");
                    }
                },8000);
            }
            else
            {
                if(AppController.getInstance().getMqttHelper().isMqttConnected())
                {
                    AppController.getInstance().getMqttHelper().disconnect();
                }
                if (Utility.isMyServiceRunning(getActivity(),OfflineLocationPublishService.class))
                {
                    Intent stopIntent = new Intent(getActivity(), OfflineLocationPublishService.class);
                    stopIntent.setAction(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE);
                    getActivity().startService(stopIntent);
                }
            }


        }
    }

    private void enableOnlineOffline(boolean b) {
        if(b)
        {
            sessionManager.setIsProfileAcivated(true);
            tvOnlineOffline.setOnClickListener(this);
            tvStatus.setVisibility(View.GONE);
        }
        else
        {
            sessionManager.setIsProfileAcivated(false);
            tvOnlineOffline.setOnClickListener(null);
            tvStatus.setText(getResources().getString(R.string.profileUnderReview));
            tvStatus.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "onRefresh: "+AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), true);
    }

    public interface BookingFragmentInteraction
    {
        void setAcceptedBookingFragment();
    }
}
