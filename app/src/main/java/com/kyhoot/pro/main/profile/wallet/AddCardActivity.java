package com.kyhoot.pro.main.profile.wallet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.kyhoot.pro.R;
import com.kyhoot.pro.utility.AppController;
import com.kyhoot.pro.utility.SessionManager;
import com.kyhoot.pro.utility.Utility;
import com.kyhoot.pro.utility.VariableConstant;
import com.stripe.android.view.CardMultilineWidget;

import java.util.List;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;



public class AddCardActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks, AddCardPresenter.View {

	private ProgressDialog progressDialog;
	private AddCardPresenter presenter;
	private SessionManager sessionManager;

	private CardMultilineWidget card_input_widget_card;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_card);

		init();
	}

	/**
	 * <h2>initVariablesAndHelperClasses</h2>
	 * <p>
	 *     method to initialize variables and other helper
	 *     classes
	 * </p>
	 */
	private void init()
	{
		sessionManager = SessionManager.getSessionManager(this);
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage(getString(R.string.loading));
		progressDialog.setCancelable(false);
		presenter = new AddCardPresenter(this);

		Typeface fontRegular = Utility.getFontRegular(this);
		Typeface fontMedium = Utility.getFontMedium(this);
		Typeface fontBold = Utility.getFontBold(this);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		if(getSupportActionBar() !=null )
		{
			getSupportActionBar().setDisplayShowTitleEnabled(false);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
		}
		TextView tvTitle = findViewById(R.id.tvTitle);
		tvTitle.setText(getString(R.string.addcard));
		tvTitle.setTypeface(fontBold);

		TextView tvScanCard = findViewById(R.id.tvScanCard);
		tvScanCard.setTypeface(fontMedium);
		tvScanCard.setOnClickListener(this);

		TextView tvDoneCard = findViewById(R.id.tvDoneCard);
		tvDoneCard.setTypeface(fontMedium);
		tvDoneCard.setOnClickListener(this);

		card_input_widget_card=  findViewById(R.id.card_input_widget_card);

	}


	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.tvScanCard:
				Utility.hideKeyboad(this);
				if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
					scanCard();
				} else {
					EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
							1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
				}
				break;

			case R.id.tvDoneCard:
				Utility.hideKeyboad(this);
				presenter.getStripeToken(this, card_input_widget_card.getCard(),sessionManager.getStripeKey(),
						AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getEmail());
				break;

			default:
				break;
		}
	}

	/**
	 *
	 */
	private void scanCard()
	{
		Intent scanIntent = new Intent(this, CardIOActivity.class);
		scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: true
		scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
		scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
		scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false
		scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO,true);
		scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON,false);
		scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME,true );
		startActivityForResult(scanIntent, 100);
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT))
		{
			CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
			scanCardResult(scanResult);
		}
	}

	/**
	 * <h2>scanCardResult</h2>
	 * <p>
	 *     method to get values from the scanned card
	 * </p>
	 * @param scanResult
	 */
	private void scanCardResult(CreditCard scanResult)
	{
		try {
			card_input_widget_card.getCard().setNumber(scanResult.cardNumber);
			if(scanResult.expiryMonth>=1 && scanResult.expiryMonth<=12)
				card_input_widget_card.getCard().setExpMonth(scanResult.expiryMonth);
			card_input_widget_card.getCard().setExpYear(scanResult.expiryYear);
			card_input_widget_card.getCard().setCVC(scanResult.cvv);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == android.R.id.home)
		{
			closeActivity();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		closeActivity();
	}

	private void closeActivity()
	{
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
			finishAfterTransition();
		}
		else
		{
			finish();
			overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
		}
	}

	@Override
	public void startProgressBar() {
		progressDialog.show();
	}

	@Override
	public void stopProgressBar() {
		progressDialog.dismiss();
	}

	@Override
	public void onFailure(String msg) {
		Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onFailure() {
		Toast.makeText(this,getString(R.string.unableToAddCard),Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSuccess(String msg) {
		Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
		VariableConstant.IS_CARD_UPDATED = true;
		closeActivity();
	}

	@Override
	public void invalidCard() {
		Toast.makeText(this,getString(R.string.plsEnterValidCard),Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onNewToken(String token) {
		AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
				sessionManager.getPassword() , token);
	}

	@Override
	public void sessionExpired(String msg) {
		Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
		Utility.logoutSessionExiperd(sessionManager,this);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
	}

	@Override
	public void onPermissionsGranted(int requestCode, List<String> perms) {
		if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
			scanCard();
		}
	}

	@Override
	public void onPermissionsDenied(int requestCode, List<String> perms) {
		if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
			new AppSettingsDialog.Builder(this).build().show();
		}
	}
}
